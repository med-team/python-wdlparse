# Generated from WdlV1Parser.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3r")
        buf.write("\u031f\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.\t.\4")
        buf.write("/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t\64")
        buf.write("\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t")
        buf.write(";\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\3\2\3\2\3\2\3\2\3\2\3")
        buf.write("\2\3\2\3\3\3\3\3\3\3\3\3\3\5\3\u008d\n\3\3\4\3\4\3\4\3")
        buf.write("\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\5\5\u009a\n\5\3\6\3\6\3")
        buf.write("\6\3\6\5\6\u00a0\n\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3")
        buf.write("\t\3\t\5\t\u00ac\n\t\3\n\3\n\3\13\3\13\3\13\3\13\5\13")
        buf.write("\u00b4\n\13\3\13\3\13\3\13\3\13\5\13\u00ba\n\13\3\13\3")
        buf.write("\13\3\13\3\13\5\13\u00c0\n\13\5\13\u00c2\n\13\3\f\7\f")
        buf.write("\u00c5\n\f\f\f\16\f\u00c8\13\f\3\r\3\r\7\r\u00cc\n\r\f")
        buf.write("\r\16\r\u00cf\13\r\3\r\3\r\3\r\3\16\3\16\3\16\3\17\3\17")
        buf.write("\3\17\7\17\u00da\n\17\f\17\16\17\u00dd\13\17\3\17\3\17")
        buf.write("\3\17\3\17\3\17\7\17\u00e4\n\17\f\17\16\17\u00e7\13\17")
        buf.write("\3\17\3\17\5\17\u00eb\n\17\3\20\3\20\3\20\3\20\5\20\u00f1")
        buf.write("\n\20\3\21\3\21\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23")
        buf.write("\7\23\u00fd\n\23\f\23\16\23\u0100\13\23\3\24\3\24\3\24")
        buf.write("\3\24\3\24\3\24\7\24\u0108\n\24\f\24\16\24\u010b\13\24")
        buf.write("\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25")
        buf.write("\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\7\25")
        buf.write("\u0122\n\25\f\25\16\25\u0125\13\25\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\7\26\u0130\n\26\f\26\16\26\u0133")
        buf.write("\13\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3")
        buf.write("\27\3\27\3\27\7\27\u0141\n\27\f\27\16\27\u0144\13\27\3")
        buf.write("\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\7\31\u014e\n\31")
        buf.write("\f\31\16\31\u0151\13\31\3\31\5\31\u0154\n\31\5\31\u0156")
        buf.write("\n\31\3\31\3\31\3\31\3\31\3\31\7\31\u015d\n\31\f\31\16")
        buf.write("\31\u0160\13\31\3\31\5\31\u0163\n\31\7\31\u0165\n\31\f")
        buf.write("\31\16\31\u0168\13\31\3\31\3\31\3\31\3\31\3\31\3\31\3")
        buf.write("\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\7\31")
        buf.write("\u017a\n\31\f\31\16\31\u017d\13\31\3\31\5\31\u0180\n\31")
        buf.write("\7\31\u0182\n\31\f\31\16\31\u0185\13\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\31\3\31\3\31\7\31\u0191\n\31\f")
        buf.write("\31\16\31\u0194\13\31\3\31\5\31\u0197\n\31\7\31\u0199")
        buf.write("\n\31\f\31\16\31\u019c\13\31\3\31\3\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\5\31\u01b0\n\31\3\31\3\31\3\31\3\31\3\31\3")
        buf.write("\31\3\31\3\31\7\31\u01ba\n\31\f\31\16\31\u01bd\13\31\3")
        buf.write("\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34")
        buf.write("\3\35\3\35\3\35\5\35\u01cd\n\35\3\35\7\35\u01d0\n\35\f")
        buf.write("\35\16\35\u01d3\13\35\3\36\3\36\3\36\3\36\7\36\u01d9\n")
        buf.write("\36\f\36\16\36\u01dc\13\36\3\36\3\36\3\37\3\37\3\37\3")
        buf.write("\37\3\37\3\37\3\37\5\37\u01e7\n\37\3 \7 \u01ea\n \f \16")
        buf.write(" \u01ed\13 \3!\3!\3!\3!\3!\3!\3!\3!\5!\u01f7\n!\3\"\3")
        buf.write("\"\3\"\3\"\3\"\7\"\u01fe\n\"\f\"\16\"\u0201\13\"\3\"\3")
        buf.write("\"\5\"\u0205\n\"\3#\3#\3#\3#\3#\7#\u020c\n#\f#\16#\u020f")
        buf.write("\13#\3#\3#\5#\u0213\n#\3$\3$\3$\3$\3%\3%\3%\3%\3&\3&\3")
        buf.write("&\7&\u0220\n&\f&\16&\u0223\13&\3&\3&\3\'\3\'\3\'\7\'\u022a")
        buf.write("\n\'\f\'\16\'\u022d\13\'\3\'\3\'\3(\3(\3(\3(\3)\3)\3)")
        buf.write("\7)\u0238\n)\f)\16)\u023b\13)\3)\3)\3*\3*\3*\7*\u0242")
        buf.write("\n*\f*\16*\u0245\13*\3*\3*\3+\3+\3+\7+\u024c\n+\f+\16")
        buf.write("+\u024f\13+\3+\3+\3,\7,\u0254\n,\f,\16,\u0257\13,\3-\3")
        buf.write("-\7-\u025b\n-\f-\16-\u025e\13-\3-\3-\3-\3.\3.\3.\3/\3")
        buf.write("/\3/\3/\7/\u026a\n/\f/\16/\u026d\13/\3/\3/\3/\3/\3/\3")
        buf.write("/\7/\u0275\n/\f/\16/\u0278\13/\3/\3/\5/\u027c\n/\3\60")
        buf.write("\3\60\3\60\3\60\3\60\3\60\3\60\5\60\u0285\n\60\3\61\3")
        buf.write("\61\3\61\3\61\6\61\u028b\n\61\r\61\16\61\u028c\3\61\3")
        buf.write("\61\3\62\3\62\3\62\3\62\5\62\u0295\n\62\3\63\3\63\3\63")
        buf.write("\3\64\3\64\3\64\3\64\3\65\3\65\3\65\3\65\3\65\7\65\u02a3")
        buf.write("\n\65\f\65\16\65\u02a6\13\65\3\65\5\65\u02a9\n\65\7\65")
        buf.write("\u02ab\n\65\f\65\16\65\u02ae\13\65\3\66\3\66\5\66\u02b2")
        buf.write("\n\66\3\66\3\66\3\67\3\67\3\67\7\67\u02b9\n\67\f\67\16")
        buf.write("\67\u02bc\13\67\38\38\38\58\u02c1\n8\38\58\u02c4\n8\3")
        buf.write("9\39\39\39\39\39\39\39\79\u02ce\n9\f9\169\u02d1\139\3")
        buf.write("9\39\3:\3:\3:\3:\3:\3:\7:\u02db\n:\f:\16:\u02de\13:\3")
        buf.write(":\3:\3;\3;\3;\7;\u02e5\n;\f;\16;\u02e8\13;\3;\3;\3<\3")
        buf.write("<\3<\7<\u02ef\n<\f<\16<\u02f2\13<\3<\3<\3=\3=\3=\3=\3")
        buf.write("=\5=\u02fb\n=\3>\3>\3>\3>\7>\u0301\n>\f>\16>\u0304\13")
        buf.write(">\3>\3>\3?\3?\3?\5?\u030b\n?\3@\3@\7@\u030f\n@\f@\16@")
        buf.write("\u0312\13@\3@\3@\7@\u0316\n@\f@\16@\u0319\13@\5@\u031b")
        buf.write("\n@\3@\3@\3@\2\b$&(*,\60A\2\4\6\b\n\f\16\20\22\24\26\30")
        buf.write("\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`b")
        buf.write("dfhjlnprtvxz|~\2\7\5\2\27\33\37\37FF\3\2#$\3\29:\4\2g")
        buf.write("gii\4\2nnpp\2\u034f\2\u0080\3\2\2\2\4\u0087\3\2\2\2\6")
        buf.write("\u008e\3\2\2\2\b\u0099\3\2\2\2\n\u009f\3\2\2\2\f\u00a1")
        buf.write("\3\2\2\2\16\u00a4\3\2\2\2\20\u00ab\3\2\2\2\22\u00ad\3")
        buf.write("\2\2\2\24\u00c1\3\2\2\2\26\u00c6\3\2\2\2\30\u00c9\3\2")
        buf.write("\2\2\32\u00d3\3\2\2\2\34\u00ea\3\2\2\2\36\u00f0\3\2\2")
        buf.write("\2 \u00f2\3\2\2\2\"\u00f4\3\2\2\2$\u00f6\3\2\2\2&\u0101")
        buf.write("\3\2\2\2(\u010c\3\2\2\2*\u0126\3\2\2\2,\u0134\3\2\2\2")
        buf.write(".\u0145\3\2\2\2\60\u01af\3\2\2\2\62\u01be\3\2\2\2\64\u01c1")
        buf.write("\3\2\2\2\66\u01c6\3\2\2\28\u01c9\3\2\2\2:\u01d4\3\2\2")
        buf.write("\2<\u01e6\3\2\2\2>\u01eb\3\2\2\2@\u01f6\3\2\2\2B\u0204")
        buf.write("\3\2\2\2D\u0212\3\2\2\2F\u0214\3\2\2\2H\u0218\3\2\2\2")
        buf.write("J\u021c\3\2\2\2L\u0226\3\2\2\2N\u0230\3\2\2\2P\u0234\3")
        buf.write("\2\2\2R\u023e\3\2\2\2T\u0248\3\2\2\2V\u0255\3\2\2\2X\u0258")
        buf.write("\3\2\2\2Z\u0262\3\2\2\2\\\u027b\3\2\2\2^\u0284\3\2\2\2")
        buf.write("`\u0286\3\2\2\2b\u0294\3\2\2\2d\u0296\3\2\2\2f\u0299\3")
        buf.write("\2\2\2h\u029d\3\2\2\2j\u02af\3\2\2\2l\u02b5\3\2\2\2n\u02bd")
        buf.write("\3\2\2\2p\u02c5\3\2\2\2r\u02d4\3\2\2\2t\u02e1\3\2\2\2")
        buf.write("v\u02eb\3\2\2\2x\u02fa\3\2\2\2z\u02fc\3\2\2\2|\u030a\3")
        buf.write("\2\2\2~\u030c\3\2\2\2\u0080\u0081\7\35\2\2\u0081\u0082")
        buf.write("\7*\2\2\u0082\u0083\5\n\6\2\u0083\u0084\7<\2\2\u0084\u0085")
        buf.write("\5\n\6\2\u0085\u0086\7+\2\2\u0086\3\3\2\2\2\u0087\u0088")
        buf.write("\7\34\2\2\u0088\u0089\7*\2\2\u0089\u008a\5\n\6\2\u008a")
        buf.write("\u008c\7+\2\2\u008b\u008d\79\2\2\u008c\u008b\3\2\2\2\u008c")
        buf.write("\u008d\3\2\2\2\u008d\5\3\2\2\2\u008e\u008f\7\36\2\2\u008f")
        buf.write("\u0090\7*\2\2\u0090\u0091\5\n\6\2\u0091\u0092\7<\2\2\u0092")
        buf.write("\u0093\5\n\6\2\u0093\u0094\7+\2\2\u0094\7\3\2\2\2\u0095")
        buf.write("\u009a\5\4\3\2\u0096\u009a\5\2\2\2\u0097\u009a\5\6\4\2")
        buf.write("\u0098\u009a\t\2\2\2\u0099\u0095\3\2\2\2\u0099\u0096\3")
        buf.write("\2\2\2\u0099\u0097\3\2\2\2\u0099\u0098\3\2\2\2\u009a\t")
        buf.write("\3\2\2\2\u009b\u009c\5\b\5\2\u009c\u009d\7\67\2\2\u009d")
        buf.write("\u00a0\3\2\2\2\u009e\u00a0\5\b\5\2\u009f\u009b\3\2\2\2")
        buf.write("\u009f\u009e\3\2\2\2\u00a0\13\3\2\2\2\u00a1\u00a2\5\n")
        buf.write("\6\2\u00a2\u00a3\7F\2\2\u00a3\r\3\2\2\2\u00a4\u00a5\5")
        buf.write("\n\6\2\u00a5\u00a6\7F\2\2\u00a6\u00a7\7\64\2\2\u00a7\u00a8")
        buf.write("\5 \21\2\u00a8\17\3\2\2\2\u00a9\u00ac\5\f\7\2\u00aa\u00ac")
        buf.write("\5\16\b\2\u00ab\u00a9\3\2\2\2\u00ab\u00aa\3\2\2\2\u00ac")
        buf.write("\21\3\2\2\2\u00ad\u00ae\t\3\2\2\u00ae\23\3\2\2\2\u00af")
        buf.write("\u00b0\7%\2\2\u00b0\u00b3\7\64\2\2\u00b1\u00b4\5\34\17")
        buf.write("\2\u00b2\u00b4\5\22\n\2\u00b3\u00b1\3\2\2\2\u00b3\u00b2")
        buf.write("\3\2\2\2\u00b4\u00c2\3\2\2\2\u00b5\u00b6\7\"\2\2\u00b6")
        buf.write("\u00b9\7\64\2\2\u00b7\u00ba\5\34\17\2\u00b8\u00ba\5\22")
        buf.write("\n\2\u00b9\u00b7\3\2\2\2\u00b9\u00b8\3\2\2\2\u00ba\u00c2")
        buf.write("\3\2\2\2\u00bb\u00bc\7!\2\2\u00bc\u00bf\7\64\2\2\u00bd")
        buf.write("\u00c0\5\34\17\2\u00be\u00c0\5\22\n\2\u00bf\u00bd\3\2")
        buf.write("\2\2\u00bf\u00be\3\2\2\2\u00c0\u00c2\3\2\2\2\u00c1\u00af")
        buf.write("\3\2\2\2\u00c1\u00b5\3\2\2\2\u00c1\u00bb\3\2\2\2\u00c2")
        buf.write("\25\3\2\2\2\u00c3\u00c5\7G\2\2\u00c4\u00c3\3\2\2\2\u00c5")
        buf.write("\u00c8\3\2\2\2\u00c6\u00c4\3\2\2\2\u00c6\u00c7\3\2\2\2")
        buf.write("\u00c7\27\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c9\u00cd\7M\2")
        buf.write("\2\u00ca\u00cc\5\24\13\2\u00cb\u00ca\3\2\2\2\u00cc\u00cf")
        buf.write("\3\2\2\2\u00cd\u00cb\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce")
        buf.write("\u00d0\3\2\2\2\u00cf\u00cd\3\2\2\2\u00d0\u00d1\5 \21\2")
        buf.write("\u00d1\u00d2\7)\2\2\u00d2\31\3\2\2\2\u00d3\u00d4\5\30")
        buf.write("\r\2\u00d4\u00d5\5\26\f\2\u00d5\33\3\2\2\2\u00d6\u00d7")
        buf.write("\7D\2\2\u00d7\u00db\5\26\f\2\u00d8\u00da\5\32\16\2\u00d9")
        buf.write("\u00d8\3\2\2\2\u00da\u00dd\3\2\2\2\u00db\u00d9\3\2\2\2")
        buf.write("\u00db\u00dc\3\2\2\2\u00dc\u00de\3\2\2\2\u00dd\u00db\3")
        buf.write("\2\2\2\u00de\u00df\7D\2\2\u00df\u00eb\3\2\2\2\u00e0\u00e1")
        buf.write("\7C\2\2\u00e1\u00e5\5\26\f\2\u00e2\u00e4\5\32\16\2\u00e3")
        buf.write("\u00e2\3\2\2\2\u00e4\u00e7\3\2\2\2\u00e5\u00e3\3\2\2\2")
        buf.write("\u00e5\u00e6\3\2\2\2\u00e6\u00e8\3\2\2\2\u00e7\u00e5\3")
        buf.write("\2\2\2\u00e8\u00e9\7C\2\2\u00e9\u00eb\3\2\2\2\u00ea\u00d6")
        buf.write("\3\2\2\2\u00ea\u00e0\3\2\2\2\u00eb\35\3\2\2\2\u00ec\u00f1")
        buf.write("\7%\2\2\u00ed\u00f1\5\22\n\2\u00ee\u00f1\5\34\17\2\u00ef")
        buf.write("\u00f1\7F\2\2\u00f0\u00ec\3\2\2\2\u00f0\u00ed\3\2\2\2")
        buf.write("\u00f0\u00ee\3\2\2\2\u00f0\u00ef\3\2\2\2\u00f1\37\3\2")
        buf.write("\2\2\u00f2\u00f3\5\"\22\2\u00f3!\3\2\2\2\u00f4\u00f5\5")
        buf.write("$\23\2\u00f5#\3\2\2\2\u00f6\u00f7\b\23\1\2\u00f7\u00f8")
        buf.write("\5&\24\2\u00f8\u00fe\3\2\2\2\u00f9\u00fa\f\4\2\2\u00fa")
        buf.write("\u00fb\7\66\2\2\u00fb\u00fd\5&\24\2\u00fc\u00f9\3\2\2")
        buf.write("\2\u00fd\u0100\3\2\2\2\u00fe\u00fc\3\2\2\2\u00fe\u00ff")
        buf.write("\3\2\2\2\u00ff%\3\2\2\2\u0100\u00fe\3\2\2\2\u0101\u0102")
        buf.write("\b\24\1\2\u0102\u0103\5(\25\2\u0103\u0109\3\2\2\2\u0104")
        buf.write("\u0105\f\4\2\2\u0105\u0106\7\65\2\2\u0106\u0108\5(\25")
        buf.write("\2\u0107\u0104\3\2\2\2\u0108\u010b\3\2\2\2\u0109\u0107")
        buf.write("\3\2\2\2\u0109\u010a\3\2\2\2\u010a\'\3\2\2\2\u010b\u0109")
        buf.write("\3\2\2\2\u010c\u010d\b\25\1\2\u010d\u010e\5*\26\2\u010e")
        buf.write("\u0123\3\2\2\2\u010f\u0110\f\t\2\2\u0110\u0111\7\62\2")
        buf.write("\2\u0111\u0122\5*\26\2\u0112\u0113\f\b\2\2\u0113\u0114")
        buf.write("\7\63\2\2\u0114\u0122\5*\26\2\u0115\u0116\f\7\2\2\u0116")
        buf.write("\u0117\7\61\2\2\u0117\u0122\5*\26\2\u0118\u0119\f\6\2")
        buf.write("\2\u0119\u011a\7\60\2\2\u011a\u0122\5*\26\2\u011b\u011c")
        buf.write("\f\5\2\2\u011c\u011d\7.\2\2\u011d\u0122\5*\26\2\u011e")
        buf.write("\u011f\f\4\2\2\u011f\u0120\7/\2\2\u0120\u0122\5*\26\2")
        buf.write("\u0121\u010f\3\2\2\2\u0121\u0112\3\2\2\2\u0121\u0115\3")
        buf.write("\2\2\2\u0121\u0118\3\2\2\2\u0121\u011b\3\2\2\2\u0121\u011e")
        buf.write("\3\2\2\2\u0122\u0125\3\2\2\2\u0123\u0121\3\2\2\2\u0123")
        buf.write("\u0124\3\2\2\2\u0124)\3\2\2\2\u0125\u0123\3\2\2\2\u0126")
        buf.write("\u0127\b\26\1\2\u0127\u0128\5,\27\2\u0128\u0131\3\2\2")
        buf.write("\2\u0129\u012a\f\5\2\2\u012a\u012b\79\2\2\u012b\u0130")
        buf.write("\5,\27\2\u012c\u012d\f\4\2\2\u012d\u012e\7:\2\2\u012e")
        buf.write("\u0130\5,\27\2\u012f\u0129\3\2\2\2\u012f\u012c\3\2\2\2")
        buf.write("\u0130\u0133\3\2\2\2\u0131\u012f\3\2\2\2\u0131\u0132\3")
        buf.write("\2\2\2\u0132+\3\2\2\2\u0133\u0131\3\2\2\2\u0134\u0135")
        buf.write("\b\27\1\2\u0135\u0136\5.\30\2\u0136\u0142\3\2\2\2\u0137")
        buf.write("\u0138\f\6\2\2\u0138\u0139\78\2\2\u0139\u0141\5.\30\2")
        buf.write("\u013a\u013b\f\5\2\2\u013b\u013c\7A\2\2\u013c\u0141\5")
        buf.write(".\30\2\u013d\u013e\f\4\2\2\u013e\u013f\7B\2\2\u013f\u0141")
        buf.write("\5.\30\2\u0140\u0137\3\2\2\2\u0140\u013a\3\2\2\2\u0140")
        buf.write("\u013d\3\2\2\2\u0141\u0144\3\2\2\2\u0142\u0140\3\2\2\2")
        buf.write("\u0142\u0143\3\2\2\2\u0143-\3\2\2\2\u0144\u0142\3\2\2")
        buf.write("\2\u0145\u0146\5\60\31\2\u0146/\3\2\2\2\u0147\u0148\b")
        buf.write("\31\1\2\u0148\u0149\7F\2\2\u0149\u0155\7&\2\2\u014a\u014f")
        buf.write("\5 \21\2\u014b\u014c\7<\2\2\u014c\u014e\5 \21\2\u014d")
        buf.write("\u014b\3\2\2\2\u014e\u0151\3\2\2\2\u014f\u014d\3\2\2\2")
        buf.write("\u014f\u0150\3\2\2\2\u0150\u0153\3\2\2\2\u0151\u014f\3")
        buf.write("\2\2\2\u0152\u0154\7<\2\2\u0153\u0152\3\2\2\2\u0153\u0154")
        buf.write("\3\2\2\2\u0154\u0156\3\2\2\2\u0155\u014a\3\2\2\2\u0155")
        buf.write("\u0156\3\2\2\2\u0156\u0157\3\2\2\2\u0157\u01b0\7\'\2\2")
        buf.write("\u0158\u0166\7*\2\2\u0159\u015e\5 \21\2\u015a\u015b\7")
        buf.write("<\2\2\u015b\u015d\5 \21\2\u015c\u015a\3\2\2\2\u015d\u0160")
        buf.write("\3\2\2\2\u015e\u015c\3\2\2\2\u015e\u015f\3\2\2\2\u015f")
        buf.write("\u0162\3\2\2\2\u0160\u015e\3\2\2\2\u0161\u0163\7<\2\2")
        buf.write("\u0162\u0161\3\2\2\2\u0162\u0163\3\2\2\2\u0163\u0165\3")
        buf.write("\2\2\2\u0164\u0159\3\2\2\2\u0165\u0168\3\2\2\2\u0166\u0164")
        buf.write("\3\2\2\2\u0166\u0167\3\2\2\2\u0167\u0169\3\2\2\2\u0168")
        buf.write("\u0166\3\2\2\2\u0169\u01b0\7+\2\2\u016a\u016b\7&\2\2\u016b")
        buf.write("\u016c\5 \21\2\u016c\u016d\7<\2\2\u016d\u016e\5 \21\2")
        buf.write("\u016e\u016f\7\'\2\2\u016f\u01b0\3\2\2\2\u0170\u0183\7")
        buf.write("(\2\2\u0171\u0172\5 \21\2\u0172\u0173\7-\2\2\u0173\u017b")
        buf.write("\5 \21\2\u0174\u0175\7<\2\2\u0175\u0176\5 \21\2\u0176")
        buf.write("\u0177\7-\2\2\u0177\u0178\5 \21\2\u0178\u017a\3\2\2\2")
        buf.write("\u0179\u0174\3\2\2\2\u017a\u017d\3\2\2\2\u017b\u0179\3")
        buf.write("\2\2\2\u017b\u017c\3\2\2\2\u017c\u017f\3\2\2\2\u017d\u017b")
        buf.write("\3\2\2\2\u017e\u0180\7<\2\2\u017f\u017e\3\2\2\2\u017f")
        buf.write("\u0180\3\2\2\2\u0180\u0182\3\2\2\2\u0181\u0171\3\2\2\2")
        buf.write("\u0182\u0185\3\2\2\2\u0183\u0181\3\2\2\2\u0183\u0184\3")
        buf.write("\2\2\2\u0184\u0186\3\2\2\2\u0185\u0183\3\2\2\2\u0186\u01b0")
        buf.write("\7)\2\2\u0187\u0188\7 \2\2\u0188\u019a\7(\2\2\u0189\u018a")
        buf.write("\7F\2\2\u018a\u018b\7-\2\2\u018b\u0192\5 \21\2\u018c\u018d")
        buf.write("\7<\2\2\u018d\u018e\7F\2\2\u018e\u018f\7-\2\2\u018f\u0191")
        buf.write("\5 \21\2\u0190\u018c\3\2\2\2\u0191\u0194\3\2\2\2\u0192")
        buf.write("\u0190\3\2\2\2\u0192\u0193\3\2\2\2\u0193\u0196\3\2\2\2")
        buf.write("\u0194\u0192\3\2\2\2\u0195\u0197\7<\2\2\u0196\u0195\3")
        buf.write("\2\2\2\u0196\u0197\3\2\2\2\u0197\u0199\3\2\2\2\u0198\u0189")
        buf.write("\3\2\2\2\u0199\u019c\3\2\2\2\u019a\u0198\3\2\2\2\u019a")
        buf.write("\u019b\3\2\2\2\u019b\u019d\3\2\2\2\u019c\u019a\3\2\2\2")
        buf.write("\u019d\u01b0\7)\2\2\u019e\u019f\7\13\2\2\u019f\u01a0\5")
        buf.write(" \21\2\u01a0\u01a1\7\f\2\2\u01a1\u01a2\5 \21\2\u01a2\u01a3")
        buf.write("\7\r\2\2\u01a3\u01a4\5 \21\2\u01a4\u01b0\3\2\2\2\u01a5")
        buf.write("\u01a6\7&\2\2\u01a6\u01a7\5 \21\2\u01a7\u01a8\7\'\2\2")
        buf.write("\u01a8\u01b0\3\2\2\2\u01a9\u01aa\7?\2\2\u01aa\u01b0\5")
        buf.write(" \21\2\u01ab\u01ac\t\4\2\2\u01ac\u01b0\5 \21\2\u01ad\u01b0")
        buf.write("\5\36\20\2\u01ae\u01b0\7F\2\2\u01af\u0147\3\2\2\2\u01af")
        buf.write("\u0158\3\2\2\2\u01af\u016a\3\2\2\2\u01af\u0170\3\2\2\2")
        buf.write("\u01af\u0187\3\2\2\2\u01af\u019e\3\2\2\2\u01af\u01a5\3")
        buf.write("\2\2\2\u01af\u01a9\3\2\2\2\u01af\u01ab\3\2\2\2\u01af\u01ad")
        buf.write("\3\2\2\2\u01af\u01ae\3\2\2\2\u01b0\u01bb\3\2\2\2\u01b1")
        buf.write("\u01b2\f\b\2\2\u01b2\u01b3\7*\2\2\u01b3\u01b4\5 \21\2")
        buf.write("\u01b4\u01b5\7+\2\2\u01b5\u01ba\3\2\2\2\u01b6\u01b7\f")
        buf.write("\7\2\2\u01b7\u01b8\7>\2\2\u01b8\u01ba\7F\2\2\u01b9\u01b1")
        buf.write("\3\2\2\2\u01b9\u01b6\3\2\2\2\u01ba\u01bd\3\2\2\2\u01bb")
        buf.write("\u01b9\3\2\2\2\u01bb\u01bc\3\2\2\2\u01bc\61\3\2\2\2\u01bd")
        buf.write("\u01bb\3\2\2\2\u01be\u01bf\7\4\2\2\u01bf\u01c0\7Q\2\2")
        buf.write("\u01c0\63\3\2\2\2\u01c1\u01c2\7\16\2\2\u01c2\u01c3\7F")
        buf.write("\2\2\u01c3\u01c4\7\17\2\2\u01c4\u01c5\7F\2\2\u01c5\65")
        buf.write("\3\2\2\2\u01c6\u01c7\7\17\2\2\u01c7\u01c8\7F\2\2\u01c8")
        buf.write("\67\3\2\2\2\u01c9\u01ca\7\5\2\2\u01ca\u01cc\5\34\17\2")
        buf.write("\u01cb\u01cd\5\66\34\2\u01cc\u01cb\3\2\2\2\u01cc\u01cd")
        buf.write("\3\2\2\2\u01cd\u01d1\3\2\2\2\u01ce\u01d0\5\64\33\2\u01cf")
        buf.write("\u01ce\3\2\2\2\u01d0\u01d3\3\2\2\2\u01d1\u01cf\3\2\2\2")
        buf.write("\u01d1\u01d2\3\2\2\2\u01d29\3\2\2\2\u01d3\u01d1\3\2\2")
        buf.write("\2\u01d4\u01d5\7\b\2\2\u01d5\u01d6\7F\2\2\u01d6\u01da")
        buf.write("\7(\2\2\u01d7\u01d9\5\f\7\2\u01d8\u01d7\3\2\2\2\u01d9")
        buf.write("\u01dc\3\2\2\2\u01da\u01d8\3\2\2\2\u01da\u01db\3\2\2\2")
        buf.write("\u01db\u01dd\3\2\2\2\u01dc\u01da\3\2\2\2\u01dd\u01de\7")
        buf.write(")\2\2\u01de;\3\2\2\2\u01df\u01e7\7]\2\2\u01e0\u01e7\7")
        buf.write("Z\2\2\u01e1\u01e7\7[\2\2\u01e2\u01e7\7\\\2\2\u01e3\u01e7")
        buf.write("\5@!\2\u01e4\u01e7\5D#\2\u01e5\u01e7\5B\"\2\u01e6\u01df")
        buf.write("\3\2\2\2\u01e6\u01e0\3\2\2\2\u01e6\u01e1\3\2\2\2\u01e6")
        buf.write("\u01e2\3\2\2\2\u01e6\u01e3\3\2\2\2\u01e6\u01e4\3\2\2\2")
        buf.write("\u01e6\u01e5\3\2\2\2\u01e7=\3\2\2\2\u01e8\u01ea\7e\2\2")
        buf.write("\u01e9\u01e8\3\2\2\2\u01ea\u01ed\3\2\2\2\u01eb\u01e9\3")
        buf.write("\2\2\2\u01eb\u01ec\3\2\2\2\u01ec?\3\2\2\2\u01ed\u01eb")
        buf.write("\3\2\2\2\u01ee\u01ef\7_\2\2\u01ef\u01f0\5> \2\u01f0\u01f1")
        buf.write("\7_\2\2\u01f1\u01f7\3\2\2\2\u01f2\u01f3\7^\2\2\u01f3\u01f4")
        buf.write("\5> \2\u01f4\u01f5\7^\2\2\u01f5\u01f7\3\2\2\2\u01f6\u01ee")
        buf.write("\3\2\2\2\u01f6\u01f2\3\2\2\2\u01f7A\3\2\2\2\u01f8\u0205")
        buf.write("\7a\2\2\u01f9\u01fa\7b\2\2\u01fa\u01ff\5<\37\2\u01fb\u01fc")
        buf.write("\7h\2\2\u01fc\u01fe\5<\37\2\u01fd\u01fb\3\2\2\2\u01fe")
        buf.write("\u0201\3\2\2\2\u01ff\u01fd\3\2\2\2\u01ff\u0200\3\2\2\2")
        buf.write("\u0200\u0202\3\2\2\2\u0201\u01ff\3\2\2\2\u0202\u0203\t")
        buf.write("\5\2\2\u0203\u0205\3\2\2\2\u0204\u01f8\3\2\2\2\u0204\u01f9")
        buf.write("\3\2\2\2\u0205C\3\2\2\2\u0206\u0213\7`\2\2\u0207\u0208")
        buf.write("\7c\2\2\u0208\u020d\5F$\2\u0209\u020a\7o\2\2\u020a\u020c")
        buf.write("\5F$\2\u020b\u0209\3\2\2\2\u020c\u020f\3\2\2\2\u020d\u020b")
        buf.write("\3\2\2\2\u020d\u020e\3\2\2\2\u020e\u0210\3\2\2\2\u020f")
        buf.write("\u020d\3\2\2\2\u0210\u0211\t\6\2\2\u0211\u0213\3\2\2\2")
        buf.write("\u0212\u0206\3\2\2\2\u0212\u0207\3\2\2\2\u0213E\3\2\2")
        buf.write("\2\u0214\u0215\7l\2\2\u0215\u0216\7m\2\2\u0216\u0217\5")
        buf.write("<\37\2\u0217G\3\2\2\2\u0218\u0219\7U\2\2\u0219\u021a\7")
        buf.write("V\2\2\u021a\u021b\5<\37\2\u021bI\3\2\2\2\u021c\u021d\7")
        buf.write("\23\2\2\u021d\u0221\7R\2\2\u021e\u0220\5H%\2\u021f\u021e")
        buf.write("\3\2\2\2\u0220\u0223\3\2\2\2\u0221\u021f\3\2\2\2\u0221")
        buf.write("\u0222\3\2\2\2\u0222\u0224\3\2\2\2\u0223\u0221\3\2\2\2")
        buf.write("\u0224\u0225\7W\2\2\u0225K\3\2\2\2\u0226\u0227\7\24\2")
        buf.write("\2\u0227\u022b\7R\2\2\u0228\u022a\5H%\2\u0229\u0228\3")
        buf.write("\2\2\2\u022a\u022d\3\2\2\2\u022b\u0229\3\2\2\2\u022b\u022c")
        buf.write("\3\2\2\2\u022c\u022e\3\2\2\2\u022d\u022b\3\2\2\2\u022e")
        buf.write("\u022f\7W\2\2\u022fM\3\2\2\2\u0230\u0231\7F\2\2\u0231")
        buf.write("\u0232\7-\2\2\u0232\u0233\5 \21\2\u0233O\3\2\2\2\u0234")
        buf.write("\u0235\7\26\2\2\u0235\u0239\7(\2\2\u0236\u0238\5N(\2\u0237")
        buf.write("\u0236\3\2\2\2\u0238\u023b\3\2\2\2\u0239\u0237\3\2\2\2")
        buf.write("\u0239\u023a\3\2\2\2\u023a\u023c\3\2\2\2\u023b\u0239\3")
        buf.write("\2\2\2\u023c\u023d\7)\2\2\u023dQ\3\2\2\2\u023e\u023f\7")
        buf.write("\21\2\2\u023f\u0243\7(\2\2\u0240\u0242\5\20\t\2\u0241")
        buf.write("\u0240\3\2\2\2\u0242\u0245\3\2\2\2\u0243\u0241\3\2\2\2")
        buf.write("\u0243\u0244\3\2\2\2\u0244\u0246\3\2\2\2\u0245\u0243\3")
        buf.write("\2\2\2\u0246\u0247\7)\2\2\u0247S\3\2\2\2\u0248\u0249\7")
        buf.write("\22\2\2\u0249\u024d\7(\2\2\u024a\u024c\5\16\b\2\u024b")
        buf.write("\u024a\3\2\2\2\u024c\u024f\3\2\2\2\u024d\u024b\3\2\2\2")
        buf.write("\u024d\u024e\3\2\2\2\u024e\u0250\3\2\2\2\u024f\u024d\3")
        buf.write("\2\2\2\u0250\u0251\7)\2\2\u0251U\3\2\2\2\u0252\u0254\7")
        buf.write("O\2\2\u0253\u0252\3\2\2\2\u0254\u0257\3\2\2\2\u0255\u0253")
        buf.write("\3\2\2\2\u0255\u0256\3\2\2\2\u0256W\3\2\2\2\u0257\u0255")
        buf.write("\3\2\2\2\u0258\u025c\7M\2\2\u0259\u025b\5\24\13\2\u025a")
        buf.write("\u0259\3\2\2\2\u025b\u025e\3\2\2\2\u025c\u025a\3\2\2\2")
        buf.write("\u025c\u025d\3\2\2\2\u025d\u025f\3\2\2\2\u025e\u025c\3")
        buf.write("\2\2\2\u025f\u0260\5 \21\2\u0260\u0261\7)\2\2\u0261Y\3")
        buf.write("\2\2\2\u0262\u0263\5X-\2\u0263\u0264\5V,\2\u0264[\3\2")
        buf.write("\2\2\u0265\u0266\7\25\2\2\u0266\u0267\7J\2\2\u0267\u026b")
        buf.write("\5V,\2\u0268\u026a\5Z.\2\u0269\u0268\3\2\2\2\u026a\u026d")
        buf.write("\3\2\2\2\u026b\u0269\3\2\2\2\u026b\u026c\3\2\2\2\u026c")
        buf.write("\u026e\3\2\2\2\u026d\u026b\3\2\2\2\u026e\u026f\7N\2\2")
        buf.write("\u026f\u027c\3\2\2\2\u0270\u0271\7\25\2\2\u0271\u0272")
        buf.write("\7I\2\2\u0272\u0276\5V,\2\u0273\u0275\5Z.\2\u0274\u0273")
        buf.write("\3\2\2\2\u0275\u0278\3\2\2\2\u0276\u0274\3\2\2\2\u0276")
        buf.write("\u0277\3\2\2\2\u0277\u0279\3\2\2\2\u0278\u0276\3\2\2\2")
        buf.write("\u0279\u027a\7N\2\2\u027a\u027c\3\2\2\2\u027b\u0265\3")
        buf.write("\2\2\2\u027b\u0270\3\2\2\2\u027c]\3\2\2\2\u027d\u0285")
        buf.write("\5R*\2\u027e\u0285\5T+\2\u027f\u0285\5\\/\2\u0280\u0285")
        buf.write("\5P)\2\u0281\u0285\5\16\b\2\u0282\u0285\5J&\2\u0283\u0285")
        buf.write("\5L\'\2\u0284\u027d\3\2\2\2\u0284\u027e\3\2\2\2\u0284")
        buf.write("\u027f\3\2\2\2\u0284\u0280\3\2\2\2\u0284\u0281\3\2\2\2")
        buf.write("\u0284\u0282\3\2\2\2\u0284\u0283\3\2\2\2\u0285_\3\2\2")
        buf.write("\2\u0286\u0287\7\7\2\2\u0287\u0288\7F\2\2\u0288\u028a")
        buf.write("\7(\2\2\u0289\u028b\5^\60\2\u028a\u0289\3\2\2\2\u028b")
        buf.write("\u028c\3\2\2\2\u028c\u028a\3\2\2\2\u028c\u028d\3\2\2\2")
        buf.write("\u028d\u028e\3\2\2\2\u028e\u028f\7)\2\2\u028fa\3\2\2\2")
        buf.write("\u0290\u0295\5\16\b\2\u0291\u0295\5n8\2\u0292\u0295\5")
        buf.write("p9\2\u0293\u0295\5r:\2\u0294\u0290\3\2\2\2\u0294\u0291")
        buf.write("\3\2\2\2\u0294\u0292\3\2\2\2\u0294\u0293\3\2\2\2\u0295")
        buf.write("c\3\2\2\2\u0296\u0297\7\17\2\2\u0297\u0298\7F\2\2\u0298")
        buf.write("e\3\2\2\2\u0299\u029a\7F\2\2\u029a\u029b\7\64\2\2\u029b")
        buf.write("\u029c\5 \21\2\u029cg\3\2\2\2\u029d\u029e\7\21\2\2\u029e")
        buf.write("\u02ac\7-\2\2\u029f\u02a4\5f\64\2\u02a0\u02a1\7<\2\2\u02a1")
        buf.write("\u02a3\5f\64\2\u02a2\u02a0\3\2\2\2\u02a3\u02a6\3\2\2\2")
        buf.write("\u02a4\u02a2\3\2\2\2\u02a4\u02a5\3\2\2\2\u02a5\u02a8\3")
        buf.write("\2\2\2\u02a6\u02a4\3\2\2\2\u02a7\u02a9\7<\2\2\u02a8\u02a7")
        buf.write("\3\2\2\2\u02a8\u02a9\3\2\2\2\u02a9\u02ab\3\2\2\2\u02aa")
        buf.write("\u029f\3\2\2\2\u02ab\u02ae\3\2\2\2\u02ac\u02aa\3\2\2\2")
        buf.write("\u02ac\u02ad\3\2\2\2\u02adi\3\2\2\2\u02ae\u02ac\3\2\2")
        buf.write("\2\u02af\u02b1\7(\2\2\u02b0\u02b2\5h\65\2\u02b1\u02b0")
        buf.write("\3\2\2\2\u02b1\u02b2\3\2\2\2\u02b2\u02b3\3\2\2\2\u02b3")
        buf.write("\u02b4\7)\2\2\u02b4k\3\2\2\2\u02b5\u02ba\7F\2\2\u02b6")
        buf.write("\u02b7\7>\2\2\u02b7\u02b9\7F\2\2\u02b8\u02b6\3\2\2\2\u02b9")
        buf.write("\u02bc\3\2\2\2\u02ba\u02b8\3\2\2\2\u02ba\u02bb\3\2\2\2")
        buf.write("\u02bbm\3\2\2\2\u02bc\u02ba\3\2\2\2\u02bd\u02be\7\n\2")
        buf.write("\2\u02be\u02c0\5l\67\2\u02bf\u02c1\5d\63\2\u02c0\u02bf")
        buf.write("\3\2\2\2\u02c0\u02c1\3\2\2\2\u02c1\u02c3\3\2\2\2\u02c2")
        buf.write("\u02c4\5j\66\2\u02c3\u02c2\3\2\2\2\u02c3\u02c4\3\2\2\2")
        buf.write("\u02c4o\3\2\2\2\u02c5\u02c6\7\t\2\2\u02c6\u02c7\7&\2\2")
        buf.write("\u02c7\u02c8\7F\2\2\u02c8\u02c9\7\20\2\2\u02c9\u02ca\5")
        buf.write(" \21\2\u02ca\u02cb\7\'\2\2\u02cb\u02cf\7(\2\2\u02cc\u02ce")
        buf.write("\5b\62\2\u02cd\u02cc\3\2\2\2\u02ce\u02d1\3\2\2\2\u02cf")
        buf.write("\u02cd\3\2\2\2\u02cf\u02d0\3\2\2\2\u02d0\u02d2\3\2\2\2")
        buf.write("\u02d1\u02cf\3\2\2\2\u02d2\u02d3\7)\2\2\u02d3q\3\2\2\2")
        buf.write("\u02d4\u02d5\7\13\2\2\u02d5\u02d6\7&\2\2\u02d6\u02d7\5")
        buf.write(" \21\2\u02d7\u02d8\7\'\2\2\u02d8\u02dc\7(\2\2\u02d9\u02db")
        buf.write("\5b\62\2\u02da\u02d9\3\2\2\2\u02db\u02de\3\2\2\2\u02dc")
        buf.write("\u02da\3\2\2\2\u02dc\u02dd\3\2\2\2\u02dd\u02df\3\2\2\2")
        buf.write("\u02de\u02dc\3\2\2\2\u02df\u02e0\7)\2\2\u02e0s\3\2\2\2")
        buf.write("\u02e1\u02e2\7\21\2\2\u02e2\u02e6\7(\2\2\u02e3\u02e5\5")
        buf.write("\20\t\2\u02e4\u02e3\3\2\2\2\u02e5\u02e8\3\2\2\2\u02e6")
        buf.write("\u02e4\3\2\2\2\u02e6\u02e7\3\2\2\2\u02e7\u02e9\3\2\2\2")
        buf.write("\u02e8\u02e6\3\2\2\2\u02e9\u02ea\7)\2\2\u02eau\3\2\2\2")
        buf.write("\u02eb\u02ec\7\22\2\2\u02ec\u02f0\7(\2\2\u02ed\u02ef\5")
        buf.write("\16\b\2\u02ee\u02ed\3\2\2\2\u02ef\u02f2\3\2\2\2\u02f0")
        buf.write("\u02ee\3\2\2\2\u02f0\u02f1\3\2\2\2\u02f1\u02f3\3\2\2\2")
        buf.write("\u02f2\u02f0\3\2\2\2\u02f3\u02f4\7)\2\2\u02f4w\3\2\2\2")
        buf.write("\u02f5\u02fb\5t;\2\u02f6\u02fb\5v<\2\u02f7\u02fb\5b\62")
        buf.write("\2\u02f8\u02fb\5J&\2\u02f9\u02fb\5L\'\2\u02fa\u02f5\3")
        buf.write("\2\2\2\u02fa\u02f6\3\2\2\2\u02fa\u02f7\3\2\2\2\u02fa\u02f8")
        buf.write("\3\2\2\2\u02fa\u02f9\3\2\2\2\u02fby\3\2\2\2\u02fc\u02fd")
        buf.write("\7\6\2\2\u02fd\u02fe\7F\2\2\u02fe\u0302\7(\2\2\u02ff\u0301")
        buf.write("\5x=\2\u0300\u02ff\3\2\2\2\u0301\u0304\3\2\2\2\u0302\u0300")
        buf.write("\3\2\2\2\u0302\u0303\3\2\2\2\u0303\u0305\3\2\2\2\u0304")
        buf.write("\u0302\3\2\2\2\u0305\u0306\7)\2\2\u0306{\3\2\2\2\u0307")
        buf.write("\u030b\58\35\2\u0308\u030b\5:\36\2\u0309\u030b\5`\61\2")
        buf.write("\u030a\u0307\3\2\2\2\u030a\u0308\3\2\2\2\u030a\u0309\3")
        buf.write("\2\2\2\u030b}\3\2\2\2\u030c\u0310\5\62\32\2\u030d\u030f")
        buf.write("\5|?\2\u030e\u030d\3\2\2\2\u030f\u0312\3\2\2\2\u0310\u030e")
        buf.write("\3\2\2\2\u0310\u0311\3\2\2\2\u0311\u031a\3\2\2\2\u0312")
        buf.write("\u0310\3\2\2\2\u0313\u0317\5z>\2\u0314\u0316\5|?\2\u0315")
        buf.write("\u0314\3\2\2\2\u0316\u0319\3\2\2\2\u0317\u0315\3\2\2\2")
        buf.write("\u0317\u0318\3\2\2\2\u0318\u031b\3\2\2\2\u0319\u0317\3")
        buf.write("\2\2\2\u031a\u0313\3\2\2\2\u031a\u031b\3\2\2\2\u031b\u031c")
        buf.write("\3\2\2\2\u031c\u031d\7\2\2\3\u031d\177\3\2\2\2O\u008c")
        buf.write("\u0099\u009f\u00ab\u00b3\u00b9\u00bf\u00c1\u00c6\u00cd")
        buf.write("\u00db\u00e5\u00ea\u00f0\u00fe\u0109\u0121\u0123\u012f")
        buf.write("\u0131\u0140\u0142\u014f\u0153\u0155\u015e\u0162\u0166")
        buf.write("\u017b\u017f\u0183\u0192\u0196\u019a\u01af\u01b9\u01bb")
        buf.write("\u01cc\u01d1\u01da\u01e6\u01eb\u01f6\u01ff\u0204\u020d")
        buf.write("\u0212\u0221\u022b\u0239\u0243\u024d\u0255\u025c\u026b")
        buf.write("\u0276\u027b\u0284\u028c\u0294\u02a4\u02a8\u02ac\u02b1")
        buf.write("\u02ba\u02c0\u02c3\u02cf\u02dc\u02e6\u02f0\u02fa\u0302")
        buf.write("\u030a\u0310\u0317\u031a")
        return buf.getvalue()


class WdlV1Parser ( Parser ):

    grammarFileName = "WdlV1Parser.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "'version'", "'import'", 
                     "'workflow'", "'task'", "'struct'", "'scatter'", "'call'", 
                     "'if'", "'then'", "'else'", "'alias'", "'as'", "'in'", 
                     "'input'", "'output'", "'parameter_meta'", "'meta'", 
                     "'command'", "'runtime'", "'Boolean'", "'Int'", "'Float'", 
                     "'String'", "'File'", "'Array'", "'Map'", "'Pair'", 
                     "'Object'", "'object'", "'sep'", "'default'", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "'('", "')'", "<INVALID>", 
                     "<INVALID>", "'['", "<INVALID>", "'\\'", "<INVALID>", 
                     "'<'", "'>'", "'>='", "'<='", "'=='", "'!='", "'='", 
                     "'&&'", "'||'", "'?'", "'*'", "'+'", "'-'", "<INVALID>", 
                     "<INVALID>", "';'", "'.'", "'!'", "<INVALID>", "'/'", 
                     "'%'", "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "'<<<'", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "'null'", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "'\\>>>'" ]

    symbolicNames = [ "<INVALID>", "LINE_COMMENT", "VERSION", "IMPORT", 
                      "WORKFLOW", "TASK", "STRUCT", "SCATTER", "CALL", "IF", 
                      "THEN", "ELSE", "ALIAS", "AS", "In", "INPUT", "OUTPUT", 
                      "PARAMETERMETA", "META", "COMMAND", "RUNTIME", "BOOLEAN", 
                      "INT", "FLOAT", "STRING", "FILE", "ARRAY", "MAP", 
                      "PAIR", "OBJECT", "OBJECT_LITERAL", "SEP", "DEFAULT", 
                      "IntLiteral", "FloatLiteral", "BoolLiteral", "LPAREN", 
                      "RPAREN", "LBRACE", "RBRACE", "LBRACK", "RBRACK", 
                      "ESC", "COLON", "LT", "GT", "GTE", "LTE", "EQUALITY", 
                      "NOTEQUAL", "EQUAL", "AND", "OR", "OPTIONAL", "STAR", 
                      "PLUS", "MINUS", "DOLLAR", "COMMA", "SEMI", "DOT", 
                      "NOT", "TILDE", "DIVIDE", "MOD", "SQUOTE", "DQUOTE", 
                      "WHITESPACE", "Identifier", "StringPart", "BeginWhitespace", 
                      "BeginHereDoc", "BeginLBrace", "HereDocUnicodeEscape", 
                      "CommandUnicodeEscape", "StringCommandStart", "EndCommand", 
                      "CommandStringPart", "VersionWhitespace", "ReleaseVersion", 
                      "BeginMeta", "MetaWhitespace", "MetaBodyComment", 
                      "MetaIdentifier", "MetaColon", "EndMeta", "MetaBodyWhitespace", 
                      "MetaValueComment", "MetaBool", "MetaInt", "MetaFloat", 
                      "MetaNull", "MetaSquote", "MetaDquote", "MetaEmptyObject", 
                      "MetaEmptyArray", "MetaLbrack", "MetaLbrace", "MetaValueWhitespace", 
                      "MetaStringPart", "MetaArrayComment", "MetaArrayCommaRbrack", 
                      "MetaArrayComma", "MetaRbrack", "MetaArrayWhitespace", 
                      "MetaObjectComment", "MetaObjectIdentifier", "MetaObjectColon", 
                      "MetaObjectCommaRbrace", "MetaObjectComma", "MetaRbrace", 
                      "MetaObjectWhitespace", "HereDocEscapedEnd" ]

    RULE_map_type = 0
    RULE_array_type = 1
    RULE_pair_type = 2
    RULE_type_base = 3
    RULE_wdl_type = 4
    RULE_unbound_decls = 5
    RULE_bound_decls = 6
    RULE_any_decls = 7
    RULE_number = 8
    RULE_expression_placeholder_option = 9
    RULE_string_part = 10
    RULE_string_expr_part = 11
    RULE_string_expr_with_string_part = 12
    RULE_string = 13
    RULE_primitive_literal = 14
    RULE_expr = 15
    RULE_expr_infix = 16
    RULE_expr_infix0 = 17
    RULE_expr_infix1 = 18
    RULE_expr_infix2 = 19
    RULE_expr_infix3 = 20
    RULE_expr_infix4 = 21
    RULE_expr_infix5 = 22
    RULE_expr_core = 23
    RULE_version = 24
    RULE_import_alias = 25
    RULE_import_as = 26
    RULE_import_doc = 27
    RULE_struct = 28
    RULE_meta_value = 29
    RULE_meta_string_part = 30
    RULE_meta_string = 31
    RULE_meta_array = 32
    RULE_meta_object = 33
    RULE_meta_object_kv = 34
    RULE_meta_kv = 35
    RULE_parameter_meta = 36
    RULE_meta = 37
    RULE_task_runtime_kv = 38
    RULE_task_runtime = 39
    RULE_task_input = 40
    RULE_task_output = 41
    RULE_task_command_string_part = 42
    RULE_task_command_expr_part = 43
    RULE_task_command_expr_with_string = 44
    RULE_task_command = 45
    RULE_task_element = 46
    RULE_task = 47
    RULE_inner_workflow_element = 48
    RULE_call_alias = 49
    RULE_call_input = 50
    RULE_call_inputs = 51
    RULE_call_body = 52
    RULE_call_name = 53
    RULE_call = 54
    RULE_scatter = 55
    RULE_conditional = 56
    RULE_workflow_input = 57
    RULE_workflow_output = 58
    RULE_workflow_element = 59
    RULE_workflow = 60
    RULE_document_element = 61
    RULE_document = 62

    ruleNames =  [ "map_type", "array_type", "pair_type", "type_base", "wdl_type", 
                   "unbound_decls", "bound_decls", "any_decls", "number", 
                   "expression_placeholder_option", "string_part", "string_expr_part", 
                   "string_expr_with_string_part", "string", "primitive_literal", 
                   "expr", "expr_infix", "expr_infix0", "expr_infix1", "expr_infix2", 
                   "expr_infix3", "expr_infix4", "expr_infix5", "expr_core", 
                   "version", "import_alias", "import_as", "import_doc", 
                   "struct", "meta_value", "meta_string_part", "meta_string", 
                   "meta_array", "meta_object", "meta_object_kv", "meta_kv", 
                   "parameter_meta", "meta", "task_runtime_kv", "task_runtime", 
                   "task_input", "task_output", "task_command_string_part", 
                   "task_command_expr_part", "task_command_expr_with_string", 
                   "task_command", "task_element", "task", "inner_workflow_element", 
                   "call_alias", "call_input", "call_inputs", "call_body", 
                   "call_name", "call", "scatter", "conditional", "workflow_input", 
                   "workflow_output", "workflow_element", "workflow", "document_element", 
                   "document" ]

    EOF = Token.EOF
    LINE_COMMENT=1
    VERSION=2
    IMPORT=3
    WORKFLOW=4
    TASK=5
    STRUCT=6
    SCATTER=7
    CALL=8
    IF=9
    THEN=10
    ELSE=11
    ALIAS=12
    AS=13
    In=14
    INPUT=15
    OUTPUT=16
    PARAMETERMETA=17
    META=18
    COMMAND=19
    RUNTIME=20
    BOOLEAN=21
    INT=22
    FLOAT=23
    STRING=24
    FILE=25
    ARRAY=26
    MAP=27
    PAIR=28
    OBJECT=29
    OBJECT_LITERAL=30
    SEP=31
    DEFAULT=32
    IntLiteral=33
    FloatLiteral=34
    BoolLiteral=35
    LPAREN=36
    RPAREN=37
    LBRACE=38
    RBRACE=39
    LBRACK=40
    RBRACK=41
    ESC=42
    COLON=43
    LT=44
    GT=45
    GTE=46
    LTE=47
    EQUALITY=48
    NOTEQUAL=49
    EQUAL=50
    AND=51
    OR=52
    OPTIONAL=53
    STAR=54
    PLUS=55
    MINUS=56
    DOLLAR=57
    COMMA=58
    SEMI=59
    DOT=60
    NOT=61
    TILDE=62
    DIVIDE=63
    MOD=64
    SQUOTE=65
    DQUOTE=66
    WHITESPACE=67
    Identifier=68
    StringPart=69
    BeginWhitespace=70
    BeginHereDoc=71
    BeginLBrace=72
    HereDocUnicodeEscape=73
    CommandUnicodeEscape=74
    StringCommandStart=75
    EndCommand=76
    CommandStringPart=77
    VersionWhitespace=78
    ReleaseVersion=79
    BeginMeta=80
    MetaWhitespace=81
    MetaBodyComment=82
    MetaIdentifier=83
    MetaColon=84
    EndMeta=85
    MetaBodyWhitespace=86
    MetaValueComment=87
    MetaBool=88
    MetaInt=89
    MetaFloat=90
    MetaNull=91
    MetaSquote=92
    MetaDquote=93
    MetaEmptyObject=94
    MetaEmptyArray=95
    MetaLbrack=96
    MetaLbrace=97
    MetaValueWhitespace=98
    MetaStringPart=99
    MetaArrayComment=100
    MetaArrayCommaRbrack=101
    MetaArrayComma=102
    MetaRbrack=103
    MetaArrayWhitespace=104
    MetaObjectComment=105
    MetaObjectIdentifier=106
    MetaObjectColon=107
    MetaObjectCommaRbrace=108
    MetaObjectComma=109
    MetaRbrace=110
    MetaObjectWhitespace=111
    HereDocEscapedEnd=112

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class Map_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MAP(self):
            return self.getToken(WdlV1Parser.MAP, 0)

        def LBRACK(self):
            return self.getToken(WdlV1Parser.LBRACK, 0)

        def wdl_type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Wdl_typeContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Wdl_typeContext,i)


        def COMMA(self):
            return self.getToken(WdlV1Parser.COMMA, 0)

        def RBRACK(self):
            return self.getToken(WdlV1Parser.RBRACK, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_map_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMap_type" ):
                listener.enterMap_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMap_type" ):
                listener.exitMap_type(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMap_type" ):
                return visitor.visitMap_type(self)
            else:
                return visitor.visitChildren(self)




    def map_type(self):

        localctx = WdlV1Parser.Map_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_map_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 126
            self.match(WdlV1Parser.MAP)
            self.state = 127
            self.match(WdlV1Parser.LBRACK)
            self.state = 128
            self.wdl_type()
            self.state = 129
            self.match(WdlV1Parser.COMMA)
            self.state = 130
            self.wdl_type()
            self.state = 131
            self.match(WdlV1Parser.RBRACK)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Array_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ARRAY(self):
            return self.getToken(WdlV1Parser.ARRAY, 0)

        def LBRACK(self):
            return self.getToken(WdlV1Parser.LBRACK, 0)

        def wdl_type(self):
            return self.getTypedRuleContext(WdlV1Parser.Wdl_typeContext,0)


        def RBRACK(self):
            return self.getToken(WdlV1Parser.RBRACK, 0)

        def PLUS(self):
            return self.getToken(WdlV1Parser.PLUS, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_array_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArray_type" ):
                listener.enterArray_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArray_type" ):
                listener.exitArray_type(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArray_type" ):
                return visitor.visitArray_type(self)
            else:
                return visitor.visitChildren(self)




    def array_type(self):

        localctx = WdlV1Parser.Array_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_array_type)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 133
            self.match(WdlV1Parser.ARRAY)
            self.state = 134
            self.match(WdlV1Parser.LBRACK)
            self.state = 135
            self.wdl_type()
            self.state = 136
            self.match(WdlV1Parser.RBRACK)
            self.state = 138
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlV1Parser.PLUS:
                self.state = 137
                self.match(WdlV1Parser.PLUS)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Pair_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PAIR(self):
            return self.getToken(WdlV1Parser.PAIR, 0)

        def LBRACK(self):
            return self.getToken(WdlV1Parser.LBRACK, 0)

        def wdl_type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Wdl_typeContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Wdl_typeContext,i)


        def COMMA(self):
            return self.getToken(WdlV1Parser.COMMA, 0)

        def RBRACK(self):
            return self.getToken(WdlV1Parser.RBRACK, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_pair_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPair_type" ):
                listener.enterPair_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPair_type" ):
                listener.exitPair_type(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPair_type" ):
                return visitor.visitPair_type(self)
            else:
                return visitor.visitChildren(self)




    def pair_type(self):

        localctx = WdlV1Parser.Pair_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_pair_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 140
            self.match(WdlV1Parser.PAIR)
            self.state = 141
            self.match(WdlV1Parser.LBRACK)
            self.state = 142
            self.wdl_type()
            self.state = 143
            self.match(WdlV1Parser.COMMA)
            self.state = 144
            self.wdl_type()
            self.state = 145
            self.match(WdlV1Parser.RBRACK)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_baseContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def array_type(self):
            return self.getTypedRuleContext(WdlV1Parser.Array_typeContext,0)


        def map_type(self):
            return self.getTypedRuleContext(WdlV1Parser.Map_typeContext,0)


        def pair_type(self):
            return self.getTypedRuleContext(WdlV1Parser.Pair_typeContext,0)


        def STRING(self):
            return self.getToken(WdlV1Parser.STRING, 0)

        def FILE(self):
            return self.getToken(WdlV1Parser.FILE, 0)

        def BOOLEAN(self):
            return self.getToken(WdlV1Parser.BOOLEAN, 0)

        def OBJECT(self):
            return self.getToken(WdlV1Parser.OBJECT, 0)

        def INT(self):
            return self.getToken(WdlV1Parser.INT, 0)

        def FLOAT(self):
            return self.getToken(WdlV1Parser.FLOAT, 0)

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_type_base

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_base" ):
                listener.enterType_base(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_base" ):
                listener.exitType_base(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_base" ):
                return visitor.visitType_base(self)
            else:
                return visitor.visitChildren(self)




    def type_base(self):

        localctx = WdlV1Parser.Type_baseContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_type_base)
        self._la = 0 # Token type
        try:
            self.state = 151
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.ARRAY]:
                self.enterOuterAlt(localctx, 1)
                self.state = 147
                self.array_type()
                pass
            elif token in [WdlV1Parser.MAP]:
                self.enterOuterAlt(localctx, 2)
                self.state = 148
                self.map_type()
                pass
            elif token in [WdlV1Parser.PAIR]:
                self.enterOuterAlt(localctx, 3)
                self.state = 149
                self.pair_type()
                pass
            elif token in [WdlV1Parser.BOOLEAN, WdlV1Parser.INT, WdlV1Parser.FLOAT, WdlV1Parser.STRING, WdlV1Parser.FILE, WdlV1Parser.OBJECT, WdlV1Parser.Identifier]:
                self.enterOuterAlt(localctx, 4)
                self.state = 150
                _la = self._input.LA(1)
                if not(((((_la - 21)) & ~0x3f) == 0 and ((1 << (_la - 21)) & ((1 << (WdlV1Parser.BOOLEAN - 21)) | (1 << (WdlV1Parser.INT - 21)) | (1 << (WdlV1Parser.FLOAT - 21)) | (1 << (WdlV1Parser.STRING - 21)) | (1 << (WdlV1Parser.FILE - 21)) | (1 << (WdlV1Parser.OBJECT - 21)) | (1 << (WdlV1Parser.Identifier - 21)))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Wdl_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_base(self):
            return self.getTypedRuleContext(WdlV1Parser.Type_baseContext,0)


        def OPTIONAL(self):
            return self.getToken(WdlV1Parser.OPTIONAL, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_wdl_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWdl_type" ):
                listener.enterWdl_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWdl_type" ):
                listener.exitWdl_type(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWdl_type" ):
                return visitor.visitWdl_type(self)
            else:
                return visitor.visitChildren(self)




    def wdl_type(self):

        localctx = WdlV1Parser.Wdl_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_wdl_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 157
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.state = 153
                self.type_base()
                self.state = 154
                self.match(WdlV1Parser.OPTIONAL)
                pass

            elif la_ == 2:
                self.state = 156
                self.type_base()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Unbound_declsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def wdl_type(self):
            return self.getTypedRuleContext(WdlV1Parser.Wdl_typeContext,0)


        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_unbound_decls

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnbound_decls" ):
                listener.enterUnbound_decls(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnbound_decls" ):
                listener.exitUnbound_decls(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnbound_decls" ):
                return visitor.visitUnbound_decls(self)
            else:
                return visitor.visitChildren(self)




    def unbound_decls(self):

        localctx = WdlV1Parser.Unbound_declsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_unbound_decls)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 159
            self.wdl_type()
            self.state = 160
            self.match(WdlV1Parser.Identifier)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bound_declsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def wdl_type(self):
            return self.getTypedRuleContext(WdlV1Parser.Wdl_typeContext,0)


        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def EQUAL(self):
            return self.getToken(WdlV1Parser.EQUAL, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_bound_decls

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBound_decls" ):
                listener.enterBound_decls(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBound_decls" ):
                listener.exitBound_decls(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBound_decls" ):
                return visitor.visitBound_decls(self)
            else:
                return visitor.visitChildren(self)




    def bound_decls(self):

        localctx = WdlV1Parser.Bound_declsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_bound_decls)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 162
            self.wdl_type()
            self.state = 163
            self.match(WdlV1Parser.Identifier)
            self.state = 164
            self.match(WdlV1Parser.EQUAL)
            self.state = 165
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Any_declsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def unbound_decls(self):
            return self.getTypedRuleContext(WdlV1Parser.Unbound_declsContext,0)


        def bound_decls(self):
            return self.getTypedRuleContext(WdlV1Parser.Bound_declsContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_any_decls

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAny_decls" ):
                listener.enterAny_decls(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAny_decls" ):
                listener.exitAny_decls(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAny_decls" ):
                return visitor.visitAny_decls(self)
            else:
                return visitor.visitChildren(self)




    def any_decls(self):

        localctx = WdlV1Parser.Any_declsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_any_decls)
        try:
            self.state = 169
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 167
                self.unbound_decls()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 168
                self.bound_decls()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumberContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IntLiteral(self):
            return self.getToken(WdlV1Parser.IntLiteral, 0)

        def FloatLiteral(self):
            return self.getToken(WdlV1Parser.FloatLiteral, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_number

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumber" ):
                listener.enterNumber(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumber" ):
                listener.exitNumber(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNumber" ):
                return visitor.visitNumber(self)
            else:
                return visitor.visitChildren(self)




    def number(self):

        localctx = WdlV1Parser.NumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_number)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 171
            _la = self._input.LA(1)
            if not(_la==WdlV1Parser.IntLiteral or _la==WdlV1Parser.FloatLiteral):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expression_placeholder_optionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BoolLiteral(self):
            return self.getToken(WdlV1Parser.BoolLiteral, 0)

        def EQUAL(self):
            return self.getToken(WdlV1Parser.EQUAL, 0)

        def string(self):
            return self.getTypedRuleContext(WdlV1Parser.StringContext,0)


        def number(self):
            return self.getTypedRuleContext(WdlV1Parser.NumberContext,0)


        def DEFAULT(self):
            return self.getToken(WdlV1Parser.DEFAULT, 0)

        def SEP(self):
            return self.getToken(WdlV1Parser.SEP, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_expression_placeholder_option

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression_placeholder_option" ):
                listener.enterExpression_placeholder_option(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression_placeholder_option" ):
                listener.exitExpression_placeholder_option(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression_placeholder_option" ):
                return visitor.visitExpression_placeholder_option(self)
            else:
                return visitor.visitChildren(self)




    def expression_placeholder_option(self):

        localctx = WdlV1Parser.Expression_placeholder_optionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_expression_placeholder_option)
        try:
            self.state = 191
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.BoolLiteral]:
                self.enterOuterAlt(localctx, 1)
                self.state = 173
                self.match(WdlV1Parser.BoolLiteral)
                self.state = 174
                self.match(WdlV1Parser.EQUAL)
                self.state = 177
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [WdlV1Parser.SQUOTE, WdlV1Parser.DQUOTE]:
                    self.state = 175
                    self.string()
                    pass
                elif token in [WdlV1Parser.IntLiteral, WdlV1Parser.FloatLiteral]:
                    self.state = 176
                    self.number()
                    pass
                else:
                    raise NoViableAltException(self)

                pass
            elif token in [WdlV1Parser.DEFAULT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 179
                self.match(WdlV1Parser.DEFAULT)
                self.state = 180
                self.match(WdlV1Parser.EQUAL)
                self.state = 183
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [WdlV1Parser.SQUOTE, WdlV1Parser.DQUOTE]:
                    self.state = 181
                    self.string()
                    pass
                elif token in [WdlV1Parser.IntLiteral, WdlV1Parser.FloatLiteral]:
                    self.state = 182
                    self.number()
                    pass
                else:
                    raise NoViableAltException(self)

                pass
            elif token in [WdlV1Parser.SEP]:
                self.enterOuterAlt(localctx, 3)
                self.state = 185
                self.match(WdlV1Parser.SEP)
                self.state = 186
                self.match(WdlV1Parser.EQUAL)
                self.state = 189
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [WdlV1Parser.SQUOTE, WdlV1Parser.DQUOTE]:
                    self.state = 187
                    self.string()
                    pass
                elif token in [WdlV1Parser.IntLiteral, WdlV1Parser.FloatLiteral]:
                    self.state = 188
                    self.number()
                    pass
                else:
                    raise NoViableAltException(self)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class String_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def StringPart(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.StringPart)
            else:
                return self.getToken(WdlV1Parser.StringPart, i)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_string_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString_part" ):
                listener.enterString_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString_part" ):
                listener.exitString_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString_part" ):
                return visitor.visitString_part(self)
            else:
                return visitor.visitChildren(self)




    def string_part(self):

        localctx = WdlV1Parser.String_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_string_part)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 196
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlV1Parser.StringPart:
                self.state = 193
                self.match(WdlV1Parser.StringPart)
                self.state = 198
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class String_expr_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def StringCommandStart(self):
            return self.getToken(WdlV1Parser.StringCommandStart, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)


        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def expression_placeholder_option(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Expression_placeholder_optionContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Expression_placeholder_optionContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_string_expr_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString_expr_part" ):
                listener.enterString_expr_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString_expr_part" ):
                listener.exitString_expr_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString_expr_part" ):
                return visitor.visitString_expr_part(self)
            else:
                return visitor.visitChildren(self)




    def string_expr_part(self):

        localctx = WdlV1Parser.String_expr_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_string_expr_part)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 199
            self.match(WdlV1Parser.StringCommandStart)
            self.state = 203
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,9,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 200
                    self.expression_placeholder_option() 
                self.state = 205
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,9,self._ctx)

            self.state = 206
            self.expr()
            self.state = 207
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class String_expr_with_string_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def string_expr_part(self):
            return self.getTypedRuleContext(WdlV1Parser.String_expr_partContext,0)


        def string_part(self):
            return self.getTypedRuleContext(WdlV1Parser.String_partContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_string_expr_with_string_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString_expr_with_string_part" ):
                listener.enterString_expr_with_string_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString_expr_with_string_part" ):
                listener.exitString_expr_with_string_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString_expr_with_string_part" ):
                return visitor.visitString_expr_with_string_part(self)
            else:
                return visitor.visitChildren(self)




    def string_expr_with_string_part(self):

        localctx = WdlV1Parser.String_expr_with_string_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_string_expr_with_string_part)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 209
            self.string_expr_part()
            self.state = 210
            self.string_part()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StringContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DQUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.DQUOTE)
            else:
                return self.getToken(WdlV1Parser.DQUOTE, i)

        def string_part(self):
            return self.getTypedRuleContext(WdlV1Parser.String_partContext,0)


        def string_expr_with_string_part(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.String_expr_with_string_partContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.String_expr_with_string_partContext,i)


        def SQUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.SQUOTE)
            else:
                return self.getToken(WdlV1Parser.SQUOTE, i)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_string

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString" ):
                listener.enterString(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString" ):
                listener.exitString(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString" ):
                return visitor.visitString(self)
            else:
                return visitor.visitChildren(self)




    def string(self):

        localctx = WdlV1Parser.StringContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_string)
        self._la = 0 # Token type
        try:
            self.state = 232
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.DQUOTE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 212
                self.match(WdlV1Parser.DQUOTE)
                self.state = 213
                self.string_part()
                self.state = 217
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlV1Parser.StringCommandStart:
                    self.state = 214
                    self.string_expr_with_string_part()
                    self.state = 219
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 220
                self.match(WdlV1Parser.DQUOTE)
                pass
            elif token in [WdlV1Parser.SQUOTE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 222
                self.match(WdlV1Parser.SQUOTE)
                self.state = 223
                self.string_part()
                self.state = 227
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlV1Parser.StringCommandStart:
                    self.state = 224
                    self.string_expr_with_string_part()
                    self.state = 229
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 230
                self.match(WdlV1Parser.SQUOTE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Primitive_literalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BoolLiteral(self):
            return self.getToken(WdlV1Parser.BoolLiteral, 0)

        def number(self):
            return self.getTypedRuleContext(WdlV1Parser.NumberContext,0)


        def string(self):
            return self.getTypedRuleContext(WdlV1Parser.StringContext,0)


        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_primitive_literal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimitive_literal" ):
                listener.enterPrimitive_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimitive_literal" ):
                listener.exitPrimitive_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrimitive_literal" ):
                return visitor.visitPrimitive_literal(self)
            else:
                return visitor.visitChildren(self)




    def primitive_literal(self):

        localctx = WdlV1Parser.Primitive_literalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_primitive_literal)
        try:
            self.state = 238
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.BoolLiteral]:
                self.enterOuterAlt(localctx, 1)
                self.state = 234
                self.match(WdlV1Parser.BoolLiteral)
                pass
            elif token in [WdlV1Parser.IntLiteral, WdlV1Parser.FloatLiteral]:
                self.enterOuterAlt(localctx, 2)
                self.state = 235
                self.number()
                pass
            elif token in [WdlV1Parser.SQUOTE, WdlV1Parser.DQUOTE]:
                self.enterOuterAlt(localctx, 3)
                self.state = 236
                self.string()
                pass
            elif token in [WdlV1Parser.Identifier]:
                self.enterOuterAlt(localctx, 4)
                self.state = 237
                self.match(WdlV1Parser.Identifier)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr_infix(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infixContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = WdlV1Parser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 240
            self.expr_infix()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr_infixContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlV1Parser.RULE_expr_infix

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class Infix0Context(Expr_infixContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infixContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix0(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix0Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix0" ):
                listener.enterInfix0(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix0" ):
                listener.exitInfix0(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix0" ):
                return visitor.visitInfix0(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix(self):

        localctx = WdlV1Parser.Expr_infixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_expr_infix)
        try:
            localctx = WdlV1Parser.Infix0Context(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 242
            self.expr_infix0(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr_infix0Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlV1Parser.RULE_expr_infix0

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class Infix1Context(Expr_infix0Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix0Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix1(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix1Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix1" ):
                listener.enterInfix1(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix1" ):
                listener.exitInfix1(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix1" ):
                return visitor.visitInfix1(self)
            else:
                return visitor.visitChildren(self)


    class LorContext(Expr_infix0Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix0Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix0(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix0Context,0)

        def OR(self):
            return self.getToken(WdlV1Parser.OR, 0)
        def expr_infix1(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix1Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLor" ):
                listener.enterLor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLor" ):
                listener.exitLor(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLor" ):
                return visitor.visitLor(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix0(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlV1Parser.Expr_infix0Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 34
        self.enterRecursionRule(localctx, 34, self.RULE_expr_infix0, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            localctx = WdlV1Parser.Infix1Context(self, localctx)
            self._ctx = localctx
            _prevctx = localctx

            self.state = 245
            self.expr_infix1(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 252
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,14,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = WdlV1Parser.LorContext(self, WdlV1Parser.Expr_infix0Context(self, _parentctx, _parentState))
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix0)
                    self.state = 247
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 248
                    self.match(WdlV1Parser.OR)
                    self.state = 249
                    self.expr_infix1(0) 
                self.state = 254
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,14,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr_infix1Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlV1Parser.RULE_expr_infix1

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class Infix2Context(Expr_infix1Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix1Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix2Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix2" ):
                listener.enterInfix2(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix2" ):
                listener.exitInfix2(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix2" ):
                return visitor.visitInfix2(self)
            else:
                return visitor.visitChildren(self)


    class LandContext(Expr_infix1Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix1Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix1(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix1Context,0)

        def AND(self):
            return self.getToken(WdlV1Parser.AND, 0)
        def expr_infix2(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix2Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLand" ):
                listener.enterLand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLand" ):
                listener.exitLand(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLand" ):
                return visitor.visitLand(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix1(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlV1Parser.Expr_infix1Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 36
        self.enterRecursionRule(localctx, 36, self.RULE_expr_infix1, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            localctx = WdlV1Parser.Infix2Context(self, localctx)
            self._ctx = localctx
            _prevctx = localctx

            self.state = 256
            self.expr_infix2(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 263
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,15,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = WdlV1Parser.LandContext(self, WdlV1Parser.Expr_infix1Context(self, _parentctx, _parentState))
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix1)
                    self.state = 258
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 259
                    self.match(WdlV1Parser.AND)
                    self.state = 260
                    self.expr_infix2(0) 
                self.state = 265
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,15,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr_infix2Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlV1Parser.RULE_expr_infix2

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class EqeqContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix2Context,0)

        def EQUALITY(self):
            return self.getToken(WdlV1Parser.EQUALITY, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEqeq" ):
                listener.enterEqeq(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEqeq" ):
                listener.exitEqeq(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEqeq" ):
                return visitor.visitEqeq(self)
            else:
                return visitor.visitChildren(self)


    class LtContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix2Context,0)

        def LT(self):
            return self.getToken(WdlV1Parser.LT, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLt" ):
                listener.enterLt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLt" ):
                listener.exitLt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLt" ):
                return visitor.visitLt(self)
            else:
                return visitor.visitChildren(self)


    class Infix3Context(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix3(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix3" ):
                listener.enterInfix3(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix3" ):
                listener.exitInfix3(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix3" ):
                return visitor.visitInfix3(self)
            else:
                return visitor.visitChildren(self)


    class GteContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix2Context,0)

        def GTE(self):
            return self.getToken(WdlV1Parser.GTE, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGte" ):
                listener.enterGte(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGte" ):
                listener.exitGte(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGte" ):
                return visitor.visitGte(self)
            else:
                return visitor.visitChildren(self)


    class NeqContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix2Context,0)

        def NOTEQUAL(self):
            return self.getToken(WdlV1Parser.NOTEQUAL, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNeq" ):
                listener.enterNeq(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNeq" ):
                listener.exitNeq(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNeq" ):
                return visitor.visitNeq(self)
            else:
                return visitor.visitChildren(self)


    class LteContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix2Context,0)

        def LTE(self):
            return self.getToken(WdlV1Parser.LTE, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLte" ):
                listener.enterLte(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLte" ):
                listener.exitLte(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLte" ):
                return visitor.visitLte(self)
            else:
                return visitor.visitChildren(self)


    class GtContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix2Context,0)

        def GT(self):
            return self.getToken(WdlV1Parser.GT, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGt" ):
                listener.enterGt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGt" ):
                listener.exitGt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGt" ):
                return visitor.visitGt(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix2(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlV1Parser.Expr_infix2Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 38
        self.enterRecursionRule(localctx, 38, self.RULE_expr_infix2, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            localctx = WdlV1Parser.Infix3Context(self, localctx)
            self._ctx = localctx
            _prevctx = localctx

            self.state = 267
            self.expr_infix3(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 289
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,17,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 287
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,16,self._ctx)
                    if la_ == 1:
                        localctx = WdlV1Parser.EqeqContext(self, WdlV1Parser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 269
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 270
                        self.match(WdlV1Parser.EQUALITY)
                        self.state = 271
                        self.expr_infix3(0)
                        pass

                    elif la_ == 2:
                        localctx = WdlV1Parser.NeqContext(self, WdlV1Parser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 272
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 273
                        self.match(WdlV1Parser.NOTEQUAL)
                        self.state = 274
                        self.expr_infix3(0)
                        pass

                    elif la_ == 3:
                        localctx = WdlV1Parser.LteContext(self, WdlV1Parser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 275
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 276
                        self.match(WdlV1Parser.LTE)
                        self.state = 277
                        self.expr_infix3(0)
                        pass

                    elif la_ == 4:
                        localctx = WdlV1Parser.GteContext(self, WdlV1Parser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 278
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 279
                        self.match(WdlV1Parser.GTE)
                        self.state = 280
                        self.expr_infix3(0)
                        pass

                    elif la_ == 5:
                        localctx = WdlV1Parser.LtContext(self, WdlV1Parser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 281
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 282
                        self.match(WdlV1Parser.LT)
                        self.state = 283
                        self.expr_infix3(0)
                        pass

                    elif la_ == 6:
                        localctx = WdlV1Parser.GtContext(self, WdlV1Parser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 284
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 285
                        self.match(WdlV1Parser.GT)
                        self.state = 286
                        self.expr_infix3(0)
                        pass

             
                self.state = 291
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,17,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr_infix3Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlV1Parser.RULE_expr_infix3

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class AddContext(Expr_infix3Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix3Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix3(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix3Context,0)

        def PLUS(self):
            return self.getToken(WdlV1Parser.PLUS, 0)
        def expr_infix4(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix4Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAdd" ):
                listener.enterAdd(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAdd" ):
                listener.exitAdd(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAdd" ):
                return visitor.visitAdd(self)
            else:
                return visitor.visitChildren(self)


    class SubContext(Expr_infix3Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix3Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix3(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix3Context,0)

        def MINUS(self):
            return self.getToken(WdlV1Parser.MINUS, 0)
        def expr_infix4(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix4Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSub" ):
                listener.enterSub(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSub" ):
                listener.exitSub(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSub" ):
                return visitor.visitSub(self)
            else:
                return visitor.visitChildren(self)


    class Infix4Context(Expr_infix3Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix3Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix4(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix4Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix4" ):
                listener.enterInfix4(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix4" ):
                listener.exitInfix4(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix4" ):
                return visitor.visitInfix4(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix3(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlV1Parser.Expr_infix3Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 40
        self.enterRecursionRule(localctx, 40, self.RULE_expr_infix3, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            localctx = WdlV1Parser.Infix4Context(self, localctx)
            self._ctx = localctx
            _prevctx = localctx

            self.state = 293
            self.expr_infix4(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 303
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,19,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 301
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
                    if la_ == 1:
                        localctx = WdlV1Parser.AddContext(self, WdlV1Parser.Expr_infix3Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix3)
                        self.state = 295
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 296
                        self.match(WdlV1Parser.PLUS)
                        self.state = 297
                        self.expr_infix4(0)
                        pass

                    elif la_ == 2:
                        localctx = WdlV1Parser.SubContext(self, WdlV1Parser.Expr_infix3Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix3)
                        self.state = 298
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 299
                        self.match(WdlV1Parser.MINUS)
                        self.state = 300
                        self.expr_infix4(0)
                        pass

             
                self.state = 305
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,19,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr_infix4Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlV1Parser.RULE_expr_infix4

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class ModContext(Expr_infix4Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix4Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix4(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix4Context,0)

        def MOD(self):
            return self.getToken(WdlV1Parser.MOD, 0)
        def expr_infix5(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix5Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMod" ):
                listener.enterMod(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMod" ):
                listener.exitMod(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMod" ):
                return visitor.visitMod(self)
            else:
                return visitor.visitChildren(self)


    class MulContext(Expr_infix4Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix4Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix4(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix4Context,0)

        def STAR(self):
            return self.getToken(WdlV1Parser.STAR, 0)
        def expr_infix5(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix5Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMul" ):
                listener.enterMul(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMul" ):
                listener.exitMul(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMul" ):
                return visitor.visitMul(self)
            else:
                return visitor.visitChildren(self)


    class DivideContext(Expr_infix4Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix4Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix4(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix4Context,0)

        def DIVIDE(self):
            return self.getToken(WdlV1Parser.DIVIDE, 0)
        def expr_infix5(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix5Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDivide" ):
                listener.enterDivide(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDivide" ):
                listener.exitDivide(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDivide" ):
                return visitor.visitDivide(self)
            else:
                return visitor.visitChildren(self)


    class Infix5Context(Expr_infix4Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_infix4Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix5(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_infix5Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix5" ):
                listener.enterInfix5(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix5" ):
                listener.exitInfix5(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix5" ):
                return visitor.visitInfix5(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix4(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlV1Parser.Expr_infix4Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 42
        self.enterRecursionRule(localctx, 42, self.RULE_expr_infix4, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            localctx = WdlV1Parser.Infix5Context(self, localctx)
            self._ctx = localctx
            _prevctx = localctx

            self.state = 307
            self.expr_infix5()
            self._ctx.stop = self._input.LT(-1)
            self.state = 320
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,21,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 318
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,20,self._ctx)
                    if la_ == 1:
                        localctx = WdlV1Parser.MulContext(self, WdlV1Parser.Expr_infix4Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix4)
                        self.state = 309
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 310
                        self.match(WdlV1Parser.STAR)
                        self.state = 311
                        self.expr_infix5()
                        pass

                    elif la_ == 2:
                        localctx = WdlV1Parser.DivideContext(self, WdlV1Parser.Expr_infix4Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix4)
                        self.state = 312
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 313
                        self.match(WdlV1Parser.DIVIDE)
                        self.state = 314
                        self.expr_infix5()
                        pass

                    elif la_ == 3:
                        localctx = WdlV1Parser.ModContext(self, WdlV1Parser.Expr_infix4Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix4)
                        self.state = 315
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 316
                        self.match(WdlV1Parser.MOD)
                        self.state = 317
                        self.expr_infix5()
                        pass

             
                self.state = 322
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,21,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr_infix5Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr_core(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_coreContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_expr_infix5

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr_infix5" ):
                listener.enterExpr_infix5(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr_infix5" ):
                listener.exitExpr_infix5(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr_infix5" ):
                return visitor.visitExpr_infix5(self)
            else:
                return visitor.visitChildren(self)




    def expr_infix5(self):

        localctx = WdlV1Parser.Expr_infix5Context(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_expr_infix5)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 323
            self.expr_core(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr_coreContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlV1Parser.RULE_expr_core

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class Pair_literalContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LPAREN(self):
            return self.getToken(WdlV1Parser.LPAREN, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.ExprContext,i)

        def COMMA(self):
            return self.getToken(WdlV1Parser.COMMA, 0)
        def RPAREN(self):
            return self.getToken(WdlV1Parser.RPAREN, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPair_literal" ):
                listener.enterPair_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPair_literal" ):
                listener.exitPair_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPair_literal" ):
                return visitor.visitPair_literal(self)
            else:
                return visitor.visitChildren(self)


    class UnarysignedContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)

        def PLUS(self):
            return self.getToken(WdlV1Parser.PLUS, 0)
        def MINUS(self):
            return self.getToken(WdlV1Parser.MINUS, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnarysigned" ):
                listener.enterUnarysigned(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnarysigned" ):
                listener.exitUnarysigned(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnarysigned" ):
                return visitor.visitUnarysigned(self)
            else:
                return visitor.visitChildren(self)


    class ApplyContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)
        def LPAREN(self):
            return self.getToken(WdlV1Parser.LPAREN, 0)
        def RPAREN(self):
            return self.getToken(WdlV1Parser.RPAREN, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.ExprContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.COMMA)
            else:
                return self.getToken(WdlV1Parser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterApply" ):
                listener.enterApply(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitApply" ):
                listener.exitApply(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitApply" ):
                return visitor.visitApply(self)
            else:
                return visitor.visitChildren(self)


    class Expression_groupContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LPAREN(self):
            return self.getToken(WdlV1Parser.LPAREN, 0)
        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)

        def RPAREN(self):
            return self.getToken(WdlV1Parser.RPAREN, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression_group" ):
                listener.enterExpression_group(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression_group" ):
                listener.exitExpression_group(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression_group" ):
                return visitor.visitExpression_group(self)
            else:
                return visitor.visitChildren(self)


    class PrimitivesContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def primitive_literal(self):
            return self.getTypedRuleContext(WdlV1Parser.Primitive_literalContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimitives" ):
                listener.enterPrimitives(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimitives" ):
                listener.exitPrimitives(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrimitives" ):
                return visitor.visitPrimitives(self)
            else:
                return visitor.visitChildren(self)


    class Left_nameContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLeft_name" ):
                listener.enterLeft_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLeft_name" ):
                listener.exitLeft_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLeft_name" ):
                return visitor.visitLeft_name(self)
            else:
                return visitor.visitChildren(self)


    class AtContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_core(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_coreContext,0)

        def LBRACK(self):
            return self.getToken(WdlV1Parser.LBRACK, 0)
        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)

        def RBRACK(self):
            return self.getToken(WdlV1Parser.RBRACK, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAt" ):
                listener.enterAt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAt" ):
                listener.exitAt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAt" ):
                return visitor.visitAt(self)
            else:
                return visitor.visitChildren(self)


    class NegateContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NOT(self):
            return self.getToken(WdlV1Parser.NOT, 0)
        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNegate" ):
                listener.enterNegate(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNegate" ):
                listener.exitNegate(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNegate" ):
                return visitor.visitNegate(self)
            else:
                return visitor.visitChildren(self)


    class Map_literalContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)
        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.ExprContext,i)

        def COLON(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.COLON)
            else:
                return self.getToken(WdlV1Parser.COLON, i)
        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.COMMA)
            else:
                return self.getToken(WdlV1Parser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMap_literal" ):
                listener.enterMap_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMap_literal" ):
                listener.exitMap_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMap_literal" ):
                return visitor.visitMap_literal(self)
            else:
                return visitor.visitChildren(self)


    class IfthenelseContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IF(self):
            return self.getToken(WdlV1Parser.IF, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.ExprContext,i)

        def THEN(self):
            return self.getToken(WdlV1Parser.THEN, 0)
        def ELSE(self):
            return self.getToken(WdlV1Parser.ELSE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIfthenelse" ):
                listener.enterIfthenelse(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIfthenelse" ):
                listener.exitIfthenelse(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfthenelse" ):
                return visitor.visitIfthenelse(self)
            else:
                return visitor.visitChildren(self)


    class Get_nameContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_core(self):
            return self.getTypedRuleContext(WdlV1Parser.Expr_coreContext,0)

        def DOT(self):
            return self.getToken(WdlV1Parser.DOT, 0)
        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGet_name" ):
                listener.enterGet_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGet_name" ):
                listener.exitGet_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGet_name" ):
                return visitor.visitGet_name(self)
            else:
                return visitor.visitChildren(self)


    class Object_literalContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def OBJECT_LITERAL(self):
            return self.getToken(WdlV1Parser.OBJECT_LITERAL, 0)
        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)
        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)
        def Identifier(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.Identifier)
            else:
                return self.getToken(WdlV1Parser.Identifier, i)
        def COLON(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.COLON)
            else:
                return self.getToken(WdlV1Parser.COLON, i)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.ExprContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.COMMA)
            else:
                return self.getToken(WdlV1Parser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObject_literal" ):
                listener.enterObject_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObject_literal" ):
                listener.exitObject_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitObject_literal" ):
                return visitor.visitObject_literal(self)
            else:
                return visitor.visitChildren(self)


    class Array_literalContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LBRACK(self):
            return self.getToken(WdlV1Parser.LBRACK, 0)
        def RBRACK(self):
            return self.getToken(WdlV1Parser.RBRACK, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.ExprContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.COMMA)
            else:
                return self.getToken(WdlV1Parser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArray_literal" ):
                listener.enterArray_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArray_literal" ):
                listener.exitArray_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArray_literal" ):
                return visitor.visitArray_literal(self)
            else:
                return visitor.visitChildren(self)



    def expr_core(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlV1Parser.Expr_coreContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 46
        self.enterRecursionRule(localctx, 46, self.RULE_expr_core, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 429
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,34,self._ctx)
            if la_ == 1:
                localctx = WdlV1Parser.ApplyContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 326
                self.match(WdlV1Parser.Identifier)
                self.state = 327
                self.match(WdlV1Parser.LPAREN)
                self.state = 339
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if ((((_la - 9)) & ~0x3f) == 0 and ((1 << (_la - 9)) & ((1 << (WdlV1Parser.IF - 9)) | (1 << (WdlV1Parser.OBJECT_LITERAL - 9)) | (1 << (WdlV1Parser.IntLiteral - 9)) | (1 << (WdlV1Parser.FloatLiteral - 9)) | (1 << (WdlV1Parser.BoolLiteral - 9)) | (1 << (WdlV1Parser.LPAREN - 9)) | (1 << (WdlV1Parser.LBRACE - 9)) | (1 << (WdlV1Parser.LBRACK - 9)) | (1 << (WdlV1Parser.PLUS - 9)) | (1 << (WdlV1Parser.MINUS - 9)) | (1 << (WdlV1Parser.NOT - 9)) | (1 << (WdlV1Parser.SQUOTE - 9)) | (1 << (WdlV1Parser.DQUOTE - 9)) | (1 << (WdlV1Parser.Identifier - 9)))) != 0):
                    self.state = 328
                    self.expr()
                    self.state = 333
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,22,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 329
                            self.match(WdlV1Parser.COMMA)
                            self.state = 330
                            self.expr() 
                        self.state = 335
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,22,self._ctx)

                    self.state = 337
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if _la==WdlV1Parser.COMMA:
                        self.state = 336
                        self.match(WdlV1Parser.COMMA)




                self.state = 341
                self.match(WdlV1Parser.RPAREN)
                pass

            elif la_ == 2:
                localctx = WdlV1Parser.Array_literalContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 342
                self.match(WdlV1Parser.LBRACK)
                self.state = 356
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while ((((_la - 9)) & ~0x3f) == 0 and ((1 << (_la - 9)) & ((1 << (WdlV1Parser.IF - 9)) | (1 << (WdlV1Parser.OBJECT_LITERAL - 9)) | (1 << (WdlV1Parser.IntLiteral - 9)) | (1 << (WdlV1Parser.FloatLiteral - 9)) | (1 << (WdlV1Parser.BoolLiteral - 9)) | (1 << (WdlV1Parser.LPAREN - 9)) | (1 << (WdlV1Parser.LBRACE - 9)) | (1 << (WdlV1Parser.LBRACK - 9)) | (1 << (WdlV1Parser.PLUS - 9)) | (1 << (WdlV1Parser.MINUS - 9)) | (1 << (WdlV1Parser.NOT - 9)) | (1 << (WdlV1Parser.SQUOTE - 9)) | (1 << (WdlV1Parser.DQUOTE - 9)) | (1 << (WdlV1Parser.Identifier - 9)))) != 0):
                    self.state = 343
                    self.expr()
                    self.state = 348
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,25,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 344
                            self.match(WdlV1Parser.COMMA)
                            self.state = 345
                            self.expr() 
                        self.state = 350
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,25,self._ctx)

                    self.state = 352
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if _la==WdlV1Parser.COMMA:
                        self.state = 351
                        self.match(WdlV1Parser.COMMA)


                    self.state = 358
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 359
                self.match(WdlV1Parser.RBRACK)
                pass

            elif la_ == 3:
                localctx = WdlV1Parser.Pair_literalContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 360
                self.match(WdlV1Parser.LPAREN)
                self.state = 361
                self.expr()
                self.state = 362
                self.match(WdlV1Parser.COMMA)
                self.state = 363
                self.expr()
                self.state = 364
                self.match(WdlV1Parser.RPAREN)
                pass

            elif la_ == 4:
                localctx = WdlV1Parser.Map_literalContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 366
                self.match(WdlV1Parser.LBRACE)
                self.state = 385
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while ((((_la - 9)) & ~0x3f) == 0 and ((1 << (_la - 9)) & ((1 << (WdlV1Parser.IF - 9)) | (1 << (WdlV1Parser.OBJECT_LITERAL - 9)) | (1 << (WdlV1Parser.IntLiteral - 9)) | (1 << (WdlV1Parser.FloatLiteral - 9)) | (1 << (WdlV1Parser.BoolLiteral - 9)) | (1 << (WdlV1Parser.LPAREN - 9)) | (1 << (WdlV1Parser.LBRACE - 9)) | (1 << (WdlV1Parser.LBRACK - 9)) | (1 << (WdlV1Parser.PLUS - 9)) | (1 << (WdlV1Parser.MINUS - 9)) | (1 << (WdlV1Parser.NOT - 9)) | (1 << (WdlV1Parser.SQUOTE - 9)) | (1 << (WdlV1Parser.DQUOTE - 9)) | (1 << (WdlV1Parser.Identifier - 9)))) != 0):
                    self.state = 367
                    self.expr()
                    self.state = 368
                    self.match(WdlV1Parser.COLON)
                    self.state = 369
                    self.expr()
                    self.state = 377
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,28,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 370
                            self.match(WdlV1Parser.COMMA)
                            self.state = 371
                            self.expr()
                            self.state = 372
                            self.match(WdlV1Parser.COLON)
                            self.state = 373
                            self.expr() 
                        self.state = 379
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,28,self._ctx)

                    self.state = 381
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if _la==WdlV1Parser.COMMA:
                        self.state = 380
                        self.match(WdlV1Parser.COMMA)


                    self.state = 387
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 388
                self.match(WdlV1Parser.RBRACE)
                pass

            elif la_ == 5:
                localctx = WdlV1Parser.Object_literalContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 389
                self.match(WdlV1Parser.OBJECT_LITERAL)
                self.state = 390
                self.match(WdlV1Parser.LBRACE)
                self.state = 408
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlV1Parser.Identifier:
                    self.state = 391
                    self.match(WdlV1Parser.Identifier)
                    self.state = 392
                    self.match(WdlV1Parser.COLON)
                    self.state = 393
                    self.expr()
                    self.state = 400
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,31,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 394
                            self.match(WdlV1Parser.COMMA)
                            self.state = 395
                            self.match(WdlV1Parser.Identifier)
                            self.state = 396
                            self.match(WdlV1Parser.COLON)
                            self.state = 397
                            self.expr() 
                        self.state = 402
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,31,self._ctx)

                    self.state = 404
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if _la==WdlV1Parser.COMMA:
                        self.state = 403
                        self.match(WdlV1Parser.COMMA)


                    self.state = 410
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 411
                self.match(WdlV1Parser.RBRACE)
                pass

            elif la_ == 6:
                localctx = WdlV1Parser.IfthenelseContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 412
                self.match(WdlV1Parser.IF)
                self.state = 413
                self.expr()
                self.state = 414
                self.match(WdlV1Parser.THEN)
                self.state = 415
                self.expr()
                self.state = 416
                self.match(WdlV1Parser.ELSE)
                self.state = 417
                self.expr()
                pass

            elif la_ == 7:
                localctx = WdlV1Parser.Expression_groupContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 419
                self.match(WdlV1Parser.LPAREN)
                self.state = 420
                self.expr()
                self.state = 421
                self.match(WdlV1Parser.RPAREN)
                pass

            elif la_ == 8:
                localctx = WdlV1Parser.NegateContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 423
                self.match(WdlV1Parser.NOT)
                self.state = 424
                self.expr()
                pass

            elif la_ == 9:
                localctx = WdlV1Parser.UnarysignedContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 425
                _la = self._input.LA(1)
                if not(_la==WdlV1Parser.PLUS or _la==WdlV1Parser.MINUS):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 426
                self.expr()
                pass

            elif la_ == 10:
                localctx = WdlV1Parser.PrimitivesContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 427
                self.primitive_literal()
                pass

            elif la_ == 11:
                localctx = WdlV1Parser.Left_nameContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 428
                self.match(WdlV1Parser.Identifier)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 441
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,36,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 439
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,35,self._ctx)
                    if la_ == 1:
                        localctx = WdlV1Parser.AtContext(self, WdlV1Parser.Expr_coreContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_core)
                        self.state = 431
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 432
                        self.match(WdlV1Parser.LBRACK)
                        self.state = 433
                        self.expr()
                        self.state = 434
                        self.match(WdlV1Parser.RBRACK)
                        pass

                    elif la_ == 2:
                        localctx = WdlV1Parser.Get_nameContext(self, WdlV1Parser.Expr_coreContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_core)
                        self.state = 436
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 437
                        self.match(WdlV1Parser.DOT)
                        self.state = 438
                        self.match(WdlV1Parser.Identifier)
                        pass

             
                self.state = 443
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,36,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class VersionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VERSION(self):
            return self.getToken(WdlV1Parser.VERSION, 0)

        def ReleaseVersion(self):
            return self.getToken(WdlV1Parser.ReleaseVersion, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_version

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVersion" ):
                listener.enterVersion(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVersion" ):
                listener.exitVersion(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVersion" ):
                return visitor.visitVersion(self)
            else:
                return visitor.visitChildren(self)




    def version(self):

        localctx = WdlV1Parser.VersionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_version)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 444
            self.match(WdlV1Parser.VERSION)
            self.state = 445
            self.match(WdlV1Parser.ReleaseVersion)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_aliasContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ALIAS(self):
            return self.getToken(WdlV1Parser.ALIAS, 0)

        def Identifier(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.Identifier)
            else:
                return self.getToken(WdlV1Parser.Identifier, i)

        def AS(self):
            return self.getToken(WdlV1Parser.AS, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_import_alias

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_alias" ):
                listener.enterImport_alias(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_alias" ):
                listener.exitImport_alias(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImport_alias" ):
                return visitor.visitImport_alias(self)
            else:
                return visitor.visitChildren(self)




    def import_alias(self):

        localctx = WdlV1Parser.Import_aliasContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_import_alias)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 447
            self.match(WdlV1Parser.ALIAS)
            self.state = 448
            self.match(WdlV1Parser.Identifier)
            self.state = 449
            self.match(WdlV1Parser.AS)
            self.state = 450
            self.match(WdlV1Parser.Identifier)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_asContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def AS(self):
            return self.getToken(WdlV1Parser.AS, 0)

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_import_as

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_as" ):
                listener.enterImport_as(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_as" ):
                listener.exitImport_as(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImport_as" ):
                return visitor.visitImport_as(self)
            else:
                return visitor.visitChildren(self)




    def import_as(self):

        localctx = WdlV1Parser.Import_asContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_import_as)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 452
            self.match(WdlV1Parser.AS)
            self.state = 453
            self.match(WdlV1Parser.Identifier)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_docContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IMPORT(self):
            return self.getToken(WdlV1Parser.IMPORT, 0)

        def string(self):
            return self.getTypedRuleContext(WdlV1Parser.StringContext,0)


        def import_as(self):
            return self.getTypedRuleContext(WdlV1Parser.Import_asContext,0)


        def import_alias(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Import_aliasContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Import_aliasContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_import_doc

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_doc" ):
                listener.enterImport_doc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_doc" ):
                listener.exitImport_doc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImport_doc" ):
                return visitor.visitImport_doc(self)
            else:
                return visitor.visitChildren(self)




    def import_doc(self):

        localctx = WdlV1Parser.Import_docContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_import_doc)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 455
            self.match(WdlV1Parser.IMPORT)
            self.state = 456
            self.string()
            self.state = 458
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlV1Parser.AS:
                self.state = 457
                self.import_as()


            self.state = 463
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlV1Parser.ALIAS:
                self.state = 460
                self.import_alias()
                self.state = 465
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StructContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRUCT(self):
            return self.getToken(WdlV1Parser.STRUCT, 0)

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def unbound_decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Unbound_declsContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Unbound_declsContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_struct

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStruct" ):
                listener.enterStruct(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStruct" ):
                listener.exitStruct(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStruct" ):
                return visitor.visitStruct(self)
            else:
                return visitor.visitChildren(self)




    def struct(self):

        localctx = WdlV1Parser.StructContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_struct)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 466
            self.match(WdlV1Parser.STRUCT)
            self.state = 467
            self.match(WdlV1Parser.Identifier)
            self.state = 468
            self.match(WdlV1Parser.LBRACE)
            self.state = 472
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 21)) & ~0x3f) == 0 and ((1 << (_la - 21)) & ((1 << (WdlV1Parser.BOOLEAN - 21)) | (1 << (WdlV1Parser.INT - 21)) | (1 << (WdlV1Parser.FLOAT - 21)) | (1 << (WdlV1Parser.STRING - 21)) | (1 << (WdlV1Parser.FILE - 21)) | (1 << (WdlV1Parser.ARRAY - 21)) | (1 << (WdlV1Parser.MAP - 21)) | (1 << (WdlV1Parser.PAIR - 21)) | (1 << (WdlV1Parser.OBJECT - 21)) | (1 << (WdlV1Parser.Identifier - 21)))) != 0):
                self.state = 469
                self.unbound_decls()
                self.state = 474
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 475
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meta_valueContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MetaNull(self):
            return self.getToken(WdlV1Parser.MetaNull, 0)

        def MetaBool(self):
            return self.getToken(WdlV1Parser.MetaBool, 0)

        def MetaInt(self):
            return self.getToken(WdlV1Parser.MetaInt, 0)

        def MetaFloat(self):
            return self.getToken(WdlV1Parser.MetaFloat, 0)

        def meta_string(self):
            return self.getTypedRuleContext(WdlV1Parser.Meta_stringContext,0)


        def meta_object(self):
            return self.getTypedRuleContext(WdlV1Parser.Meta_objectContext,0)


        def meta_array(self):
            return self.getTypedRuleContext(WdlV1Parser.Meta_arrayContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_meta_value

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta_value" ):
                listener.enterMeta_value(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta_value" ):
                listener.exitMeta_value(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta_value" ):
                return visitor.visitMeta_value(self)
            else:
                return visitor.visitChildren(self)




    def meta_value(self):

        localctx = WdlV1Parser.Meta_valueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_meta_value)
        try:
            self.state = 484
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.MetaNull]:
                self.enterOuterAlt(localctx, 1)
                self.state = 477
                self.match(WdlV1Parser.MetaNull)
                pass
            elif token in [WdlV1Parser.MetaBool]:
                self.enterOuterAlt(localctx, 2)
                self.state = 478
                self.match(WdlV1Parser.MetaBool)
                pass
            elif token in [WdlV1Parser.MetaInt]:
                self.enterOuterAlt(localctx, 3)
                self.state = 479
                self.match(WdlV1Parser.MetaInt)
                pass
            elif token in [WdlV1Parser.MetaFloat]:
                self.enterOuterAlt(localctx, 4)
                self.state = 480
                self.match(WdlV1Parser.MetaFloat)
                pass
            elif token in [WdlV1Parser.MetaSquote, WdlV1Parser.MetaDquote]:
                self.enterOuterAlt(localctx, 5)
                self.state = 481
                self.meta_string()
                pass
            elif token in [WdlV1Parser.MetaEmptyObject, WdlV1Parser.MetaLbrace]:
                self.enterOuterAlt(localctx, 6)
                self.state = 482
                self.meta_object()
                pass
            elif token in [WdlV1Parser.MetaEmptyArray, WdlV1Parser.MetaLbrack]:
                self.enterOuterAlt(localctx, 7)
                self.state = 483
                self.meta_array()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meta_string_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MetaStringPart(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.MetaStringPart)
            else:
                return self.getToken(WdlV1Parser.MetaStringPart, i)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_meta_string_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta_string_part" ):
                listener.enterMeta_string_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta_string_part" ):
                listener.exitMeta_string_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta_string_part" ):
                return visitor.visitMeta_string_part(self)
            else:
                return visitor.visitChildren(self)




    def meta_string_part(self):

        localctx = WdlV1Parser.Meta_string_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_meta_string_part)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 489
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlV1Parser.MetaStringPart:
                self.state = 486
                self.match(WdlV1Parser.MetaStringPart)
                self.state = 491
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meta_stringContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MetaDquote(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.MetaDquote)
            else:
                return self.getToken(WdlV1Parser.MetaDquote, i)

        def meta_string_part(self):
            return self.getTypedRuleContext(WdlV1Parser.Meta_string_partContext,0)


        def MetaSquote(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.MetaSquote)
            else:
                return self.getToken(WdlV1Parser.MetaSquote, i)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_meta_string

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta_string" ):
                listener.enterMeta_string(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta_string" ):
                listener.exitMeta_string(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta_string" ):
                return visitor.visitMeta_string(self)
            else:
                return visitor.visitChildren(self)




    def meta_string(self):

        localctx = WdlV1Parser.Meta_stringContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_meta_string)
        try:
            self.state = 500
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.MetaDquote]:
                self.enterOuterAlt(localctx, 1)
                self.state = 492
                self.match(WdlV1Parser.MetaDquote)
                self.state = 493
                self.meta_string_part()
                self.state = 494
                self.match(WdlV1Parser.MetaDquote)
                pass
            elif token in [WdlV1Parser.MetaSquote]:
                self.enterOuterAlt(localctx, 2)
                self.state = 496
                self.match(WdlV1Parser.MetaSquote)
                self.state = 497
                self.meta_string_part()
                self.state = 498
                self.match(WdlV1Parser.MetaSquote)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meta_arrayContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MetaEmptyArray(self):
            return self.getToken(WdlV1Parser.MetaEmptyArray, 0)

        def MetaLbrack(self):
            return self.getToken(WdlV1Parser.MetaLbrack, 0)

        def meta_value(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Meta_valueContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Meta_valueContext,i)


        def MetaArrayCommaRbrack(self):
            return self.getToken(WdlV1Parser.MetaArrayCommaRbrack, 0)

        def MetaRbrack(self):
            return self.getToken(WdlV1Parser.MetaRbrack, 0)

        def MetaArrayComma(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.MetaArrayComma)
            else:
                return self.getToken(WdlV1Parser.MetaArrayComma, i)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_meta_array

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta_array" ):
                listener.enterMeta_array(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta_array" ):
                listener.exitMeta_array(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta_array" ):
                return visitor.visitMeta_array(self)
            else:
                return visitor.visitChildren(self)




    def meta_array(self):

        localctx = WdlV1Parser.Meta_arrayContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_meta_array)
        self._la = 0 # Token type
        try:
            self.state = 514
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.MetaEmptyArray]:
                self.enterOuterAlt(localctx, 1)
                self.state = 502
                self.match(WdlV1Parser.MetaEmptyArray)
                pass
            elif token in [WdlV1Parser.MetaLbrack]:
                self.enterOuterAlt(localctx, 2)
                self.state = 503
                self.match(WdlV1Parser.MetaLbrack)
                self.state = 504
                self.meta_value()
                self.state = 509
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlV1Parser.MetaArrayComma:
                    self.state = 505
                    self.match(WdlV1Parser.MetaArrayComma)
                    self.state = 506
                    self.meta_value()
                    self.state = 511
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 512
                _la = self._input.LA(1)
                if not(_la==WdlV1Parser.MetaArrayCommaRbrack or _la==WdlV1Parser.MetaRbrack):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meta_objectContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MetaEmptyObject(self):
            return self.getToken(WdlV1Parser.MetaEmptyObject, 0)

        def MetaLbrace(self):
            return self.getToken(WdlV1Parser.MetaLbrace, 0)

        def meta_object_kv(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Meta_object_kvContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Meta_object_kvContext,i)


        def MetaObjectCommaRbrace(self):
            return self.getToken(WdlV1Parser.MetaObjectCommaRbrace, 0)

        def MetaRbrace(self):
            return self.getToken(WdlV1Parser.MetaRbrace, 0)

        def MetaObjectComma(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.MetaObjectComma)
            else:
                return self.getToken(WdlV1Parser.MetaObjectComma, i)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_meta_object

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta_object" ):
                listener.enterMeta_object(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta_object" ):
                listener.exitMeta_object(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta_object" ):
                return visitor.visitMeta_object(self)
            else:
                return visitor.visitChildren(self)




    def meta_object(self):

        localctx = WdlV1Parser.Meta_objectContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_meta_object)
        self._la = 0 # Token type
        try:
            self.state = 528
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.MetaEmptyObject]:
                self.enterOuterAlt(localctx, 1)
                self.state = 516
                self.match(WdlV1Parser.MetaEmptyObject)
                pass
            elif token in [WdlV1Parser.MetaLbrace]:
                self.enterOuterAlt(localctx, 2)
                self.state = 517
                self.match(WdlV1Parser.MetaLbrace)
                self.state = 518
                self.meta_object_kv()
                self.state = 523
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlV1Parser.MetaObjectComma:
                    self.state = 519
                    self.match(WdlV1Parser.MetaObjectComma)
                    self.state = 520
                    self.meta_object_kv()
                    self.state = 525
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 526
                _la = self._input.LA(1)
                if not(_la==WdlV1Parser.MetaObjectCommaRbrace or _la==WdlV1Parser.MetaRbrace):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meta_object_kvContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MetaObjectIdentifier(self):
            return self.getToken(WdlV1Parser.MetaObjectIdentifier, 0)

        def MetaObjectColon(self):
            return self.getToken(WdlV1Parser.MetaObjectColon, 0)

        def meta_value(self):
            return self.getTypedRuleContext(WdlV1Parser.Meta_valueContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_meta_object_kv

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta_object_kv" ):
                listener.enterMeta_object_kv(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta_object_kv" ):
                listener.exitMeta_object_kv(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta_object_kv" ):
                return visitor.visitMeta_object_kv(self)
            else:
                return visitor.visitChildren(self)




    def meta_object_kv(self):

        localctx = WdlV1Parser.Meta_object_kvContext(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_meta_object_kv)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 530
            self.match(WdlV1Parser.MetaObjectIdentifier)
            self.state = 531
            self.match(WdlV1Parser.MetaObjectColon)
            self.state = 532
            self.meta_value()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meta_kvContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MetaIdentifier(self):
            return self.getToken(WdlV1Parser.MetaIdentifier, 0)

        def MetaColon(self):
            return self.getToken(WdlV1Parser.MetaColon, 0)

        def meta_value(self):
            return self.getTypedRuleContext(WdlV1Parser.Meta_valueContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_meta_kv

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta_kv" ):
                listener.enterMeta_kv(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta_kv" ):
                listener.exitMeta_kv(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta_kv" ):
                return visitor.visitMeta_kv(self)
            else:
                return visitor.visitChildren(self)




    def meta_kv(self):

        localctx = WdlV1Parser.Meta_kvContext(self, self._ctx, self.state)
        self.enterRule(localctx, 70, self.RULE_meta_kv)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 534
            self.match(WdlV1Parser.MetaIdentifier)
            self.state = 535
            self.match(WdlV1Parser.MetaColon)
            self.state = 536
            self.meta_value()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Parameter_metaContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PARAMETERMETA(self):
            return self.getToken(WdlV1Parser.PARAMETERMETA, 0)

        def BeginMeta(self):
            return self.getToken(WdlV1Parser.BeginMeta, 0)

        def EndMeta(self):
            return self.getToken(WdlV1Parser.EndMeta, 0)

        def meta_kv(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Meta_kvContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Meta_kvContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_parameter_meta

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameter_meta" ):
                listener.enterParameter_meta(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameter_meta" ):
                listener.exitParameter_meta(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameter_meta" ):
                return visitor.visitParameter_meta(self)
            else:
                return visitor.visitChildren(self)




    def parameter_meta(self):

        localctx = WdlV1Parser.Parameter_metaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 72, self.RULE_parameter_meta)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 538
            self.match(WdlV1Parser.PARAMETERMETA)
            self.state = 539
            self.match(WdlV1Parser.BeginMeta)
            self.state = 543
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlV1Parser.MetaIdentifier:
                self.state = 540
                self.meta_kv()
                self.state = 545
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 546
            self.match(WdlV1Parser.EndMeta)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MetaContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def META(self):
            return self.getToken(WdlV1Parser.META, 0)

        def BeginMeta(self):
            return self.getToken(WdlV1Parser.BeginMeta, 0)

        def EndMeta(self):
            return self.getToken(WdlV1Parser.EndMeta, 0)

        def meta_kv(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Meta_kvContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Meta_kvContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_meta

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta" ):
                listener.enterMeta(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta" ):
                listener.exitMeta(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta" ):
                return visitor.visitMeta(self)
            else:
                return visitor.visitChildren(self)




    def meta(self):

        localctx = WdlV1Parser.MetaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_meta)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 548
            self.match(WdlV1Parser.META)
            self.state = 549
            self.match(WdlV1Parser.BeginMeta)
            self.state = 553
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlV1Parser.MetaIdentifier:
                self.state = 550
                self.meta_kv()
                self.state = 555
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 556
            self.match(WdlV1Parser.EndMeta)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_runtime_kvContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def COLON(self):
            return self.getToken(WdlV1Parser.COLON, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_task_runtime_kv

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_runtime_kv" ):
                listener.enterTask_runtime_kv(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_runtime_kv" ):
                listener.exitTask_runtime_kv(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_runtime_kv" ):
                return visitor.visitTask_runtime_kv(self)
            else:
                return visitor.visitChildren(self)




    def task_runtime_kv(self):

        localctx = WdlV1Parser.Task_runtime_kvContext(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_task_runtime_kv)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 558
            self.match(WdlV1Parser.Identifier)
            self.state = 559
            self.match(WdlV1Parser.COLON)
            self.state = 560
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_runtimeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RUNTIME(self):
            return self.getToken(WdlV1Parser.RUNTIME, 0)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def task_runtime_kv(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Task_runtime_kvContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Task_runtime_kvContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_task_runtime

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_runtime" ):
                listener.enterTask_runtime(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_runtime" ):
                listener.exitTask_runtime(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_runtime" ):
                return visitor.visitTask_runtime(self)
            else:
                return visitor.visitChildren(self)




    def task_runtime(self):

        localctx = WdlV1Parser.Task_runtimeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_task_runtime)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 562
            self.match(WdlV1Parser.RUNTIME)
            self.state = 563
            self.match(WdlV1Parser.LBRACE)
            self.state = 567
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlV1Parser.Identifier:
                self.state = 564
                self.task_runtime_kv()
                self.state = 569
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 570
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_inputContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INPUT(self):
            return self.getToken(WdlV1Parser.INPUT, 0)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def any_decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Any_declsContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Any_declsContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_task_input

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_input" ):
                listener.enterTask_input(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_input" ):
                listener.exitTask_input(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_input" ):
                return visitor.visitTask_input(self)
            else:
                return visitor.visitChildren(self)




    def task_input(self):

        localctx = WdlV1Parser.Task_inputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 80, self.RULE_task_input)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 572
            self.match(WdlV1Parser.INPUT)
            self.state = 573
            self.match(WdlV1Parser.LBRACE)
            self.state = 577
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 21)) & ~0x3f) == 0 and ((1 << (_la - 21)) & ((1 << (WdlV1Parser.BOOLEAN - 21)) | (1 << (WdlV1Parser.INT - 21)) | (1 << (WdlV1Parser.FLOAT - 21)) | (1 << (WdlV1Parser.STRING - 21)) | (1 << (WdlV1Parser.FILE - 21)) | (1 << (WdlV1Parser.ARRAY - 21)) | (1 << (WdlV1Parser.MAP - 21)) | (1 << (WdlV1Parser.PAIR - 21)) | (1 << (WdlV1Parser.OBJECT - 21)) | (1 << (WdlV1Parser.Identifier - 21)))) != 0):
                self.state = 574
                self.any_decls()
                self.state = 579
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 580
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_outputContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OUTPUT(self):
            return self.getToken(WdlV1Parser.OUTPUT, 0)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def bound_decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Bound_declsContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Bound_declsContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_task_output

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_output" ):
                listener.enterTask_output(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_output" ):
                listener.exitTask_output(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_output" ):
                return visitor.visitTask_output(self)
            else:
                return visitor.visitChildren(self)




    def task_output(self):

        localctx = WdlV1Parser.Task_outputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 82, self.RULE_task_output)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 582
            self.match(WdlV1Parser.OUTPUT)
            self.state = 583
            self.match(WdlV1Parser.LBRACE)
            self.state = 587
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 21)) & ~0x3f) == 0 and ((1 << (_la - 21)) & ((1 << (WdlV1Parser.BOOLEAN - 21)) | (1 << (WdlV1Parser.INT - 21)) | (1 << (WdlV1Parser.FLOAT - 21)) | (1 << (WdlV1Parser.STRING - 21)) | (1 << (WdlV1Parser.FILE - 21)) | (1 << (WdlV1Parser.ARRAY - 21)) | (1 << (WdlV1Parser.MAP - 21)) | (1 << (WdlV1Parser.PAIR - 21)) | (1 << (WdlV1Parser.OBJECT - 21)) | (1 << (WdlV1Parser.Identifier - 21)))) != 0):
                self.state = 584
                self.bound_decls()
                self.state = 589
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 590
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_command_string_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CommandStringPart(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.CommandStringPart)
            else:
                return self.getToken(WdlV1Parser.CommandStringPart, i)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_task_command_string_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_command_string_part" ):
                listener.enterTask_command_string_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_command_string_part" ):
                listener.exitTask_command_string_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_command_string_part" ):
                return visitor.visitTask_command_string_part(self)
            else:
                return visitor.visitChildren(self)




    def task_command_string_part(self):

        localctx = WdlV1Parser.Task_command_string_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 84, self.RULE_task_command_string_part)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 595
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlV1Parser.CommandStringPart:
                self.state = 592
                self.match(WdlV1Parser.CommandStringPart)
                self.state = 597
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_command_expr_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def StringCommandStart(self):
            return self.getToken(WdlV1Parser.StringCommandStart, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)


        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def expression_placeholder_option(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Expression_placeholder_optionContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Expression_placeholder_optionContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_task_command_expr_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_command_expr_part" ):
                listener.enterTask_command_expr_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_command_expr_part" ):
                listener.exitTask_command_expr_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_command_expr_part" ):
                return visitor.visitTask_command_expr_part(self)
            else:
                return visitor.visitChildren(self)




    def task_command_expr_part(self):

        localctx = WdlV1Parser.Task_command_expr_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 86, self.RULE_task_command_expr_part)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 598
            self.match(WdlV1Parser.StringCommandStart)
            self.state = 602
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,53,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 599
                    self.expression_placeholder_option() 
                self.state = 604
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,53,self._ctx)

            self.state = 605
            self.expr()
            self.state = 606
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_command_expr_with_stringContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def task_command_expr_part(self):
            return self.getTypedRuleContext(WdlV1Parser.Task_command_expr_partContext,0)


        def task_command_string_part(self):
            return self.getTypedRuleContext(WdlV1Parser.Task_command_string_partContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_task_command_expr_with_string

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_command_expr_with_string" ):
                listener.enterTask_command_expr_with_string(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_command_expr_with_string" ):
                listener.exitTask_command_expr_with_string(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_command_expr_with_string" ):
                return visitor.visitTask_command_expr_with_string(self)
            else:
                return visitor.visitChildren(self)




    def task_command_expr_with_string(self):

        localctx = WdlV1Parser.Task_command_expr_with_stringContext(self, self._ctx, self.state)
        self.enterRule(localctx, 88, self.RULE_task_command_expr_with_string)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 608
            self.task_command_expr_part()
            self.state = 609
            self.task_command_string_part()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_commandContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def COMMAND(self):
            return self.getToken(WdlV1Parser.COMMAND, 0)

        def BeginLBrace(self):
            return self.getToken(WdlV1Parser.BeginLBrace, 0)

        def task_command_string_part(self):
            return self.getTypedRuleContext(WdlV1Parser.Task_command_string_partContext,0)


        def EndCommand(self):
            return self.getToken(WdlV1Parser.EndCommand, 0)

        def task_command_expr_with_string(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Task_command_expr_with_stringContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Task_command_expr_with_stringContext,i)


        def BeginHereDoc(self):
            return self.getToken(WdlV1Parser.BeginHereDoc, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_task_command

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_command" ):
                listener.enterTask_command(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_command" ):
                listener.exitTask_command(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_command" ):
                return visitor.visitTask_command(self)
            else:
                return visitor.visitChildren(self)




    def task_command(self):

        localctx = WdlV1Parser.Task_commandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 90, self.RULE_task_command)
        self._la = 0 # Token type
        try:
            self.state = 633
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,56,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 611
                self.match(WdlV1Parser.COMMAND)
                self.state = 612
                self.match(WdlV1Parser.BeginLBrace)
                self.state = 613
                self.task_command_string_part()
                self.state = 617
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlV1Parser.StringCommandStart:
                    self.state = 614
                    self.task_command_expr_with_string()
                    self.state = 619
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 620
                self.match(WdlV1Parser.EndCommand)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 622
                self.match(WdlV1Parser.COMMAND)
                self.state = 623
                self.match(WdlV1Parser.BeginHereDoc)
                self.state = 624
                self.task_command_string_part()
                self.state = 628
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlV1Parser.StringCommandStart:
                    self.state = 625
                    self.task_command_expr_with_string()
                    self.state = 630
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 631
                self.match(WdlV1Parser.EndCommand)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_elementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def task_input(self):
            return self.getTypedRuleContext(WdlV1Parser.Task_inputContext,0)


        def task_output(self):
            return self.getTypedRuleContext(WdlV1Parser.Task_outputContext,0)


        def task_command(self):
            return self.getTypedRuleContext(WdlV1Parser.Task_commandContext,0)


        def task_runtime(self):
            return self.getTypedRuleContext(WdlV1Parser.Task_runtimeContext,0)


        def bound_decls(self):
            return self.getTypedRuleContext(WdlV1Parser.Bound_declsContext,0)


        def parameter_meta(self):
            return self.getTypedRuleContext(WdlV1Parser.Parameter_metaContext,0)


        def meta(self):
            return self.getTypedRuleContext(WdlV1Parser.MetaContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_task_element

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_element" ):
                listener.enterTask_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_element" ):
                listener.exitTask_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_element" ):
                return visitor.visitTask_element(self)
            else:
                return visitor.visitChildren(self)




    def task_element(self):

        localctx = WdlV1Parser.Task_elementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 92, self.RULE_task_element)
        try:
            self.state = 642
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.INPUT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 635
                self.task_input()
                pass
            elif token in [WdlV1Parser.OUTPUT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 636
                self.task_output()
                pass
            elif token in [WdlV1Parser.COMMAND]:
                self.enterOuterAlt(localctx, 3)
                self.state = 637
                self.task_command()
                pass
            elif token in [WdlV1Parser.RUNTIME]:
                self.enterOuterAlt(localctx, 4)
                self.state = 638
                self.task_runtime()
                pass
            elif token in [WdlV1Parser.BOOLEAN, WdlV1Parser.INT, WdlV1Parser.FLOAT, WdlV1Parser.STRING, WdlV1Parser.FILE, WdlV1Parser.ARRAY, WdlV1Parser.MAP, WdlV1Parser.PAIR, WdlV1Parser.OBJECT, WdlV1Parser.Identifier]:
                self.enterOuterAlt(localctx, 5)
                self.state = 639
                self.bound_decls()
                pass
            elif token in [WdlV1Parser.PARAMETERMETA]:
                self.enterOuterAlt(localctx, 6)
                self.state = 640
                self.parameter_meta()
                pass
            elif token in [WdlV1Parser.META]:
                self.enterOuterAlt(localctx, 7)
                self.state = 641
                self.meta()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TaskContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TASK(self):
            return self.getToken(WdlV1Parser.TASK, 0)

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def task_element(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Task_elementContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Task_elementContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_task

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask" ):
                listener.enterTask(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask" ):
                listener.exitTask(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask" ):
                return visitor.visitTask(self)
            else:
                return visitor.visitChildren(self)




    def task(self):

        localctx = WdlV1Parser.TaskContext(self, self._ctx, self.state)
        self.enterRule(localctx, 94, self.RULE_task)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 644
            self.match(WdlV1Parser.TASK)
            self.state = 645
            self.match(WdlV1Parser.Identifier)
            self.state = 646
            self.match(WdlV1Parser.LBRACE)
            self.state = 648 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 647
                self.task_element()
                self.state = 650 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (((((_la - 15)) & ~0x3f) == 0 and ((1 << (_la - 15)) & ((1 << (WdlV1Parser.INPUT - 15)) | (1 << (WdlV1Parser.OUTPUT - 15)) | (1 << (WdlV1Parser.PARAMETERMETA - 15)) | (1 << (WdlV1Parser.META - 15)) | (1 << (WdlV1Parser.COMMAND - 15)) | (1 << (WdlV1Parser.RUNTIME - 15)) | (1 << (WdlV1Parser.BOOLEAN - 15)) | (1 << (WdlV1Parser.INT - 15)) | (1 << (WdlV1Parser.FLOAT - 15)) | (1 << (WdlV1Parser.STRING - 15)) | (1 << (WdlV1Parser.FILE - 15)) | (1 << (WdlV1Parser.ARRAY - 15)) | (1 << (WdlV1Parser.MAP - 15)) | (1 << (WdlV1Parser.PAIR - 15)) | (1 << (WdlV1Parser.OBJECT - 15)) | (1 << (WdlV1Parser.Identifier - 15)))) != 0)):
                    break

            self.state = 652
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Inner_workflow_elementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def bound_decls(self):
            return self.getTypedRuleContext(WdlV1Parser.Bound_declsContext,0)


        def call(self):
            return self.getTypedRuleContext(WdlV1Parser.CallContext,0)


        def scatter(self):
            return self.getTypedRuleContext(WdlV1Parser.ScatterContext,0)


        def conditional(self):
            return self.getTypedRuleContext(WdlV1Parser.ConditionalContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_inner_workflow_element

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInner_workflow_element" ):
                listener.enterInner_workflow_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInner_workflow_element" ):
                listener.exitInner_workflow_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInner_workflow_element" ):
                return visitor.visitInner_workflow_element(self)
            else:
                return visitor.visitChildren(self)




    def inner_workflow_element(self):

        localctx = WdlV1Parser.Inner_workflow_elementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 96, self.RULE_inner_workflow_element)
        try:
            self.state = 658
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.BOOLEAN, WdlV1Parser.INT, WdlV1Parser.FLOAT, WdlV1Parser.STRING, WdlV1Parser.FILE, WdlV1Parser.ARRAY, WdlV1Parser.MAP, WdlV1Parser.PAIR, WdlV1Parser.OBJECT, WdlV1Parser.Identifier]:
                self.enterOuterAlt(localctx, 1)
                self.state = 654
                self.bound_decls()
                pass
            elif token in [WdlV1Parser.CALL]:
                self.enterOuterAlt(localctx, 2)
                self.state = 655
                self.call()
                pass
            elif token in [WdlV1Parser.SCATTER]:
                self.enterOuterAlt(localctx, 3)
                self.state = 656
                self.scatter()
                pass
            elif token in [WdlV1Parser.IF]:
                self.enterOuterAlt(localctx, 4)
                self.state = 657
                self.conditional()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_aliasContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def AS(self):
            return self.getToken(WdlV1Parser.AS, 0)

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_call_alias

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_alias" ):
                listener.enterCall_alias(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_alias" ):
                listener.exitCall_alias(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_alias" ):
                return visitor.visitCall_alias(self)
            else:
                return visitor.visitChildren(self)




    def call_alias(self):

        localctx = WdlV1Parser.Call_aliasContext(self, self._ctx, self.state)
        self.enterRule(localctx, 98, self.RULE_call_alias)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 660
            self.match(WdlV1Parser.AS)
            self.state = 661
            self.match(WdlV1Parser.Identifier)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_inputContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def EQUAL(self):
            return self.getToken(WdlV1Parser.EQUAL, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_call_input

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_input" ):
                listener.enterCall_input(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_input" ):
                listener.exitCall_input(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_input" ):
                return visitor.visitCall_input(self)
            else:
                return visitor.visitChildren(self)




    def call_input(self):

        localctx = WdlV1Parser.Call_inputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 100, self.RULE_call_input)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 663
            self.match(WdlV1Parser.Identifier)
            self.state = 664
            self.match(WdlV1Parser.EQUAL)
            self.state = 665
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_inputsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INPUT(self):
            return self.getToken(WdlV1Parser.INPUT, 0)

        def COLON(self):
            return self.getToken(WdlV1Parser.COLON, 0)

        def call_input(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Call_inputContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Call_inputContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.COMMA)
            else:
                return self.getToken(WdlV1Parser.COMMA, i)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_call_inputs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_inputs" ):
                listener.enterCall_inputs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_inputs" ):
                listener.exitCall_inputs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_inputs" ):
                return visitor.visitCall_inputs(self)
            else:
                return visitor.visitChildren(self)




    def call_inputs(self):

        localctx = WdlV1Parser.Call_inputsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 102, self.RULE_call_inputs)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 667
            self.match(WdlV1Parser.INPUT)
            self.state = 668
            self.match(WdlV1Parser.COLON)
            self.state = 682
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlV1Parser.Identifier:
                self.state = 669
                self.call_input()
                self.state = 674
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,60,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 670
                        self.match(WdlV1Parser.COMMA)
                        self.state = 671
                        self.call_input() 
                    self.state = 676
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,60,self._ctx)

                self.state = 678
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==WdlV1Parser.COMMA:
                    self.state = 677
                    self.match(WdlV1Parser.COMMA)


                self.state = 684
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_bodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def call_inputs(self):
            return self.getTypedRuleContext(WdlV1Parser.Call_inputsContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_call_body

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_body" ):
                listener.enterCall_body(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_body" ):
                listener.exitCall_body(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_body" ):
                return visitor.visitCall_body(self)
            else:
                return visitor.visitChildren(self)




    def call_body(self):

        localctx = WdlV1Parser.Call_bodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 104, self.RULE_call_body)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 685
            self.match(WdlV1Parser.LBRACE)
            self.state = 687
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlV1Parser.INPUT:
                self.state = 686
                self.call_inputs()


            self.state = 689
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.Identifier)
            else:
                return self.getToken(WdlV1Parser.Identifier, i)

        def DOT(self, i:int=None):
            if i is None:
                return self.getTokens(WdlV1Parser.DOT)
            else:
                return self.getToken(WdlV1Parser.DOT, i)

        def getRuleIndex(self):
            return WdlV1Parser.RULE_call_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_name" ):
                listener.enterCall_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_name" ):
                listener.exitCall_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_name" ):
                return visitor.visitCall_name(self)
            else:
                return visitor.visitChildren(self)




    def call_name(self):

        localctx = WdlV1Parser.Call_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 106, self.RULE_call_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 691
            self.match(WdlV1Parser.Identifier)
            self.state = 696
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlV1Parser.DOT:
                self.state = 692
                self.match(WdlV1Parser.DOT)
                self.state = 693
                self.match(WdlV1Parser.Identifier)
                self.state = 698
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CALL(self):
            return self.getToken(WdlV1Parser.CALL, 0)

        def call_name(self):
            return self.getTypedRuleContext(WdlV1Parser.Call_nameContext,0)


        def call_alias(self):
            return self.getTypedRuleContext(WdlV1Parser.Call_aliasContext,0)


        def call_body(self):
            return self.getTypedRuleContext(WdlV1Parser.Call_bodyContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_call

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall" ):
                listener.enterCall(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall" ):
                listener.exitCall(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall" ):
                return visitor.visitCall(self)
            else:
                return visitor.visitChildren(self)




    def call(self):

        localctx = WdlV1Parser.CallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 108, self.RULE_call)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 699
            self.match(WdlV1Parser.CALL)
            self.state = 700
            self.call_name()
            self.state = 702
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlV1Parser.AS:
                self.state = 701
                self.call_alias()


            self.state = 705
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlV1Parser.LBRACE:
                self.state = 704
                self.call_body()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ScatterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SCATTER(self):
            return self.getToken(WdlV1Parser.SCATTER, 0)

        def LPAREN(self):
            return self.getToken(WdlV1Parser.LPAREN, 0)

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def In(self):
            return self.getToken(WdlV1Parser.In, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)


        def RPAREN(self):
            return self.getToken(WdlV1Parser.RPAREN, 0)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def inner_workflow_element(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Inner_workflow_elementContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Inner_workflow_elementContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_scatter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterScatter" ):
                listener.enterScatter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitScatter" ):
                listener.exitScatter(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitScatter" ):
                return visitor.visitScatter(self)
            else:
                return visitor.visitChildren(self)




    def scatter(self):

        localctx = WdlV1Parser.ScatterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 110, self.RULE_scatter)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 707
            self.match(WdlV1Parser.SCATTER)
            self.state = 708
            self.match(WdlV1Parser.LPAREN)
            self.state = 709
            self.match(WdlV1Parser.Identifier)
            self.state = 710
            self.match(WdlV1Parser.In)
            self.state = 711
            self.expr()
            self.state = 712
            self.match(WdlV1Parser.RPAREN)
            self.state = 713
            self.match(WdlV1Parser.LBRACE)
            self.state = 717
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 7)) & ~0x3f) == 0 and ((1 << (_la - 7)) & ((1 << (WdlV1Parser.SCATTER - 7)) | (1 << (WdlV1Parser.CALL - 7)) | (1 << (WdlV1Parser.IF - 7)) | (1 << (WdlV1Parser.BOOLEAN - 7)) | (1 << (WdlV1Parser.INT - 7)) | (1 << (WdlV1Parser.FLOAT - 7)) | (1 << (WdlV1Parser.STRING - 7)) | (1 << (WdlV1Parser.FILE - 7)) | (1 << (WdlV1Parser.ARRAY - 7)) | (1 << (WdlV1Parser.MAP - 7)) | (1 << (WdlV1Parser.PAIR - 7)) | (1 << (WdlV1Parser.OBJECT - 7)) | (1 << (WdlV1Parser.Identifier - 7)))) != 0):
                self.state = 714
                self.inner_workflow_element()
                self.state = 719
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 720
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConditionalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(WdlV1Parser.IF, 0)

        def LPAREN(self):
            return self.getToken(WdlV1Parser.LPAREN, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlV1Parser.ExprContext,0)


        def RPAREN(self):
            return self.getToken(WdlV1Parser.RPAREN, 0)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def inner_workflow_element(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Inner_workflow_elementContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Inner_workflow_elementContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_conditional

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConditional" ):
                listener.enterConditional(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConditional" ):
                listener.exitConditional(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConditional" ):
                return visitor.visitConditional(self)
            else:
                return visitor.visitChildren(self)




    def conditional(self):

        localctx = WdlV1Parser.ConditionalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 112, self.RULE_conditional)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 722
            self.match(WdlV1Parser.IF)
            self.state = 723
            self.match(WdlV1Parser.LPAREN)
            self.state = 724
            self.expr()
            self.state = 725
            self.match(WdlV1Parser.RPAREN)
            self.state = 726
            self.match(WdlV1Parser.LBRACE)
            self.state = 730
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 7)) & ~0x3f) == 0 and ((1 << (_la - 7)) & ((1 << (WdlV1Parser.SCATTER - 7)) | (1 << (WdlV1Parser.CALL - 7)) | (1 << (WdlV1Parser.IF - 7)) | (1 << (WdlV1Parser.BOOLEAN - 7)) | (1 << (WdlV1Parser.INT - 7)) | (1 << (WdlV1Parser.FLOAT - 7)) | (1 << (WdlV1Parser.STRING - 7)) | (1 << (WdlV1Parser.FILE - 7)) | (1 << (WdlV1Parser.ARRAY - 7)) | (1 << (WdlV1Parser.MAP - 7)) | (1 << (WdlV1Parser.PAIR - 7)) | (1 << (WdlV1Parser.OBJECT - 7)) | (1 << (WdlV1Parser.Identifier - 7)))) != 0):
                self.state = 727
                self.inner_workflow_element()
                self.state = 732
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 733
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Workflow_inputContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INPUT(self):
            return self.getToken(WdlV1Parser.INPUT, 0)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def any_decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Any_declsContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Any_declsContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_workflow_input

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWorkflow_input" ):
                listener.enterWorkflow_input(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWorkflow_input" ):
                listener.exitWorkflow_input(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWorkflow_input" ):
                return visitor.visitWorkflow_input(self)
            else:
                return visitor.visitChildren(self)




    def workflow_input(self):

        localctx = WdlV1Parser.Workflow_inputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 114, self.RULE_workflow_input)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 735
            self.match(WdlV1Parser.INPUT)
            self.state = 736
            self.match(WdlV1Parser.LBRACE)
            self.state = 740
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 21)) & ~0x3f) == 0 and ((1 << (_la - 21)) & ((1 << (WdlV1Parser.BOOLEAN - 21)) | (1 << (WdlV1Parser.INT - 21)) | (1 << (WdlV1Parser.FLOAT - 21)) | (1 << (WdlV1Parser.STRING - 21)) | (1 << (WdlV1Parser.FILE - 21)) | (1 << (WdlV1Parser.ARRAY - 21)) | (1 << (WdlV1Parser.MAP - 21)) | (1 << (WdlV1Parser.PAIR - 21)) | (1 << (WdlV1Parser.OBJECT - 21)) | (1 << (WdlV1Parser.Identifier - 21)))) != 0):
                self.state = 737
                self.any_decls()
                self.state = 742
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 743
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Workflow_outputContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OUTPUT(self):
            return self.getToken(WdlV1Parser.OUTPUT, 0)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def bound_decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Bound_declsContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Bound_declsContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_workflow_output

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWorkflow_output" ):
                listener.enterWorkflow_output(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWorkflow_output" ):
                listener.exitWorkflow_output(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWorkflow_output" ):
                return visitor.visitWorkflow_output(self)
            else:
                return visitor.visitChildren(self)




    def workflow_output(self):

        localctx = WdlV1Parser.Workflow_outputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 116, self.RULE_workflow_output)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 745
            self.match(WdlV1Parser.OUTPUT)
            self.state = 746
            self.match(WdlV1Parser.LBRACE)
            self.state = 750
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 21)) & ~0x3f) == 0 and ((1 << (_la - 21)) & ((1 << (WdlV1Parser.BOOLEAN - 21)) | (1 << (WdlV1Parser.INT - 21)) | (1 << (WdlV1Parser.FLOAT - 21)) | (1 << (WdlV1Parser.STRING - 21)) | (1 << (WdlV1Parser.FILE - 21)) | (1 << (WdlV1Parser.ARRAY - 21)) | (1 << (WdlV1Parser.MAP - 21)) | (1 << (WdlV1Parser.PAIR - 21)) | (1 << (WdlV1Parser.OBJECT - 21)) | (1 << (WdlV1Parser.Identifier - 21)))) != 0):
                self.state = 747
                self.bound_decls()
                self.state = 752
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 753
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Workflow_elementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlV1Parser.RULE_workflow_element

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class OutputContext(Workflow_elementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Workflow_elementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def workflow_output(self):
            return self.getTypedRuleContext(WdlV1Parser.Workflow_outputContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOutput" ):
                listener.enterOutput(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOutput" ):
                listener.exitOutput(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOutput" ):
                return visitor.visitOutput(self)
            else:
                return visitor.visitChildren(self)


    class InputContext(Workflow_elementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Workflow_elementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def workflow_input(self):
            return self.getTypedRuleContext(WdlV1Parser.Workflow_inputContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInput" ):
                listener.enterInput(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInput" ):
                listener.exitInput(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInput" ):
                return visitor.visitInput(self)
            else:
                return visitor.visitChildren(self)


    class Parameter_meta_elementContext(Workflow_elementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Workflow_elementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def parameter_meta(self):
            return self.getTypedRuleContext(WdlV1Parser.Parameter_metaContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameter_meta_element" ):
                listener.enterParameter_meta_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameter_meta_element" ):
                listener.exitParameter_meta_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameter_meta_element" ):
                return visitor.visitParameter_meta_element(self)
            else:
                return visitor.visitChildren(self)


    class Meta_elementContext(Workflow_elementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Workflow_elementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def meta(self):
            return self.getTypedRuleContext(WdlV1Parser.MetaContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta_element" ):
                listener.enterMeta_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta_element" ):
                listener.exitMeta_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta_element" ):
                return visitor.visitMeta_element(self)
            else:
                return visitor.visitChildren(self)


    class Inner_elementContext(Workflow_elementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlV1Parser.Workflow_elementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def inner_workflow_element(self):
            return self.getTypedRuleContext(WdlV1Parser.Inner_workflow_elementContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInner_element" ):
                listener.enterInner_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInner_element" ):
                listener.exitInner_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInner_element" ):
                return visitor.visitInner_element(self)
            else:
                return visitor.visitChildren(self)



    def workflow_element(self):

        localctx = WdlV1Parser.Workflow_elementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 118, self.RULE_workflow_element)
        try:
            self.state = 760
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.INPUT]:
                localctx = WdlV1Parser.InputContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 755
                self.workflow_input()
                pass
            elif token in [WdlV1Parser.OUTPUT]:
                localctx = WdlV1Parser.OutputContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 756
                self.workflow_output()
                pass
            elif token in [WdlV1Parser.SCATTER, WdlV1Parser.CALL, WdlV1Parser.IF, WdlV1Parser.BOOLEAN, WdlV1Parser.INT, WdlV1Parser.FLOAT, WdlV1Parser.STRING, WdlV1Parser.FILE, WdlV1Parser.ARRAY, WdlV1Parser.MAP, WdlV1Parser.PAIR, WdlV1Parser.OBJECT, WdlV1Parser.Identifier]:
                localctx = WdlV1Parser.Inner_elementContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 757
                self.inner_workflow_element()
                pass
            elif token in [WdlV1Parser.PARAMETERMETA]:
                localctx = WdlV1Parser.Parameter_meta_elementContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 758
                self.parameter_meta()
                pass
            elif token in [WdlV1Parser.META]:
                localctx = WdlV1Parser.Meta_elementContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 759
                self.meta()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WorkflowContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WORKFLOW(self):
            return self.getToken(WdlV1Parser.WORKFLOW, 0)

        def Identifier(self):
            return self.getToken(WdlV1Parser.Identifier, 0)

        def LBRACE(self):
            return self.getToken(WdlV1Parser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlV1Parser.RBRACE, 0)

        def workflow_element(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Workflow_elementContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Workflow_elementContext,i)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_workflow

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWorkflow" ):
                listener.enterWorkflow(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWorkflow" ):
                listener.exitWorkflow(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWorkflow" ):
                return visitor.visitWorkflow(self)
            else:
                return visitor.visitChildren(self)




    def workflow(self):

        localctx = WdlV1Parser.WorkflowContext(self, self._ctx, self.state)
        self.enterRule(localctx, 120, self.RULE_workflow)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 762
            self.match(WdlV1Parser.WORKFLOW)
            self.state = 763
            self.match(WdlV1Parser.Identifier)
            self.state = 764
            self.match(WdlV1Parser.LBRACE)
            self.state = 768
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 7)) & ~0x3f) == 0 and ((1 << (_la - 7)) & ((1 << (WdlV1Parser.SCATTER - 7)) | (1 << (WdlV1Parser.CALL - 7)) | (1 << (WdlV1Parser.IF - 7)) | (1 << (WdlV1Parser.INPUT - 7)) | (1 << (WdlV1Parser.OUTPUT - 7)) | (1 << (WdlV1Parser.PARAMETERMETA - 7)) | (1 << (WdlV1Parser.META - 7)) | (1 << (WdlV1Parser.BOOLEAN - 7)) | (1 << (WdlV1Parser.INT - 7)) | (1 << (WdlV1Parser.FLOAT - 7)) | (1 << (WdlV1Parser.STRING - 7)) | (1 << (WdlV1Parser.FILE - 7)) | (1 << (WdlV1Parser.ARRAY - 7)) | (1 << (WdlV1Parser.MAP - 7)) | (1 << (WdlV1Parser.PAIR - 7)) | (1 << (WdlV1Parser.OBJECT - 7)) | (1 << (WdlV1Parser.Identifier - 7)))) != 0):
                self.state = 765
                self.workflow_element()
                self.state = 770
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 771
            self.match(WdlV1Parser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Document_elementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def import_doc(self):
            return self.getTypedRuleContext(WdlV1Parser.Import_docContext,0)


        def struct(self):
            return self.getTypedRuleContext(WdlV1Parser.StructContext,0)


        def task(self):
            return self.getTypedRuleContext(WdlV1Parser.TaskContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_document_element

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDocument_element" ):
                listener.enterDocument_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDocument_element" ):
                listener.exitDocument_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDocument_element" ):
                return visitor.visitDocument_element(self)
            else:
                return visitor.visitChildren(self)




    def document_element(self):

        localctx = WdlV1Parser.Document_elementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 122, self.RULE_document_element)
        try:
            self.state = 776
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlV1Parser.IMPORT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 773
                self.import_doc()
                pass
            elif token in [WdlV1Parser.STRUCT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 774
                self.struct()
                pass
            elif token in [WdlV1Parser.TASK]:
                self.enterOuterAlt(localctx, 3)
                self.state = 775
                self.task()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DocumentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def version(self):
            return self.getTypedRuleContext(WdlV1Parser.VersionContext,0)


        def EOF(self):
            return self.getToken(WdlV1Parser.EOF, 0)

        def document_element(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlV1Parser.Document_elementContext)
            else:
                return self.getTypedRuleContext(WdlV1Parser.Document_elementContext,i)


        def workflow(self):
            return self.getTypedRuleContext(WdlV1Parser.WorkflowContext,0)


        def getRuleIndex(self):
            return WdlV1Parser.RULE_document

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDocument" ):
                listener.enterDocument(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDocument" ):
                listener.exitDocument(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDocument" ):
                return visitor.visitDocument(self)
            else:
                return visitor.visitChildren(self)




    def document(self):

        localctx = WdlV1Parser.DocumentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 124, self.RULE_document)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 778
            self.version()
            self.state = 782
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WdlV1Parser.IMPORT) | (1 << WdlV1Parser.TASK) | (1 << WdlV1Parser.STRUCT))) != 0):
                self.state = 779
                self.document_element()
                self.state = 784
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 792
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlV1Parser.WORKFLOW:
                self.state = 785
                self.workflow()
                self.state = 789
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WdlV1Parser.IMPORT) | (1 << WdlV1Parser.TASK) | (1 << WdlV1Parser.STRUCT))) != 0):
                    self.state = 786
                    self.document_element()
                    self.state = 791
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 794
            self.match(WdlV1Parser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[17] = self.expr_infix0_sempred
        self._predicates[18] = self.expr_infix1_sempred
        self._predicates[19] = self.expr_infix2_sempred
        self._predicates[20] = self.expr_infix3_sempred
        self._predicates[21] = self.expr_infix4_sempred
        self._predicates[23] = self.expr_core_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_infix0_sempred(self, localctx:Expr_infix0Context, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

    def expr_infix1_sempred(self, localctx:Expr_infix1Context, predIndex:int):
            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         

    def expr_infix2_sempred(self, localctx:Expr_infix2Context, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 7:
                return self.precpred(self._ctx, 2)
         

    def expr_infix3_sempred(self, localctx:Expr_infix3Context, predIndex:int):
            if predIndex == 8:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 9:
                return self.precpred(self._ctx, 2)
         

    def expr_infix4_sempred(self, localctx:Expr_infix4Context, predIndex:int):
            if predIndex == 10:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 11:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 12:
                return self.precpred(self._ctx, 2)
         

    def expr_core_sempred(self, localctx:Expr_coreContext, predIndex:int):
            if predIndex == 13:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 14:
                return self.precpred(self._ctx, 5)
         




