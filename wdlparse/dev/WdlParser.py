# Generated from WdlParser.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3X")
        buf.write("\u02d5\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.\t.\4")
        buf.write("/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t\64")
        buf.write("\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t")
        buf.write(";\4<\t<\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3")
        buf.write("\3\5\3\u0085\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3")
        buf.write("\5\3\5\5\5\u0092\n\5\3\6\3\6\3\6\3\6\5\6\u0098\n\6\3\7")
        buf.write("\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\5\t\u00a4\n\t\3\n")
        buf.write("\3\n\3\13\7\13\u00a9\n\13\f\13\16\13\u00ac\13\13\3\f\3")
        buf.write("\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16\7\16\u00b8\n\16")
        buf.write("\f\16\16\16\u00bb\13\16\3\16\3\16\3\16\3\16\3\16\7\16")
        buf.write("\u00c2\n\16\f\16\16\16\u00c5\13\16\3\16\3\16\5\16\u00c9")
        buf.write("\n\16\3\17\3\17\3\17\3\17\3\17\5\17\u00d0\n\17\3\20\3")
        buf.write("\20\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\7\22\u00dc")
        buf.write("\n\22\f\22\16\22\u00df\13\22\3\23\3\23\3\23\3\23\3\23")
        buf.write("\3\23\7\23\u00e7\n\23\f\23\16\23\u00ea\13\23\3\24\3\24")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\7\24\u0101\n")
        buf.write("\24\f\24\16\24\u0104\13\24\3\25\3\25\3\25\3\25\3\25\3")
        buf.write("\25\3\25\3\25\3\25\7\25\u010f\n\25\f\25\16\25\u0112\13")
        buf.write("\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\7\26\u0120\n\26\f\26\16\26\u0123\13\26\3\27")
        buf.write("\3\27\3\30\3\30\3\30\3\30\3\30\3\30\7\30\u012d\n\30\f")
        buf.write("\30\16\30\u0130\13\30\5\30\u0132\n\30\3\30\3\30\3\30\3")
        buf.write("\30\3\30\7\30\u0139\n\30\f\30\16\30\u013c\13\30\7\30\u013e")
        buf.write("\n\30\f\30\16\30\u0141\13\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\7\30\u0153\n\30\f\30\16\30\u0156\13\30\7\30\u0158\n\30")
        buf.write("\f\30\16\30\u015b\13\30\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\7\30\u0167\n\30\f\30\16\30\u016a")
        buf.write("\13\30\7\30\u016c\n\30\f\30\16\30\u016f\13\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\5\30\u0183\n\30\3\30\3\30\3")
        buf.write("\30\3\30\3\30\3\30\3\30\3\30\7\30\u018d\n\30\f\30\16\30")
        buf.write("\u0190\13\30\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3")
        buf.write("\33\3\33\3\33\3\34\3\34\3\34\5\34\u01a0\n\34\3\34\7\34")
        buf.write("\u01a3\n\34\f\34\16\34\u01a6\13\34\3\35\3\35\3\35\3\35")
        buf.write("\7\35\u01ac\n\35\f\35\16\35\u01af\13\35\3\35\3\35\3\36")
        buf.write("\3\36\3\36\3\36\3\37\3\37\3\37\7\37\u01ba\n\37\f\37\16")
        buf.write("\37\u01bd\13\37\3\37\3\37\3 \3 \3 \7 \u01c4\n \f \16 ")
        buf.write("\u01c7\13 \3 \3 \3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3")
        buf.write("!\3!\3!\3!\3!\3!\3!\3!\3!\5!\u01e0\n!\3\"\3\"\3\"\7\"")
        buf.write("\u01e5\n\"\f\"\16\"\u01e8\13\"\3\"\3\"\3#\3#\3#\3#\3$")
        buf.write("\3$\3$\7$\u01f3\n$\f$\16$\u01f6\13$\3$\3$\3%\3%\3%\7%")
        buf.write("\u01fd\n%\f%\16%\u0200\13%\3%\3%\3&\3&\3&\7&\u0207\n&")
        buf.write("\f&\16&\u020a\13&\3&\3&\3\'\3\'\3\'\7\'\u0211\n\'\f\'")
        buf.write("\16\'\u0214\13\'\3\'\3\'\3\'\3\'\3\'\7\'\u021b\n\'\f\'")
        buf.write("\16\'\u021e\13\'\3\'\3\'\5\'\u0222\n\'\3(\7(\u0225\n(")
        buf.write("\f(\16(\u0228\13(\3)\3)\3)\3)\3*\3*\3*\3+\3+\3+\3+\3+")
        buf.write("\3+\3+\3+\5+\u0239\n+\3,\3,\3,\3,\6,\u023f\n,\r,\16,\u0240")
        buf.write("\3,\3,\3-\3-\3-\3-\5-\u0249\n-\3.\3.\3.\3/\3/\3/\3/\3")
        buf.write("\60\3\60\3\60\3\60\3\60\7\60\u0257\n\60\f\60\16\60\u025a")
        buf.write("\13\60\3\61\3\61\5\61\u025e\n\61\3\61\3\61\3\62\3\62\3")
        buf.write("\62\3\63\3\63\3\63\7\63\u0268\n\63\f\63\16\63\u026b\13")
        buf.write("\63\3\64\3\64\3\64\5\64\u0270\n\64\3\64\7\64\u0273\n\64")
        buf.write("\f\64\16\64\u0276\13\64\3\64\5\64\u0279\n\64\3\65\3\65")
        buf.write("\3\65\3\65\3\65\3\65\3\65\3\65\7\65\u0283\n\65\f\65\16")
        buf.write("\65\u0286\13\65\3\65\3\65\3\66\3\66\3\66\3\66\3\66\3\66")
        buf.write("\7\66\u0290\n\66\f\66\16\66\u0293\13\66\3\66\3\66\3\67")
        buf.write("\3\67\3\67\7\67\u029a\n\67\f\67\16\67\u029d\13\67\3\67")
        buf.write("\3\67\38\38\38\78\u02a4\n8\f8\168\u02a7\138\38\38\39\3")
        buf.write("9\39\39\39\59\u02b0\n9\3:\3:\3:\3:\7:\u02b6\n:\f:\16:")
        buf.write("\u02b9\13:\3:\3:\3;\3;\3;\3;\5;\u02c1\n;\3<\3<\7<\u02c5")
        buf.write("\n<\f<\16<\u02c8\13<\3<\3<\7<\u02cc\n<\f<\16<\u02cf\13")
        buf.write("<\5<\u02d1\n<\3<\3<\3<\2\b\"$&(*.=\2\4\6\b\n\f\16\20\22")
        buf.write("\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPR")
        buf.write("TVXZ\\^`bdfhjlnprtv\2\5\4\2\35\"NN\3\2*+\3\2@A\2\u02fb")
        buf.write("\2x\3\2\2\2\4\177\3\2\2\2\6\u0086\3\2\2\2\b\u0091\3\2")
        buf.write("\2\2\n\u0097\3\2\2\2\f\u0099\3\2\2\2\16\u009c\3\2\2\2")
        buf.write("\20\u00a3\3\2\2\2\22\u00a5\3\2\2\2\24\u00aa\3\2\2\2\26")
        buf.write("\u00ad\3\2\2\2\30\u00b1\3\2\2\2\32\u00c8\3\2\2\2\34\u00cf")
        buf.write("\3\2\2\2\36\u00d1\3\2\2\2 \u00d3\3\2\2\2\"\u00d5\3\2\2")
        buf.write("\2$\u00e0\3\2\2\2&\u00eb\3\2\2\2(\u0105\3\2\2\2*\u0113")
        buf.write("\3\2\2\2,\u0124\3\2\2\2.\u0182\3\2\2\2\60\u0191\3\2\2")
        buf.write("\2\62\u0194\3\2\2\2\64\u0199\3\2\2\2\66\u019c\3\2\2\2")
        buf.write("8\u01a7\3\2\2\2:\u01b2\3\2\2\2<\u01b6\3\2\2\2>\u01c0\3")
        buf.write("\2\2\2@\u01df\3\2\2\2B\u01e1\3\2\2\2D\u01eb\3\2\2\2F\u01ef")
        buf.write("\3\2\2\2H\u01f9\3\2\2\2J\u0203\3\2\2\2L\u0221\3\2\2\2")
        buf.write("N\u0226\3\2\2\2P\u0229\3\2\2\2R\u022d\3\2\2\2T\u0238\3")
        buf.write("\2\2\2V\u023a\3\2\2\2X\u0248\3\2\2\2Z\u024a\3\2\2\2\\")
        buf.write("\u024d\3\2\2\2^\u0251\3\2\2\2`\u025b\3\2\2\2b\u0261\3")
        buf.write("\2\2\2d\u0264\3\2\2\2f\u026c\3\2\2\2h\u027a\3\2\2\2j\u0289")
        buf.write("\3\2\2\2l\u0296\3\2\2\2n\u02a0\3\2\2\2p\u02af\3\2\2\2")
        buf.write("r\u02b1\3\2\2\2t\u02c0\3\2\2\2v\u02c2\3\2\2\2xy\7$\2\2")
        buf.write("yz\7\61\2\2z{\5\n\6\2{|\7C\2\2|}\5\n\6\2}~\7\62\2\2~\3")
        buf.write("\3\2\2\2\177\u0080\7#\2\2\u0080\u0081\7\61\2\2\u0081\u0082")
        buf.write("\5\n\6\2\u0082\u0084\7\62\2\2\u0083\u0085\7@\2\2\u0084")
        buf.write("\u0083\3\2\2\2\u0084\u0085\3\2\2\2\u0085\5\3\2\2\2\u0086")
        buf.write("\u0087\7%\2\2\u0087\u0088\7\61\2\2\u0088\u0089\5\n\6\2")
        buf.write("\u0089\u008a\7C\2\2\u008a\u008b\5\n\6\2\u008b\u008c\7")
        buf.write("\62\2\2\u008c\7\3\2\2\2\u008d\u0092\5\4\3\2\u008e\u0092")
        buf.write("\5\2\2\2\u008f\u0092\5\6\4\2\u0090\u0092\t\2\2\2\u0091")
        buf.write("\u008d\3\2\2\2\u0091\u008e\3\2\2\2\u0091\u008f\3\2\2\2")
        buf.write("\u0091\u0090\3\2\2\2\u0092\t\3\2\2\2\u0093\u0094\5\b\5")
        buf.write("\2\u0094\u0095\7>\2\2\u0095\u0098\3\2\2\2\u0096\u0098")
        buf.write("\5\b\5\2\u0097\u0093\3\2\2\2\u0097\u0096\3\2\2\2\u0098")
        buf.write("\13\3\2\2\2\u0099\u009a\5\n\6\2\u009a\u009b\7N\2\2\u009b")
        buf.write("\r\3\2\2\2\u009c\u009d\5\n\6\2\u009d\u009e\7N\2\2\u009e")
        buf.write("\u009f\7;\2\2\u009f\u00a0\5\36\20\2\u00a0\17\3\2\2\2\u00a1")
        buf.write("\u00a4\5\f\7\2\u00a2\u00a4\5\16\b\2\u00a3\u00a1\3\2\2")
        buf.write("\2\u00a3\u00a2\3\2\2\2\u00a4\21\3\2\2\2\u00a5\u00a6\t")
        buf.write("\3\2\2\u00a6\23\3\2\2\2\u00a7\u00a9\7O\2\2\u00a8\u00a7")
        buf.write("\3\2\2\2\u00a9\u00ac\3\2\2\2\u00aa\u00a8\3\2\2\2\u00aa")
        buf.write("\u00ab\3\2\2\2\u00ab\25\3\2\2\2\u00ac\u00aa\3\2\2\2\u00ad")
        buf.write("\u00ae\7R\2\2\u00ae\u00af\5\36\20\2\u00af\u00b0\7\60\2")
        buf.write("\2\u00b0\27\3\2\2\2\u00b1\u00b2\5\26\f\2\u00b2\u00b3\5")
        buf.write("\24\13\2\u00b3\31\3\2\2\2\u00b4\u00b5\7K\2\2\u00b5\u00b9")
        buf.write("\5\24\13\2\u00b6\u00b8\5\30\r\2\u00b7\u00b6\3\2\2\2\u00b8")
        buf.write("\u00bb\3\2\2\2\u00b9\u00b7\3\2\2\2\u00b9\u00ba\3\2\2\2")
        buf.write("\u00ba\u00bc\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bc\u00bd\7")
        buf.write("K\2\2\u00bd\u00c9\3\2\2\2\u00be\u00bf\7J\2\2\u00bf\u00c3")
        buf.write("\5\24\13\2\u00c0\u00c2\5\30\r\2\u00c1\u00c0\3\2\2\2\u00c2")
        buf.write("\u00c5\3\2\2\2\u00c3\u00c1\3\2\2\2\u00c3\u00c4\3\2\2\2")
        buf.write("\u00c4\u00c6\3\2\2\2\u00c5\u00c3\3\2\2\2\u00c6\u00c7\7")
        buf.write("J\2\2\u00c7\u00c9\3\2\2\2\u00c8\u00b4\3\2\2\2\u00c8\u00be")
        buf.write("\3\2\2\2\u00c9\33\3\2\2\2\u00ca\u00d0\7,\2\2\u00cb\u00d0")
        buf.write("\5\22\n\2\u00cc\u00d0\5\32\16\2\u00cd\u00d0\7)\2\2\u00ce")
        buf.write("\u00d0\7N\2\2\u00cf\u00ca\3\2\2\2\u00cf\u00cb\3\2\2\2")
        buf.write("\u00cf\u00cc\3\2\2\2\u00cf\u00cd\3\2\2\2\u00cf\u00ce\3")
        buf.write("\2\2\2\u00d0\35\3\2\2\2\u00d1\u00d2\5 \21\2\u00d2\37\3")
        buf.write("\2\2\2\u00d3\u00d4\5\"\22\2\u00d4!\3\2\2\2\u00d5\u00d6")
        buf.write("\b\22\1\2\u00d6\u00d7\5$\23\2\u00d7\u00dd\3\2\2\2\u00d8")
        buf.write("\u00d9\f\4\2\2\u00d9\u00da\7=\2\2\u00da\u00dc\5$\23\2")
        buf.write("\u00db\u00d8\3\2\2\2\u00dc\u00df\3\2\2\2\u00dd\u00db\3")
        buf.write("\2\2\2\u00dd\u00de\3\2\2\2\u00de#\3\2\2\2\u00df\u00dd")
        buf.write("\3\2\2\2\u00e0\u00e1\b\23\1\2\u00e1\u00e2\5&\24\2\u00e2")
        buf.write("\u00e8\3\2\2\2\u00e3\u00e4\f\4\2\2\u00e4\u00e5\7<\2\2")
        buf.write("\u00e5\u00e7\5&\24\2\u00e6\u00e3\3\2\2\2\u00e7\u00ea\3")
        buf.write("\2\2\2\u00e8\u00e6\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9%")
        buf.write("\3\2\2\2\u00ea\u00e8\3\2\2\2\u00eb\u00ec\b\24\1\2\u00ec")
        buf.write("\u00ed\5(\25\2\u00ed\u0102\3\2\2\2\u00ee\u00ef\f\t\2\2")
        buf.write("\u00ef\u00f0\79\2\2\u00f0\u0101\5(\25\2\u00f1\u00f2\f")
        buf.write("\b\2\2\u00f2\u00f3\7:\2\2\u00f3\u0101\5(\25\2\u00f4\u00f5")
        buf.write("\f\7\2\2\u00f5\u00f6\78\2\2\u00f6\u0101\5(\25\2\u00f7")
        buf.write("\u00f8\f\6\2\2\u00f8\u00f9\7\67\2\2\u00f9\u0101\5(\25")
        buf.write("\2\u00fa\u00fb\f\5\2\2\u00fb\u00fc\7\65\2\2\u00fc\u0101")
        buf.write("\5(\25\2\u00fd\u00fe\f\4\2\2\u00fe\u00ff\7\66\2\2\u00ff")
        buf.write("\u0101\5(\25\2\u0100\u00ee\3\2\2\2\u0100\u00f1\3\2\2\2")
        buf.write("\u0100\u00f4\3\2\2\2\u0100\u00f7\3\2\2\2\u0100\u00fa\3")
        buf.write("\2\2\2\u0100\u00fd\3\2\2\2\u0101\u0104\3\2\2\2\u0102\u0100")
        buf.write("\3\2\2\2\u0102\u0103\3\2\2\2\u0103\'\3\2\2\2\u0104\u0102")
        buf.write("\3\2\2\2\u0105\u0106\b\25\1\2\u0106\u0107\5*\26\2\u0107")
        buf.write("\u0110\3\2\2\2\u0108\u0109\f\5\2\2\u0109\u010a\7@\2\2")
        buf.write("\u010a\u010f\5*\26\2\u010b\u010c\f\4\2\2\u010c\u010d\7")
        buf.write("A\2\2\u010d\u010f\5*\26\2\u010e\u0108\3\2\2\2\u010e\u010b")
        buf.write("\3\2\2\2\u010f\u0112\3\2\2\2\u0110\u010e\3\2\2\2\u0110")
        buf.write("\u0111\3\2\2\2\u0111)\3\2\2\2\u0112\u0110\3\2\2\2\u0113")
        buf.write("\u0114\b\26\1\2\u0114\u0115\5,\27\2\u0115\u0121\3\2\2")
        buf.write("\2\u0116\u0117\f\6\2\2\u0117\u0118\7?\2\2\u0118\u0120")
        buf.write("\5,\27\2\u0119\u011a\f\5\2\2\u011a\u011b\7H\2\2\u011b")
        buf.write("\u0120\5,\27\2\u011c\u011d\f\4\2\2\u011d\u011e\7I\2\2")
        buf.write("\u011e\u0120\5,\27\2\u011f\u0116\3\2\2\2\u011f\u0119\3")
        buf.write("\2\2\2\u011f\u011c\3\2\2\2\u0120\u0123\3\2\2\2\u0121\u011f")
        buf.write("\3\2\2\2\u0121\u0122\3\2\2\2\u0122+\3\2\2\2\u0123\u0121")
        buf.write("\3\2\2\2\u0124\u0125\5.\30\2\u0125-\3\2\2\2\u0126\u0127")
        buf.write("\b\30\1\2\u0127\u0128\7N\2\2\u0128\u0131\7-\2\2\u0129")
        buf.write("\u012e\5\36\20\2\u012a\u012b\7C\2\2\u012b\u012d\5\36\20")
        buf.write("\2\u012c\u012a\3\2\2\2\u012d\u0130\3\2\2\2\u012e\u012c")
        buf.write("\3\2\2\2\u012e\u012f\3\2\2\2\u012f\u0132\3\2\2\2\u0130")
        buf.write("\u012e\3\2\2\2\u0131\u0129\3\2\2\2\u0131\u0132\3\2\2\2")
        buf.write("\u0132\u0133\3\2\2\2\u0133\u0183\7.\2\2\u0134\u013f\7")
        buf.write("\61\2\2\u0135\u013a\5\36\20\2\u0136\u0137\7C\2\2\u0137")
        buf.write("\u0139\5\36\20\2\u0138\u0136\3\2\2\2\u0139\u013c\3\2\2")
        buf.write("\2\u013a\u0138\3\2\2\2\u013a\u013b\3\2\2\2\u013b\u013e")
        buf.write("\3\2\2\2\u013c\u013a\3\2\2\2\u013d\u0135\3\2\2\2\u013e")
        buf.write("\u0141\3\2\2\2\u013f\u013d\3\2\2\2\u013f\u0140\3\2\2\2")
        buf.write("\u0140\u0142\3\2\2\2\u0141\u013f\3\2\2\2\u0142\u0183\7")
        buf.write("\62\2\2\u0143\u0144\7-\2\2\u0144\u0145\5\36\20\2\u0145")
        buf.write("\u0146\7C\2\2\u0146\u0147\5\36\20\2\u0147\u0148\7.\2\2")
        buf.write("\u0148\u0183\3\2\2\2\u0149\u0159\7/\2\2\u014a\u014b\5")
        buf.write("\36\20\2\u014b\u014c\7\64\2\2\u014c\u0154\5\36\20\2\u014d")
        buf.write("\u014e\7C\2\2\u014e\u014f\5\36\20\2\u014f\u0150\7\64\2")
        buf.write("\2\u0150\u0151\5\36\20\2\u0151\u0153\3\2\2\2\u0152\u014d")
        buf.write("\3\2\2\2\u0153\u0156\3\2\2\2\u0154\u0152\3\2\2\2\u0154")
        buf.write("\u0155\3\2\2\2\u0155\u0158\3\2\2\2\u0156\u0154\3\2\2\2")
        buf.write("\u0157\u014a\3\2\2\2\u0158\u015b\3\2\2\2\u0159\u0157\3")
        buf.write("\2\2\2\u0159\u015a\3\2\2\2\u015a\u015c\3\2\2\2\u015b\u0159")
        buf.write("\3\2\2\2\u015c\u0183\7\60\2\2\u015d\u015e\7N\2\2\u015e")
        buf.write("\u016d\7/\2\2\u015f\u0160\7N\2\2\u0160\u0161\7\64\2\2")
        buf.write("\u0161\u0168\5\36\20\2\u0162\u0163\7C\2\2\u0163\u0164")
        buf.write("\7N\2\2\u0164\u0165\7\64\2\2\u0165\u0167\5\36\20\2\u0166")
        buf.write("\u0162\3\2\2\2\u0167\u016a\3\2\2\2\u0168\u0166\3\2\2\2")
        buf.write("\u0168\u0169\3\2\2\2\u0169\u016c\3\2\2\2\u016a\u0168\3")
        buf.write("\2\2\2\u016b\u015f\3\2\2\2\u016c\u016f\3\2\2\2\u016d\u016b")
        buf.write("\3\2\2\2\u016d\u016e\3\2\2\2\u016e\u0170\3\2\2\2\u016f")
        buf.write("\u016d\3\2\2\2\u0170\u0183\7\60\2\2\u0171\u0172\7\n\2")
        buf.write("\2\u0172\u0173\5\36\20\2\u0173\u0174\7\13\2\2\u0174\u0175")
        buf.write("\5\36\20\2\u0175\u0176\7\f\2\2\u0176\u0177\5\36\20\2\u0177")
        buf.write("\u0183\3\2\2\2\u0178\u0179\7-\2\2\u0179\u017a\5\36\20")
        buf.write("\2\u017a\u017b\7.\2\2\u017b\u0183\3\2\2\2\u017c\u017d")
        buf.write("\7F\2\2\u017d\u0183\5\36\20\2\u017e\u017f\t\4\2\2\u017f")
        buf.write("\u0183\5\36\20\2\u0180\u0183\5\34\17\2\u0181\u0183\7N")
        buf.write("\2\2\u0182\u0126\3\2\2\2\u0182\u0134\3\2\2\2\u0182\u0143")
        buf.write("\3\2\2\2\u0182\u0149\3\2\2\2\u0182\u015d\3\2\2\2\u0182")
        buf.write("\u0171\3\2\2\2\u0182\u0178\3\2\2\2\u0182\u017c\3\2\2\2")
        buf.write("\u0182\u017e\3\2\2\2\u0182\u0180\3\2\2\2\u0182\u0181\3")
        buf.write("\2\2\2\u0183\u018e\3\2\2\2\u0184\u0185\f\b\2\2\u0185\u0186")
        buf.write("\7\61\2\2\u0186\u0187\5\36\20\2\u0187\u0188\7\62\2\2\u0188")
        buf.write("\u018d\3\2\2\2\u0189\u018a\f\7\2\2\u018a\u018b\7E\2\2")
        buf.write("\u018b\u018d\7N\2\2\u018c\u0184\3\2\2\2\u018c\u0189\3")
        buf.write("\2\2\2\u018d\u0190\3\2\2\2\u018e\u018c\3\2\2\2\u018e\u018f")
        buf.write("\3\2\2\2\u018f/\3\2\2\2\u0190\u018e\3\2\2\2\u0191\u0192")
        buf.write("\7\3\2\2\u0192\u0193\7V\2\2\u0193\61\3\2\2\2\u0194\u0195")
        buf.write("\7\r\2\2\u0195\u0196\7N\2\2\u0196\u0197\7\16\2\2\u0197")
        buf.write("\u0198\7N\2\2\u0198\63\3\2\2\2\u0199\u019a\7\16\2\2\u019a")
        buf.write("\u019b\7N\2\2\u019b\65\3\2\2\2\u019c\u019d\7\4\2\2\u019d")
        buf.write("\u019f\5\32\16\2\u019e\u01a0\5\64\33\2\u019f\u019e\3\2")
        buf.write("\2\2\u019f\u01a0\3\2\2\2\u01a0\u01a4\3\2\2\2\u01a1\u01a3")
        buf.write("\5\62\32\2\u01a2\u01a1\3\2\2\2\u01a3\u01a6\3\2\2\2\u01a4")
        buf.write("\u01a2\3\2\2\2\u01a4\u01a5\3\2\2\2\u01a5\67\3\2\2\2\u01a6")
        buf.write("\u01a4\3\2\2\2\u01a7\u01a8\7\7\2\2\u01a8\u01a9\7N\2\2")
        buf.write("\u01a9\u01ad\7/\2\2\u01aa\u01ac\5\f\7\2\u01ab\u01aa\3")
        buf.write("\2\2\2\u01ac\u01af\3\2\2\2\u01ad\u01ab\3\2\2\2\u01ad\u01ae")
        buf.write("\3\2\2\2\u01ae\u01b0\3\2\2\2\u01af\u01ad\3\2\2\2\u01b0")
        buf.write("\u01b1\7\60\2\2\u01b19\3\2\2\2\u01b2\u01b3\7N\2\2\u01b3")
        buf.write("\u01b4\7\64\2\2\u01b4\u01b5\5\36\20\2\u01b5;\3\2\2\2\u01b6")
        buf.write("\u01b7\7\22\2\2\u01b7\u01bb\7/\2\2\u01b8\u01ba\5:\36\2")
        buf.write("\u01b9\u01b8\3\2\2\2\u01ba\u01bd\3\2\2\2\u01bb\u01b9\3")
        buf.write("\2\2\2\u01bb\u01bc\3\2\2\2\u01bc\u01be\3\2\2\2\u01bd\u01bb")
        buf.write("\3\2\2\2\u01be\u01bf\7\60\2\2\u01bf=\3\2\2\2\u01c0\u01c1")
        buf.write("\7\23\2\2\u01c1\u01c5\7/\2\2\u01c2\u01c4\5:\36\2\u01c3")
        buf.write("\u01c2\3\2\2\2\u01c4\u01c7\3\2\2\2\u01c5\u01c3\3\2\2\2")
        buf.write("\u01c5\u01c6\3\2\2\2\u01c6\u01c8\3\2\2\2\u01c7\u01c5\3")
        buf.write("\2\2\2\u01c8\u01c9\7\60\2\2\u01c9?\3\2\2\2\u01ca\u01cb")
        buf.write("\7\26\2\2\u01cb\u01cc\7\64\2\2\u01cc\u01e0\5\36\20\2\u01cd")
        buf.write("\u01ce\7\27\2\2\u01ce\u01cf\7\64\2\2\u01cf\u01e0\5\36")
        buf.write("\20\2\u01d0\u01d1\7\30\2\2\u01d1\u01d2\7\64\2\2\u01d2")
        buf.write("\u01e0\5\36\20\2\u01d3\u01d4\7\31\2\2\u01d4\u01d5\7\64")
        buf.write("\2\2\u01d5\u01e0\5\36\20\2\u01d6\u01d7\7\32\2\2\u01d7")
        buf.write("\u01d8\7\64\2\2\u01d8\u01e0\5\36\20\2\u01d9\u01da\7\33")
        buf.write("\2\2\u01da\u01db\7\64\2\2\u01db\u01e0\5\36\20\2\u01dc")
        buf.write("\u01dd\7\34\2\2\u01dd\u01de\7\64\2\2\u01de\u01e0\5\36")
        buf.write("\20\2\u01df\u01ca\3\2\2\2\u01df\u01cd\3\2\2\2\u01df\u01d0")
        buf.write("\3\2\2\2\u01df\u01d3\3\2\2\2\u01df\u01d6\3\2\2\2\u01df")
        buf.write("\u01d9\3\2\2\2\u01df\u01dc\3\2\2\2\u01e0A\3\2\2\2\u01e1")
        buf.write("\u01e2\7\25\2\2\u01e2\u01e6\7/\2\2\u01e3\u01e5\5@!\2\u01e4")
        buf.write("\u01e3\3\2\2\2\u01e5\u01e8\3\2\2\2\u01e6\u01e4\3\2\2\2")
        buf.write("\u01e6\u01e7\3\2\2\2\u01e7\u01e9\3\2\2\2\u01e8\u01e6\3")
        buf.write("\2\2\2\u01e9\u01ea\7\60\2\2\u01eaC\3\2\2\2\u01eb\u01ec")
        buf.write("\7N\2\2\u01ec\u01ed\7\64\2\2\u01ed\u01ee\5\36\20\2\u01ee")
        buf.write("E\3\2\2\2\u01ef\u01f0\7\24\2\2\u01f0\u01f4\7/\2\2\u01f1")
        buf.write("\u01f3\5D#\2\u01f2\u01f1\3\2\2\2\u01f3\u01f6\3\2\2\2\u01f4")
        buf.write("\u01f2\3\2\2\2\u01f4\u01f5\3\2\2\2\u01f5\u01f7\3\2\2\2")
        buf.write("\u01f6\u01f4\3\2\2\2\u01f7\u01f8\7\60\2\2\u01f8G\3\2\2")
        buf.write("\2\u01f9\u01fa\7\20\2\2\u01fa\u01fe\7/\2\2\u01fb\u01fd")
        buf.write("\5\20\t\2\u01fc\u01fb\3\2\2\2\u01fd\u0200\3\2\2\2\u01fe")
        buf.write("\u01fc\3\2\2\2\u01fe\u01ff\3\2\2\2\u01ff\u0201\3\2\2\2")
        buf.write("\u0200\u01fe\3\2\2\2\u0201\u0202\7\60\2\2\u0202I\3\2\2")
        buf.write("\2\u0203\u0204\7\21\2\2\u0204\u0208\7/\2\2\u0205\u0207")
        buf.write("\5\16\b\2\u0206\u0205\3\2\2\2\u0207\u020a\3\2\2\2\u0208")
        buf.write("\u0206\3\2\2\2\u0208\u0209\3\2\2\2\u0209\u020b\3\2\2\2")
        buf.write("\u020a\u0208\3\2\2\2\u020b\u020c\7\60\2\2\u020cK\3\2\2")
        buf.write("\2\u020d\u020e\7(\2\2\u020e\u0212\5N(\2\u020f\u0211\5")
        buf.write("R*\2\u0210\u020f\3\2\2\2\u0211\u0214\3\2\2\2\u0212\u0210")
        buf.write("\3\2\2\2\u0212\u0213\3\2\2\2\u0213\u0215\3\2\2\2\u0214")
        buf.write("\u0212\3\2\2\2\u0215\u0216\7S\2\2\u0216\u0222\3\2\2\2")
        buf.write("\u0217\u0218\7\'\2\2\u0218\u021c\5N(\2\u0219\u021b\5R")
        buf.write("*\2\u021a\u0219\3\2\2\2\u021b\u021e\3\2\2\2\u021c\u021a")
        buf.write("\3\2\2\2\u021c\u021d\3\2\2\2\u021d\u021f\3\2\2\2\u021e")
        buf.write("\u021c\3\2\2\2\u021f\u0220\7S\2\2\u0220\u0222\3\2\2\2")
        buf.write("\u0221\u020d\3\2\2\2\u0221\u0217\3\2\2\2\u0222M\3\2\2")
        buf.write("\2\u0223\u0225\7T\2\2\u0224\u0223\3\2\2\2\u0225\u0228")
        buf.write("\3\2\2\2\u0226\u0224\3\2\2\2\u0226\u0227\3\2\2\2\u0227")
        buf.write("O\3\2\2\2\u0228\u0226\3\2\2\2\u0229\u022a\7R\2\2\u022a")
        buf.write("\u022b\5\36\20\2\u022b\u022c\7\60\2\2\u022cQ\3\2\2\2\u022d")
        buf.write("\u022e\5P)\2\u022e\u022f\5N(\2\u022fS\3\2\2\2\u0230\u0239")
        buf.write("\5H%\2\u0231\u0239\5J&\2\u0232\u0239\5L\'\2\u0233\u0239")
        buf.write("\5B\"\2\u0234\u0239\5F$\2\u0235\u0239\5\16\b\2\u0236\u0239")
        buf.write("\5<\37\2\u0237\u0239\5> \2\u0238\u0230\3\2\2\2\u0238\u0231")
        buf.write("\3\2\2\2\u0238\u0232\3\2\2\2\u0238\u0233\3\2\2\2\u0238")
        buf.write("\u0234\3\2\2\2\u0238\u0235\3\2\2\2\u0238\u0236\3\2\2\2")
        buf.write("\u0238\u0237\3\2\2\2\u0239U\3\2\2\2\u023a\u023b\7\6\2")
        buf.write("\2\u023b\u023c\7N\2\2\u023c\u023e\7/\2\2\u023d\u023f\5")
        buf.write("T+\2\u023e\u023d\3\2\2\2\u023f\u0240\3\2\2\2\u0240\u023e")
        buf.write("\3\2\2\2\u0240\u0241\3\2\2\2\u0241\u0242\3\2\2\2\u0242")
        buf.write("\u0243\7\60\2\2\u0243W\3\2\2\2\u0244\u0249\5\16\b\2\u0245")
        buf.write("\u0249\5f\64\2\u0246\u0249\5h\65\2\u0247\u0249\5j\66\2")
        buf.write("\u0248\u0244\3\2\2\2\u0248\u0245\3\2\2\2\u0248\u0246\3")
        buf.write("\2\2\2\u0248\u0247\3\2\2\2\u0249Y\3\2\2\2\u024a\u024b")
        buf.write("\7\16\2\2\u024b\u024c\7N\2\2\u024c[\3\2\2\2\u024d\u024e")
        buf.write("\7N\2\2\u024e\u024f\7;\2\2\u024f\u0250\5\36\20\2\u0250")
        buf.write("]\3\2\2\2\u0251\u0252\7\20\2\2\u0252\u0253\7\64\2\2\u0253")
        buf.write("\u0258\5\\/\2\u0254\u0255\7C\2\2\u0255\u0257\5\\/\2\u0256")
        buf.write("\u0254\3\2\2\2\u0257\u025a\3\2\2\2\u0258\u0256\3\2\2\2")
        buf.write("\u0258\u0259\3\2\2\2\u0259_\3\2\2\2\u025a\u0258\3\2\2")
        buf.write("\2\u025b\u025d\7/\2\2\u025c\u025e\5^\60\2\u025d\u025c")
        buf.write("\3\2\2\2\u025d\u025e\3\2\2\2\u025e\u025f\3\2\2\2\u025f")
        buf.write("\u0260\7\60\2\2\u0260a\3\2\2\2\u0261\u0262\7&\2\2\u0262")
        buf.write("\u0263\7N\2\2\u0263c\3\2\2\2\u0264\u0269\7N\2\2\u0265")
        buf.write("\u0266\7E\2\2\u0266\u0268\7N\2\2\u0267\u0265\3\2\2\2\u0268")
        buf.write("\u026b\3\2\2\2\u0269\u0267\3\2\2\2\u0269\u026a\3\2\2\2")
        buf.write("\u026ae\3\2\2\2\u026b\u0269\3\2\2\2\u026c\u026d\7\t\2")
        buf.write("\2\u026d\u026f\5d\63\2\u026e\u0270\5Z.\2\u026f\u026e\3")
        buf.write("\2\2\2\u026f\u0270\3\2\2\2\u0270\u0274\3\2\2\2\u0271\u0273")
        buf.write("\5b\62\2\u0272\u0271\3\2\2\2\u0273\u0276\3\2\2\2\u0274")
        buf.write("\u0272\3\2\2\2\u0274\u0275\3\2\2\2\u0275\u0278\3\2\2\2")
        buf.write("\u0276\u0274\3\2\2\2\u0277\u0279\5`\61\2\u0278\u0277\3")
        buf.write("\2\2\2\u0278\u0279\3\2\2\2\u0279g\3\2\2\2\u027a\u027b")
        buf.write("\7\b\2\2\u027b\u027c\7-\2\2\u027c\u027d\7N\2\2\u027d\u027e")
        buf.write("\7\17\2\2\u027e\u027f\5\36\20\2\u027f\u0280\7.\2\2\u0280")
        buf.write("\u0284\7/\2\2\u0281\u0283\5X-\2\u0282\u0281\3\2\2\2\u0283")
        buf.write("\u0286\3\2\2\2\u0284\u0282\3\2\2\2\u0284\u0285\3\2\2\2")
        buf.write("\u0285\u0287\3\2\2\2\u0286\u0284\3\2\2\2\u0287\u0288\7")
        buf.write("\60\2\2\u0288i\3\2\2\2\u0289\u028a\7\n\2\2\u028a\u028b")
        buf.write("\7-\2\2\u028b\u028c\5\36\20\2\u028c\u028d\7.\2\2\u028d")
        buf.write("\u0291\7/\2\2\u028e\u0290\5X-\2\u028f\u028e\3\2\2\2\u0290")
        buf.write("\u0293\3\2\2\2\u0291\u028f\3\2\2\2\u0291\u0292\3\2\2\2")
        buf.write("\u0292\u0294\3\2\2\2\u0293\u0291\3\2\2\2\u0294\u0295\7")
        buf.write("\60\2\2\u0295k\3\2\2\2\u0296\u0297\7\20\2\2\u0297\u029b")
        buf.write("\7/\2\2\u0298\u029a\5\20\t\2\u0299\u0298\3\2\2\2\u029a")
        buf.write("\u029d\3\2\2\2\u029b\u0299\3\2\2\2\u029b\u029c\3\2\2\2")
        buf.write("\u029c\u029e\3\2\2\2\u029d\u029b\3\2\2\2\u029e\u029f\7")
        buf.write("\60\2\2\u029fm\3\2\2\2\u02a0\u02a1\7\21\2\2\u02a1\u02a5")
        buf.write("\7/\2\2\u02a2\u02a4\5\16\b\2\u02a3\u02a2\3\2\2\2\u02a4")
        buf.write("\u02a7\3\2\2\2\u02a5\u02a3\3\2\2\2\u02a5\u02a6\3\2\2\2")
        buf.write("\u02a6\u02a8\3\2\2\2\u02a7\u02a5\3\2\2\2\u02a8\u02a9\7")
        buf.write("\60\2\2\u02a9o\3\2\2\2\u02aa\u02b0\5l\67\2\u02ab\u02b0")
        buf.write("\5n8\2\u02ac\u02b0\5X-\2\u02ad\u02b0\5<\37\2\u02ae\u02b0")
        buf.write("\5> \2\u02af\u02aa\3\2\2\2\u02af\u02ab\3\2\2\2\u02af\u02ac")
        buf.write("\3\2\2\2\u02af\u02ad\3\2\2\2\u02af\u02ae\3\2\2\2\u02b0")
        buf.write("q\3\2\2\2\u02b1\u02b2\7\5\2\2\u02b2\u02b3\7N\2\2\u02b3")
        buf.write("\u02b7\7/\2\2\u02b4\u02b6\5p9\2\u02b5\u02b4\3\2\2\2\u02b6")
        buf.write("\u02b9\3\2\2\2\u02b7\u02b5\3\2\2\2\u02b7\u02b8\3\2\2\2")
        buf.write("\u02b8\u02ba\3\2\2\2\u02b9\u02b7\3\2\2\2\u02ba\u02bb\7")
        buf.write("\60\2\2\u02bbs\3\2\2\2\u02bc\u02c1\5\66\34\2\u02bd\u02c1")
        buf.write("\58\35\2\u02be\u02c1\5V,\2\u02bf\u02c1\5r:\2\u02c0\u02bc")
        buf.write("\3\2\2\2\u02c0\u02bd\3\2\2\2\u02c0\u02be\3\2\2\2\u02c0")
        buf.write("\u02bf\3\2\2\2\u02c1u\3\2\2\2\u02c2\u02c6\5\60\31\2\u02c3")
        buf.write("\u02c5\5t;\2\u02c4\u02c3\3\2\2\2\u02c5\u02c8\3\2\2\2\u02c6")
        buf.write("\u02c4\3\2\2\2\u02c6\u02c7\3\2\2\2\u02c7\u02d0\3\2\2\2")
        buf.write("\u02c8\u02c6\3\2\2\2\u02c9\u02cd\5r:\2\u02ca\u02cc\5t")
        buf.write(";\2\u02cb\u02ca\3\2\2\2\u02cc\u02cf\3\2\2\2\u02cd\u02cb")
        buf.write("\3\2\2\2\u02cd\u02ce\3\2\2\2\u02ce\u02d1\3\2\2\2\u02cf")
        buf.write("\u02cd\3\2\2\2\u02d0\u02c9\3\2\2\2\u02d0\u02d1\3\2\2\2")
        buf.write("\u02d1\u02d2\3\2\2\2\u02d2\u02d3\7\2\2\3\u02d3w\3\2\2")
        buf.write("\2?\u0084\u0091\u0097\u00a3\u00aa\u00b9\u00c3\u00c8\u00cf")
        buf.write("\u00dd\u00e8\u0100\u0102\u010e\u0110\u011f\u0121\u012e")
        buf.write("\u0131\u013a\u013f\u0154\u0159\u0168\u016d\u0182\u018c")
        buf.write("\u018e\u019f\u01a4\u01ad\u01bb\u01c5\u01df\u01e6\u01f4")
        buf.write("\u01fe\u0208\u0212\u021c\u0221\u0226\u0238\u0240\u0248")
        buf.write("\u0258\u025d\u0269\u026f\u0274\u0278\u0284\u0291\u029b")
        buf.write("\u02a5\u02af\u02b7\u02c0\u02c6\u02cd\u02d0")
        return buf.getvalue()


class WdlParser ( Parser ):

    grammarFileName = "WdlParser.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'version'", "'import'", "'workflow'", 
                     "'task'", "'struct'", "'scatter'", "'call'", "'if'", 
                     "'then'", "'else'", "'alias'", "'as'", "'in'", "'input'", 
                     "'output'", "'parameter_meta'", "'meta'", "'hints'", 
                     "'runtime'", "'cpu'", "'container'", "'memory'", "'gpu'", 
                     "'disks'", "'maxRetries'", "'returnCodes'", "'Boolean'", 
                     "'Int'", "'Float'", "'String'", "'File'", "'Directory'", 
                     "'Array'", "'Map'", "'Pair'", "'after'", "<INVALID>", 
                     "<INVALID>", "'None'", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'('", "')'", "<INVALID>", "<INVALID>", "'['", "']'", 
                     "'\\'", "':'", "'<'", "'>'", "'>='", "'<='", "'=='", 
                     "'!='", "'='", "'&&'", "'||'", "'?'", "'*'", "'+'", 
                     "'-'", "<INVALID>", "','", "';'", "'.'", "'!'", "<INVALID>", 
                     "'/'", "'%'", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "'\\>>>'", "'>>>'" ]

    symbolicNames = [ "<INVALID>", "VERSION", "IMPORT", "WORKFLOW", "TASK", 
                      "STRUCT", "SCATTER", "CALL", "IF", "THEN", "ELSE", 
                      "ALIAS", "AS", "In", "INPUT", "OUTPUT", "PARAMETERMETA", 
                      "META", "HINTS", "RUNTIME", "RUNTIMECPU", "RUNTIMECONTAINER", 
                      "RUNTIMEMEMORY", "RUNTIMEGPU", "RUNTIMEDISKS", "RUNTIMEMAXRETRIES", 
                      "RUNTIMERETURNCODES", "BOOLEAN", "INT", "FLOAT", "STRING", 
                      "FILE", "DIRECTORY", "ARRAY", "MAP", "PAIR", "AFTER", 
                      "HEREDOC_COMMAND", "COMMAND", "NONELITERAL", "IntLiteral", 
                      "FloatLiteral", "BoolLiteral", "LPAREN", "RPAREN", 
                      "LBRACE", "RBRACE", "LBRACK", "RBRACK", "ESC", "COLON", 
                      "LT", "GT", "GTE", "LTE", "EQUALITY", "NOTEQUAL", 
                      "EQUAL", "AND", "OR", "OPTIONAL", "STAR", "PLUS", 
                      "MINUS", "DOLLAR", "COMMA", "SEMI", "DOT", "NOT", 
                      "TILDE", "DIVIDE", "MOD", "SQUOTE", "DQUOTE", "WHITESPACE", 
                      "COMMENT", "Identifier", "StringPart", "HereDocUnicodeEscape", 
                      "CommandUnicodeEscape", "StringCommandStart", "EndCommand", 
                      "CommandStringPart", "VERSION_WHITESPACE", "RELEASE_VERSION", 
                      "HereDocEscapedEnd", "EndHereDocCommand" ]

    RULE_map_type = 0
    RULE_array_type = 1
    RULE_pair_type = 2
    RULE_type_base = 3
    RULE_wdl_type = 4
    RULE_unbound_decls = 5
    RULE_bound_decls = 6
    RULE_any_decls = 7
    RULE_number = 8
    RULE_string_part = 9
    RULE_string_expr_part = 10
    RULE_string_expr_with_string_part = 11
    RULE_string = 12
    RULE_primitive_literal = 13
    RULE_expr = 14
    RULE_expr_infix = 15
    RULE_expr_infix0 = 16
    RULE_expr_infix1 = 17
    RULE_expr_infix2 = 18
    RULE_expr_infix3 = 19
    RULE_expr_infix4 = 20
    RULE_expr_infix5 = 21
    RULE_expr_core = 22
    RULE_version = 23
    RULE_import_alias = 24
    RULE_import_as = 25
    RULE_import_doc = 26
    RULE_struct = 27
    RULE_meta_kv = 28
    RULE_parameter_meta = 29
    RULE_meta = 30
    RULE_task_runtime_kv = 31
    RULE_task_runtime = 32
    RULE_task_hints_kv = 33
    RULE_task_hints = 34
    RULE_task_input = 35
    RULE_task_output = 36
    RULE_task_command = 37
    RULE_task_command_string_part = 38
    RULE_task_command_expr_part = 39
    RULE_task_command_expr_with_string = 40
    RULE_task_element = 41
    RULE_task = 42
    RULE_inner_workflow_element = 43
    RULE_call_alias = 44
    RULE_call_input = 45
    RULE_call_inputs = 46
    RULE_call_body = 47
    RULE_call_afters = 48
    RULE_call_name = 49
    RULE_call = 50
    RULE_scatter = 51
    RULE_conditional = 52
    RULE_workflow_input = 53
    RULE_workflow_output = 54
    RULE_workflow_element = 55
    RULE_workflow = 56
    RULE_document_element = 57
    RULE_document = 58

    ruleNames =  [ "map_type", "array_type", "pair_type", "type_base", "wdl_type", 
                   "unbound_decls", "bound_decls", "any_decls", "number", 
                   "string_part", "string_expr_part", "string_expr_with_string_part", 
                   "string", "primitive_literal", "expr", "expr_infix", 
                   "expr_infix0", "expr_infix1", "expr_infix2", "expr_infix3", 
                   "expr_infix4", "expr_infix5", "expr_core", "version", 
                   "import_alias", "import_as", "import_doc", "struct", 
                   "meta_kv", "parameter_meta", "meta", "task_runtime_kv", 
                   "task_runtime", "task_hints_kv", "task_hints", "task_input", 
                   "task_output", "task_command", "task_command_string_part", 
                   "task_command_expr_part", "task_command_expr_with_string", 
                   "task_element", "task", "inner_workflow_element", "call_alias", 
                   "call_input", "call_inputs", "call_body", "call_afters", 
                   "call_name", "call", "scatter", "conditional", "workflow_input", 
                   "workflow_output", "workflow_element", "workflow", "document_element", 
                   "document" ]

    EOF = Token.EOF
    VERSION=1
    IMPORT=2
    WORKFLOW=3
    TASK=4
    STRUCT=5
    SCATTER=6
    CALL=7
    IF=8
    THEN=9
    ELSE=10
    ALIAS=11
    AS=12
    In=13
    INPUT=14
    OUTPUT=15
    PARAMETERMETA=16
    META=17
    HINTS=18
    RUNTIME=19
    RUNTIMECPU=20
    RUNTIMECONTAINER=21
    RUNTIMEMEMORY=22
    RUNTIMEGPU=23
    RUNTIMEDISKS=24
    RUNTIMEMAXRETRIES=25
    RUNTIMERETURNCODES=26
    BOOLEAN=27
    INT=28
    FLOAT=29
    STRING=30
    FILE=31
    DIRECTORY=32
    ARRAY=33
    MAP=34
    PAIR=35
    AFTER=36
    HEREDOC_COMMAND=37
    COMMAND=38
    NONELITERAL=39
    IntLiteral=40
    FloatLiteral=41
    BoolLiteral=42
    LPAREN=43
    RPAREN=44
    LBRACE=45
    RBRACE=46
    LBRACK=47
    RBRACK=48
    ESC=49
    COLON=50
    LT=51
    GT=52
    GTE=53
    LTE=54
    EQUALITY=55
    NOTEQUAL=56
    EQUAL=57
    AND=58
    OR=59
    OPTIONAL=60
    STAR=61
    PLUS=62
    MINUS=63
    DOLLAR=64
    COMMA=65
    SEMI=66
    DOT=67
    NOT=68
    TILDE=69
    DIVIDE=70
    MOD=71
    SQUOTE=72
    DQUOTE=73
    WHITESPACE=74
    COMMENT=75
    Identifier=76
    StringPart=77
    HereDocUnicodeEscape=78
    CommandUnicodeEscape=79
    StringCommandStart=80
    EndCommand=81
    CommandStringPart=82
    VERSION_WHITESPACE=83
    RELEASE_VERSION=84
    HereDocEscapedEnd=85
    EndHereDocCommand=86

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class Map_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MAP(self):
            return self.getToken(WdlParser.MAP, 0)

        def LBRACK(self):
            return self.getToken(WdlParser.LBRACK, 0)

        def wdl_type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Wdl_typeContext)
            else:
                return self.getTypedRuleContext(WdlParser.Wdl_typeContext,i)


        def COMMA(self):
            return self.getToken(WdlParser.COMMA, 0)

        def RBRACK(self):
            return self.getToken(WdlParser.RBRACK, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_map_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMap_type" ):
                listener.enterMap_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMap_type" ):
                listener.exitMap_type(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMap_type" ):
                return visitor.visitMap_type(self)
            else:
                return visitor.visitChildren(self)




    def map_type(self):

        localctx = WdlParser.Map_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_map_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 118
            self.match(WdlParser.MAP)
            self.state = 119
            self.match(WdlParser.LBRACK)
            self.state = 120
            self.wdl_type()
            self.state = 121
            self.match(WdlParser.COMMA)
            self.state = 122
            self.wdl_type()
            self.state = 123
            self.match(WdlParser.RBRACK)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Array_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ARRAY(self):
            return self.getToken(WdlParser.ARRAY, 0)

        def LBRACK(self):
            return self.getToken(WdlParser.LBRACK, 0)

        def wdl_type(self):
            return self.getTypedRuleContext(WdlParser.Wdl_typeContext,0)


        def RBRACK(self):
            return self.getToken(WdlParser.RBRACK, 0)

        def PLUS(self):
            return self.getToken(WdlParser.PLUS, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_array_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArray_type" ):
                listener.enterArray_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArray_type" ):
                listener.exitArray_type(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArray_type" ):
                return visitor.visitArray_type(self)
            else:
                return visitor.visitChildren(self)




    def array_type(self):

        localctx = WdlParser.Array_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_array_type)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 125
            self.match(WdlParser.ARRAY)
            self.state = 126
            self.match(WdlParser.LBRACK)
            self.state = 127
            self.wdl_type()
            self.state = 128
            self.match(WdlParser.RBRACK)
            self.state = 130
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlParser.PLUS:
                self.state = 129
                self.match(WdlParser.PLUS)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Pair_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PAIR(self):
            return self.getToken(WdlParser.PAIR, 0)

        def LBRACK(self):
            return self.getToken(WdlParser.LBRACK, 0)

        def wdl_type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Wdl_typeContext)
            else:
                return self.getTypedRuleContext(WdlParser.Wdl_typeContext,i)


        def COMMA(self):
            return self.getToken(WdlParser.COMMA, 0)

        def RBRACK(self):
            return self.getToken(WdlParser.RBRACK, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_pair_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPair_type" ):
                listener.enterPair_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPair_type" ):
                listener.exitPair_type(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPair_type" ):
                return visitor.visitPair_type(self)
            else:
                return visitor.visitChildren(self)




    def pair_type(self):

        localctx = WdlParser.Pair_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_pair_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 132
            self.match(WdlParser.PAIR)
            self.state = 133
            self.match(WdlParser.LBRACK)
            self.state = 134
            self.wdl_type()
            self.state = 135
            self.match(WdlParser.COMMA)
            self.state = 136
            self.wdl_type()
            self.state = 137
            self.match(WdlParser.RBRACK)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_baseContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def array_type(self):
            return self.getTypedRuleContext(WdlParser.Array_typeContext,0)


        def map_type(self):
            return self.getTypedRuleContext(WdlParser.Map_typeContext,0)


        def pair_type(self):
            return self.getTypedRuleContext(WdlParser.Pair_typeContext,0)


        def STRING(self):
            return self.getToken(WdlParser.STRING, 0)

        def FILE(self):
            return self.getToken(WdlParser.FILE, 0)

        def DIRECTORY(self):
            return self.getToken(WdlParser.DIRECTORY, 0)

        def BOOLEAN(self):
            return self.getToken(WdlParser.BOOLEAN, 0)

        def INT(self):
            return self.getToken(WdlParser.INT, 0)

        def FLOAT(self):
            return self.getToken(WdlParser.FLOAT, 0)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_type_base

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_base" ):
                listener.enterType_base(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_base" ):
                listener.exitType_base(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_base" ):
                return visitor.visitType_base(self)
            else:
                return visitor.visitChildren(self)




    def type_base(self):

        localctx = WdlParser.Type_baseContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_type_base)
        self._la = 0 # Token type
        try:
            self.state = 143
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlParser.ARRAY]:
                self.enterOuterAlt(localctx, 1)
                self.state = 139
                self.array_type()
                pass
            elif token in [WdlParser.MAP]:
                self.enterOuterAlt(localctx, 2)
                self.state = 140
                self.map_type()
                pass
            elif token in [WdlParser.PAIR]:
                self.enterOuterAlt(localctx, 3)
                self.state = 141
                self.pair_type()
                pass
            elif token in [WdlParser.BOOLEAN, WdlParser.INT, WdlParser.FLOAT, WdlParser.STRING, WdlParser.FILE, WdlParser.DIRECTORY, WdlParser.Identifier]:
                self.enterOuterAlt(localctx, 4)
                self.state = 142
                _la = self._input.LA(1)
                if not(((((_la - 27)) & ~0x3f) == 0 and ((1 << (_la - 27)) & ((1 << (WdlParser.BOOLEAN - 27)) | (1 << (WdlParser.INT - 27)) | (1 << (WdlParser.FLOAT - 27)) | (1 << (WdlParser.STRING - 27)) | (1 << (WdlParser.FILE - 27)) | (1 << (WdlParser.DIRECTORY - 27)) | (1 << (WdlParser.Identifier - 27)))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Wdl_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_base(self):
            return self.getTypedRuleContext(WdlParser.Type_baseContext,0)


        def OPTIONAL(self):
            return self.getToken(WdlParser.OPTIONAL, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_wdl_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWdl_type" ):
                listener.enterWdl_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWdl_type" ):
                listener.exitWdl_type(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWdl_type" ):
                return visitor.visitWdl_type(self)
            else:
                return visitor.visitChildren(self)




    def wdl_type(self):

        localctx = WdlParser.Wdl_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_wdl_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 149
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.state = 145
                self.type_base()
                self.state = 146
                self.match(WdlParser.OPTIONAL)
                pass

            elif la_ == 2:
                self.state = 148
                self.type_base()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Unbound_declsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def wdl_type(self):
            return self.getTypedRuleContext(WdlParser.Wdl_typeContext,0)


        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_unbound_decls

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnbound_decls" ):
                listener.enterUnbound_decls(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnbound_decls" ):
                listener.exitUnbound_decls(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnbound_decls" ):
                return visitor.visitUnbound_decls(self)
            else:
                return visitor.visitChildren(self)




    def unbound_decls(self):

        localctx = WdlParser.Unbound_declsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_unbound_decls)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 151
            self.wdl_type()
            self.state = 152
            self.match(WdlParser.Identifier)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bound_declsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def wdl_type(self):
            return self.getTypedRuleContext(WdlParser.Wdl_typeContext,0)


        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def EQUAL(self):
            return self.getToken(WdlParser.EQUAL, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_bound_decls

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBound_decls" ):
                listener.enterBound_decls(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBound_decls" ):
                listener.exitBound_decls(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBound_decls" ):
                return visitor.visitBound_decls(self)
            else:
                return visitor.visitChildren(self)




    def bound_decls(self):

        localctx = WdlParser.Bound_declsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_bound_decls)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 154
            self.wdl_type()
            self.state = 155
            self.match(WdlParser.Identifier)
            self.state = 156
            self.match(WdlParser.EQUAL)
            self.state = 157
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Any_declsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def unbound_decls(self):
            return self.getTypedRuleContext(WdlParser.Unbound_declsContext,0)


        def bound_decls(self):
            return self.getTypedRuleContext(WdlParser.Bound_declsContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_any_decls

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAny_decls" ):
                listener.enterAny_decls(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAny_decls" ):
                listener.exitAny_decls(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAny_decls" ):
                return visitor.visitAny_decls(self)
            else:
                return visitor.visitChildren(self)




    def any_decls(self):

        localctx = WdlParser.Any_declsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_any_decls)
        try:
            self.state = 161
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 159
                self.unbound_decls()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 160
                self.bound_decls()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumberContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IntLiteral(self):
            return self.getToken(WdlParser.IntLiteral, 0)

        def FloatLiteral(self):
            return self.getToken(WdlParser.FloatLiteral, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_number

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumber" ):
                listener.enterNumber(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumber" ):
                listener.exitNumber(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNumber" ):
                return visitor.visitNumber(self)
            else:
                return visitor.visitChildren(self)




    def number(self):

        localctx = WdlParser.NumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_number)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 163
            _la = self._input.LA(1)
            if not(_la==WdlParser.IntLiteral or _la==WdlParser.FloatLiteral):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class String_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def StringPart(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.StringPart)
            else:
                return self.getToken(WdlParser.StringPart, i)

        def getRuleIndex(self):
            return WdlParser.RULE_string_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString_part" ):
                listener.enterString_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString_part" ):
                listener.exitString_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString_part" ):
                return visitor.visitString_part(self)
            else:
                return visitor.visitChildren(self)




    def string_part(self):

        localctx = WdlParser.String_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_string_part)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 168
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlParser.StringPart:
                self.state = 165
                self.match(WdlParser.StringPart)
                self.state = 170
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class String_expr_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def StringCommandStart(self):
            return self.getToken(WdlParser.StringCommandStart, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)


        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_string_expr_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString_expr_part" ):
                listener.enterString_expr_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString_expr_part" ):
                listener.exitString_expr_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString_expr_part" ):
                return visitor.visitString_expr_part(self)
            else:
                return visitor.visitChildren(self)




    def string_expr_part(self):

        localctx = WdlParser.String_expr_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_string_expr_part)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 171
            self.match(WdlParser.StringCommandStart)
            self.state = 172
            self.expr()
            self.state = 173
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class String_expr_with_string_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def string_expr_part(self):
            return self.getTypedRuleContext(WdlParser.String_expr_partContext,0)


        def string_part(self):
            return self.getTypedRuleContext(WdlParser.String_partContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_string_expr_with_string_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString_expr_with_string_part" ):
                listener.enterString_expr_with_string_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString_expr_with_string_part" ):
                listener.exitString_expr_with_string_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString_expr_with_string_part" ):
                return visitor.visitString_expr_with_string_part(self)
            else:
                return visitor.visitChildren(self)




    def string_expr_with_string_part(self):

        localctx = WdlParser.String_expr_with_string_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_string_expr_with_string_part)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 175
            self.string_expr_part()
            self.state = 176
            self.string_part()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StringContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DQUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.DQUOTE)
            else:
                return self.getToken(WdlParser.DQUOTE, i)

        def string_part(self):
            return self.getTypedRuleContext(WdlParser.String_partContext,0)


        def string_expr_with_string_part(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.String_expr_with_string_partContext)
            else:
                return self.getTypedRuleContext(WdlParser.String_expr_with_string_partContext,i)


        def SQUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.SQUOTE)
            else:
                return self.getToken(WdlParser.SQUOTE, i)

        def getRuleIndex(self):
            return WdlParser.RULE_string

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString" ):
                listener.enterString(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString" ):
                listener.exitString(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString" ):
                return visitor.visitString(self)
            else:
                return visitor.visitChildren(self)




    def string(self):

        localctx = WdlParser.StringContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_string)
        self._la = 0 # Token type
        try:
            self.state = 198
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlParser.DQUOTE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 178
                self.match(WdlParser.DQUOTE)
                self.state = 179
                self.string_part()
                self.state = 183
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlParser.StringCommandStart:
                    self.state = 180
                    self.string_expr_with_string_part()
                    self.state = 185
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 186
                self.match(WdlParser.DQUOTE)
                pass
            elif token in [WdlParser.SQUOTE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 188
                self.match(WdlParser.SQUOTE)
                self.state = 189
                self.string_part()
                self.state = 193
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlParser.StringCommandStart:
                    self.state = 190
                    self.string_expr_with_string_part()
                    self.state = 195
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 196
                self.match(WdlParser.SQUOTE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Primitive_literalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BoolLiteral(self):
            return self.getToken(WdlParser.BoolLiteral, 0)

        def number(self):
            return self.getTypedRuleContext(WdlParser.NumberContext,0)


        def string(self):
            return self.getTypedRuleContext(WdlParser.StringContext,0)


        def NONELITERAL(self):
            return self.getToken(WdlParser.NONELITERAL, 0)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_primitive_literal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimitive_literal" ):
                listener.enterPrimitive_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimitive_literal" ):
                listener.exitPrimitive_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrimitive_literal" ):
                return visitor.visitPrimitive_literal(self)
            else:
                return visitor.visitChildren(self)




    def primitive_literal(self):

        localctx = WdlParser.Primitive_literalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_primitive_literal)
        try:
            self.state = 205
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlParser.BoolLiteral]:
                self.enterOuterAlt(localctx, 1)
                self.state = 200
                self.match(WdlParser.BoolLiteral)
                pass
            elif token in [WdlParser.IntLiteral, WdlParser.FloatLiteral]:
                self.enterOuterAlt(localctx, 2)
                self.state = 201
                self.number()
                pass
            elif token in [WdlParser.SQUOTE, WdlParser.DQUOTE]:
                self.enterOuterAlt(localctx, 3)
                self.state = 202
                self.string()
                pass
            elif token in [WdlParser.NONELITERAL]:
                self.enterOuterAlt(localctx, 4)
                self.state = 203
                self.match(WdlParser.NONELITERAL)
                pass
            elif token in [WdlParser.Identifier]:
                self.enterOuterAlt(localctx, 5)
                self.state = 204
                self.match(WdlParser.Identifier)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr_infix(self):
            return self.getTypedRuleContext(WdlParser.Expr_infixContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = WdlParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 207
            self.expr_infix()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr_infixContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlParser.RULE_expr_infix

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class Infix0Context(Expr_infixContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infixContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix0(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix0Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix0" ):
                listener.enterInfix0(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix0" ):
                listener.exitInfix0(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix0" ):
                return visitor.visitInfix0(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix(self):

        localctx = WdlParser.Expr_infixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_expr_infix)
        try:
            localctx = WdlParser.Infix0Context(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 209
            self.expr_infix0(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr_infix0Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlParser.RULE_expr_infix0

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class Infix1Context(Expr_infix0Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix0Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix1(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix1Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix1" ):
                listener.enterInfix1(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix1" ):
                listener.exitInfix1(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix1" ):
                return visitor.visitInfix1(self)
            else:
                return visitor.visitChildren(self)


    class LorContext(Expr_infix0Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix0Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix0(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix0Context,0)

        def OR(self):
            return self.getToken(WdlParser.OR, 0)
        def expr_infix1(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix1Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLor" ):
                listener.enterLor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLor" ):
                listener.exitLor(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLor" ):
                return visitor.visitLor(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix0(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlParser.Expr_infix0Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 32
        self.enterRecursionRule(localctx, 32, self.RULE_expr_infix0, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            localctx = WdlParser.Infix1Context(self, localctx)
            self._ctx = localctx
            _prevctx = localctx

            self.state = 212
            self.expr_infix1(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 219
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,9,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = WdlParser.LorContext(self, WdlParser.Expr_infix0Context(self, _parentctx, _parentState))
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix0)
                    self.state = 214
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 215
                    self.match(WdlParser.OR)
                    self.state = 216
                    self.expr_infix1(0) 
                self.state = 221
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,9,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr_infix1Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlParser.RULE_expr_infix1

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class Infix2Context(Expr_infix1Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix1Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix2Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix2" ):
                listener.enterInfix2(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix2" ):
                listener.exitInfix2(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix2" ):
                return visitor.visitInfix2(self)
            else:
                return visitor.visitChildren(self)


    class LandContext(Expr_infix1Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix1Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix1(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix1Context,0)

        def AND(self):
            return self.getToken(WdlParser.AND, 0)
        def expr_infix2(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix2Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLand" ):
                listener.enterLand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLand" ):
                listener.exitLand(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLand" ):
                return visitor.visitLand(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix1(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlParser.Expr_infix1Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 34
        self.enterRecursionRule(localctx, 34, self.RULE_expr_infix1, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            localctx = WdlParser.Infix2Context(self, localctx)
            self._ctx = localctx
            _prevctx = localctx

            self.state = 223
            self.expr_infix2(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 230
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,10,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = WdlParser.LandContext(self, WdlParser.Expr_infix1Context(self, _parentctx, _parentState))
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix1)
                    self.state = 225
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 226
                    self.match(WdlParser.AND)
                    self.state = 227
                    self.expr_infix2(0) 
                self.state = 232
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,10,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr_infix2Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlParser.RULE_expr_infix2

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class EqeqContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix2Context,0)

        def EQUALITY(self):
            return self.getToken(WdlParser.EQUALITY, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEqeq" ):
                listener.enterEqeq(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEqeq" ):
                listener.exitEqeq(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEqeq" ):
                return visitor.visitEqeq(self)
            else:
                return visitor.visitChildren(self)


    class LtContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix2Context,0)

        def LT(self):
            return self.getToken(WdlParser.LT, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLt" ):
                listener.enterLt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLt" ):
                listener.exitLt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLt" ):
                return visitor.visitLt(self)
            else:
                return visitor.visitChildren(self)


    class Infix3Context(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix3(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix3" ):
                listener.enterInfix3(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix3" ):
                listener.exitInfix3(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix3" ):
                return visitor.visitInfix3(self)
            else:
                return visitor.visitChildren(self)


    class GteContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix2Context,0)

        def GTE(self):
            return self.getToken(WdlParser.GTE, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGte" ):
                listener.enterGte(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGte" ):
                listener.exitGte(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGte" ):
                return visitor.visitGte(self)
            else:
                return visitor.visitChildren(self)


    class NeqContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix2Context,0)

        def NOTEQUAL(self):
            return self.getToken(WdlParser.NOTEQUAL, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNeq" ):
                listener.enterNeq(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNeq" ):
                listener.exitNeq(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNeq" ):
                return visitor.visitNeq(self)
            else:
                return visitor.visitChildren(self)


    class LteContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix2Context,0)

        def LTE(self):
            return self.getToken(WdlParser.LTE, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLte" ):
                listener.enterLte(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLte" ):
                listener.exitLte(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLte" ):
                return visitor.visitLte(self)
            else:
                return visitor.visitChildren(self)


    class GtContext(Expr_infix2Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix2Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix2(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix2Context,0)

        def GT(self):
            return self.getToken(WdlParser.GT, 0)
        def expr_infix3(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix3Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGt" ):
                listener.enterGt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGt" ):
                listener.exitGt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGt" ):
                return visitor.visitGt(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix2(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlParser.Expr_infix2Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 36
        self.enterRecursionRule(localctx, 36, self.RULE_expr_infix2, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            localctx = WdlParser.Infix3Context(self, localctx)
            self._ctx = localctx
            _prevctx = localctx

            self.state = 234
            self.expr_infix3(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 256
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,12,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 254
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
                    if la_ == 1:
                        localctx = WdlParser.EqeqContext(self, WdlParser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 236
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 237
                        self.match(WdlParser.EQUALITY)
                        self.state = 238
                        self.expr_infix3(0)
                        pass

                    elif la_ == 2:
                        localctx = WdlParser.NeqContext(self, WdlParser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 239
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 240
                        self.match(WdlParser.NOTEQUAL)
                        self.state = 241
                        self.expr_infix3(0)
                        pass

                    elif la_ == 3:
                        localctx = WdlParser.LteContext(self, WdlParser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 242
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 243
                        self.match(WdlParser.LTE)
                        self.state = 244
                        self.expr_infix3(0)
                        pass

                    elif la_ == 4:
                        localctx = WdlParser.GteContext(self, WdlParser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 245
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 246
                        self.match(WdlParser.GTE)
                        self.state = 247
                        self.expr_infix3(0)
                        pass

                    elif la_ == 5:
                        localctx = WdlParser.LtContext(self, WdlParser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 248
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 249
                        self.match(WdlParser.LT)
                        self.state = 250
                        self.expr_infix3(0)
                        pass

                    elif la_ == 6:
                        localctx = WdlParser.GtContext(self, WdlParser.Expr_infix2Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix2)
                        self.state = 251
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 252
                        self.match(WdlParser.GT)
                        self.state = 253
                        self.expr_infix3(0)
                        pass

             
                self.state = 258
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,12,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr_infix3Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlParser.RULE_expr_infix3

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class AddContext(Expr_infix3Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix3Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix3(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix3Context,0)

        def PLUS(self):
            return self.getToken(WdlParser.PLUS, 0)
        def expr_infix4(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix4Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAdd" ):
                listener.enterAdd(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAdd" ):
                listener.exitAdd(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAdd" ):
                return visitor.visitAdd(self)
            else:
                return visitor.visitChildren(self)


    class SubContext(Expr_infix3Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix3Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix3(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix3Context,0)

        def MINUS(self):
            return self.getToken(WdlParser.MINUS, 0)
        def expr_infix4(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix4Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSub" ):
                listener.enterSub(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSub" ):
                listener.exitSub(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSub" ):
                return visitor.visitSub(self)
            else:
                return visitor.visitChildren(self)


    class Infix4Context(Expr_infix3Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix3Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix4(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix4Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix4" ):
                listener.enterInfix4(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix4" ):
                listener.exitInfix4(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix4" ):
                return visitor.visitInfix4(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix3(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlParser.Expr_infix3Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 38
        self.enterRecursionRule(localctx, 38, self.RULE_expr_infix3, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            localctx = WdlParser.Infix4Context(self, localctx)
            self._ctx = localctx
            _prevctx = localctx

            self.state = 260
            self.expr_infix4(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 270
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,14,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 268
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,13,self._ctx)
                    if la_ == 1:
                        localctx = WdlParser.AddContext(self, WdlParser.Expr_infix3Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix3)
                        self.state = 262
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 263
                        self.match(WdlParser.PLUS)
                        self.state = 264
                        self.expr_infix4(0)
                        pass

                    elif la_ == 2:
                        localctx = WdlParser.SubContext(self, WdlParser.Expr_infix3Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix3)
                        self.state = 265
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 266
                        self.match(WdlParser.MINUS)
                        self.state = 267
                        self.expr_infix4(0)
                        pass

             
                self.state = 272
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,14,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr_infix4Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlParser.RULE_expr_infix4

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class ModContext(Expr_infix4Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix4Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix4(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix4Context,0)

        def MOD(self):
            return self.getToken(WdlParser.MOD, 0)
        def expr_infix5(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix5Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMod" ):
                listener.enterMod(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMod" ):
                listener.exitMod(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMod" ):
                return visitor.visitMod(self)
            else:
                return visitor.visitChildren(self)


    class MulContext(Expr_infix4Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix4Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix4(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix4Context,0)

        def STAR(self):
            return self.getToken(WdlParser.STAR, 0)
        def expr_infix5(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix5Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMul" ):
                listener.enterMul(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMul" ):
                listener.exitMul(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMul" ):
                return visitor.visitMul(self)
            else:
                return visitor.visitChildren(self)


    class DivideContext(Expr_infix4Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix4Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix4(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix4Context,0)

        def DIVIDE(self):
            return self.getToken(WdlParser.DIVIDE, 0)
        def expr_infix5(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix5Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDivide" ):
                listener.enterDivide(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDivide" ):
                listener.exitDivide(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDivide" ):
                return visitor.visitDivide(self)
            else:
                return visitor.visitChildren(self)


    class Infix5Context(Expr_infix4Context):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_infix4Context
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_infix5(self):
            return self.getTypedRuleContext(WdlParser.Expr_infix5Context,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfix5" ):
                listener.enterInfix5(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfix5" ):
                listener.exitInfix5(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfix5" ):
                return visitor.visitInfix5(self)
            else:
                return visitor.visitChildren(self)



    def expr_infix4(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlParser.Expr_infix4Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 40
        self.enterRecursionRule(localctx, 40, self.RULE_expr_infix4, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            localctx = WdlParser.Infix5Context(self, localctx)
            self._ctx = localctx
            _prevctx = localctx

            self.state = 274
            self.expr_infix5()
            self._ctx.stop = self._input.LT(-1)
            self.state = 287
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 285
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
                    if la_ == 1:
                        localctx = WdlParser.MulContext(self, WdlParser.Expr_infix4Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix4)
                        self.state = 276
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 277
                        self.match(WdlParser.STAR)
                        self.state = 278
                        self.expr_infix5()
                        pass

                    elif la_ == 2:
                        localctx = WdlParser.DivideContext(self, WdlParser.Expr_infix4Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix4)
                        self.state = 279
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 280
                        self.match(WdlParser.DIVIDE)
                        self.state = 281
                        self.expr_infix5()
                        pass

                    elif la_ == 3:
                        localctx = WdlParser.ModContext(self, WdlParser.Expr_infix4Context(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_infix4)
                        self.state = 282
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 283
                        self.match(WdlParser.MOD)
                        self.state = 284
                        self.expr_infix5()
                        pass

             
                self.state = 289
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr_infix5Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr_core(self):
            return self.getTypedRuleContext(WdlParser.Expr_coreContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_expr_infix5

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr_infix5" ):
                listener.enterExpr_infix5(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr_infix5" ):
                listener.exitExpr_infix5(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr_infix5" ):
                return visitor.visitExpr_infix5(self)
            else:
                return visitor.visitChildren(self)




    def expr_infix5(self):

        localctx = WdlParser.Expr_infix5Context(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_expr_infix5)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 290
            self.expr_core(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr_coreContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlParser.RULE_expr_core

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class Pair_literalContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LPAREN(self):
            return self.getToken(WdlParser.LPAREN, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlParser.ExprContext,i)

        def COMMA(self):
            return self.getToken(WdlParser.COMMA, 0)
        def RPAREN(self):
            return self.getToken(WdlParser.RPAREN, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPair_literal" ):
                listener.enterPair_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPair_literal" ):
                listener.exitPair_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPair_literal" ):
                return visitor.visitPair_literal(self)
            else:
                return visitor.visitChildren(self)


    class ApplyContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)
        def LPAREN(self):
            return self.getToken(WdlParser.LPAREN, 0)
        def RPAREN(self):
            return self.getToken(WdlParser.RPAREN, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlParser.ExprContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.COMMA)
            else:
                return self.getToken(WdlParser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterApply" ):
                listener.enterApply(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitApply" ):
                listener.exitApply(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitApply" ):
                return visitor.visitApply(self)
            else:
                return visitor.visitChildren(self)


    class Expression_groupContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LPAREN(self):
            return self.getToken(WdlParser.LPAREN, 0)
        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)

        def RPAREN(self):
            return self.getToken(WdlParser.RPAREN, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression_group" ):
                listener.enterExpression_group(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression_group" ):
                listener.exitExpression_group(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression_group" ):
                return visitor.visitExpression_group(self)
            else:
                return visitor.visitChildren(self)


    class PrimitivesContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def primitive_literal(self):
            return self.getTypedRuleContext(WdlParser.Primitive_literalContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimitives" ):
                listener.enterPrimitives(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimitives" ):
                listener.exitPrimitives(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrimitives" ):
                return visitor.visitPrimitives(self)
            else:
                return visitor.visitChildren(self)


    class Left_nameContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLeft_name" ):
                listener.enterLeft_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLeft_name" ):
                listener.exitLeft_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLeft_name" ):
                return visitor.visitLeft_name(self)
            else:
                return visitor.visitChildren(self)


    class AtContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_core(self):
            return self.getTypedRuleContext(WdlParser.Expr_coreContext,0)

        def LBRACK(self):
            return self.getToken(WdlParser.LBRACK, 0)
        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)

        def RBRACK(self):
            return self.getToken(WdlParser.RBRACK, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAt" ):
                listener.enterAt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAt" ):
                listener.exitAt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAt" ):
                return visitor.visitAt(self)
            else:
                return visitor.visitChildren(self)


    class NegateContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NOT(self):
            return self.getToken(WdlParser.NOT, 0)
        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNegate" ):
                listener.enterNegate(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNegate" ):
                listener.exitNegate(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNegate" ):
                return visitor.visitNegate(self)
            else:
                return visitor.visitChildren(self)


    class UnirarysignedContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)

        def PLUS(self):
            return self.getToken(WdlParser.PLUS, 0)
        def MINUS(self):
            return self.getToken(WdlParser.MINUS, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnirarysigned" ):
                listener.enterUnirarysigned(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnirarysigned" ):
                listener.exitUnirarysigned(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnirarysigned" ):
                return visitor.visitUnirarysigned(self)
            else:
                return visitor.visitChildren(self)


    class Map_literalContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)
        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlParser.ExprContext,i)

        def COLON(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.COLON)
            else:
                return self.getToken(WdlParser.COLON, i)
        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.COMMA)
            else:
                return self.getToken(WdlParser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMap_literal" ):
                listener.enterMap_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMap_literal" ):
                listener.exitMap_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMap_literal" ):
                return visitor.visitMap_literal(self)
            else:
                return visitor.visitChildren(self)


    class IfthenelseContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IF(self):
            return self.getToken(WdlParser.IF, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlParser.ExprContext,i)

        def THEN(self):
            return self.getToken(WdlParser.THEN, 0)
        def ELSE(self):
            return self.getToken(WdlParser.ELSE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIfthenelse" ):
                listener.enterIfthenelse(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIfthenelse" ):
                listener.exitIfthenelse(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfthenelse" ):
                return visitor.visitIfthenelse(self)
            else:
                return visitor.visitChildren(self)


    class Get_nameContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr_core(self):
            return self.getTypedRuleContext(WdlParser.Expr_coreContext,0)

        def DOT(self):
            return self.getToken(WdlParser.DOT, 0)
        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGet_name" ):
                listener.enterGet_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGet_name" ):
                listener.exitGet_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGet_name" ):
                return visitor.visitGet_name(self)
            else:
                return visitor.visitChildren(self)


    class Array_literalContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LBRACK(self):
            return self.getToken(WdlParser.LBRACK, 0)
        def RBRACK(self):
            return self.getToken(WdlParser.RBRACK, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlParser.ExprContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.COMMA)
            else:
                return self.getToken(WdlParser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArray_literal" ):
                listener.enterArray_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArray_literal" ):
                listener.exitArray_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArray_literal" ):
                return visitor.visitArray_literal(self)
            else:
                return visitor.visitChildren(self)


    class Struct_literalContext(Expr_coreContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Expr_coreContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def Identifier(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.Identifier)
            else:
                return self.getToken(WdlParser.Identifier, i)
        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)
        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)
        def COLON(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.COLON)
            else:
                return self.getToken(WdlParser.COLON, i)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.ExprContext)
            else:
                return self.getTypedRuleContext(WdlParser.ExprContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.COMMA)
            else:
                return self.getToken(WdlParser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStruct_literal" ):
                listener.enterStruct_literal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStruct_literal" ):
                listener.exitStruct_literal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStruct_literal" ):
                return visitor.visitStruct_literal(self)
            else:
                return visitor.visitChildren(self)



    def expr_core(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WdlParser.Expr_coreContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 44
        self.enterRecursionRule(localctx, 44, self.RULE_expr_core, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 384
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,25,self._ctx)
            if la_ == 1:
                localctx = WdlParser.ApplyContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 293
                self.match(WdlParser.Identifier)
                self.state = 294
                self.match(WdlParser.LPAREN)
                self.state = 303
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WdlParser.IF) | (1 << WdlParser.NONELITERAL) | (1 << WdlParser.IntLiteral) | (1 << WdlParser.FloatLiteral) | (1 << WdlParser.BoolLiteral) | (1 << WdlParser.LPAREN) | (1 << WdlParser.LBRACE) | (1 << WdlParser.LBRACK) | (1 << WdlParser.PLUS) | (1 << WdlParser.MINUS))) != 0) or ((((_la - 68)) & ~0x3f) == 0 and ((1 << (_la - 68)) & ((1 << (WdlParser.NOT - 68)) | (1 << (WdlParser.SQUOTE - 68)) | (1 << (WdlParser.DQUOTE - 68)) | (1 << (WdlParser.Identifier - 68)))) != 0):
                    self.state = 295
                    self.expr()
                    self.state = 300
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==WdlParser.COMMA:
                        self.state = 296
                        self.match(WdlParser.COMMA)
                        self.state = 297
                        self.expr()
                        self.state = 302
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)



                self.state = 305
                self.match(WdlParser.RPAREN)
                pass

            elif la_ == 2:
                localctx = WdlParser.Array_literalContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 306
                self.match(WdlParser.LBRACK)
                self.state = 317
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WdlParser.IF) | (1 << WdlParser.NONELITERAL) | (1 << WdlParser.IntLiteral) | (1 << WdlParser.FloatLiteral) | (1 << WdlParser.BoolLiteral) | (1 << WdlParser.LPAREN) | (1 << WdlParser.LBRACE) | (1 << WdlParser.LBRACK) | (1 << WdlParser.PLUS) | (1 << WdlParser.MINUS))) != 0) or ((((_la - 68)) & ~0x3f) == 0 and ((1 << (_la - 68)) & ((1 << (WdlParser.NOT - 68)) | (1 << (WdlParser.SQUOTE - 68)) | (1 << (WdlParser.DQUOTE - 68)) | (1 << (WdlParser.Identifier - 68)))) != 0):
                    self.state = 307
                    self.expr()
                    self.state = 312
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==WdlParser.COMMA:
                        self.state = 308
                        self.match(WdlParser.COMMA)
                        self.state = 309
                        self.expr()
                        self.state = 314
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 319
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 320
                self.match(WdlParser.RBRACK)
                pass

            elif la_ == 3:
                localctx = WdlParser.Pair_literalContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 321
                self.match(WdlParser.LPAREN)
                self.state = 322
                self.expr()
                self.state = 323
                self.match(WdlParser.COMMA)
                self.state = 324
                self.expr()
                self.state = 325
                self.match(WdlParser.RPAREN)
                pass

            elif la_ == 4:
                localctx = WdlParser.Map_literalContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 327
                self.match(WdlParser.LBRACE)
                self.state = 343
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WdlParser.IF) | (1 << WdlParser.NONELITERAL) | (1 << WdlParser.IntLiteral) | (1 << WdlParser.FloatLiteral) | (1 << WdlParser.BoolLiteral) | (1 << WdlParser.LPAREN) | (1 << WdlParser.LBRACE) | (1 << WdlParser.LBRACK) | (1 << WdlParser.PLUS) | (1 << WdlParser.MINUS))) != 0) or ((((_la - 68)) & ~0x3f) == 0 and ((1 << (_la - 68)) & ((1 << (WdlParser.NOT - 68)) | (1 << (WdlParser.SQUOTE - 68)) | (1 << (WdlParser.DQUOTE - 68)) | (1 << (WdlParser.Identifier - 68)))) != 0):
                    self.state = 328
                    self.expr()
                    self.state = 329
                    self.match(WdlParser.COLON)
                    self.state = 330
                    self.expr()
                    self.state = 338
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==WdlParser.COMMA:
                        self.state = 331
                        self.match(WdlParser.COMMA)
                        self.state = 332
                        self.expr()
                        self.state = 333
                        self.match(WdlParser.COLON)
                        self.state = 334
                        self.expr()
                        self.state = 340
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 345
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 346
                self.match(WdlParser.RBRACE)
                pass

            elif la_ == 5:
                localctx = WdlParser.Struct_literalContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 347
                self.match(WdlParser.Identifier)
                self.state = 348
                self.match(WdlParser.LBRACE)
                self.state = 363
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlParser.Identifier:
                    self.state = 349
                    self.match(WdlParser.Identifier)
                    self.state = 350
                    self.match(WdlParser.COLON)
                    self.state = 351
                    self.expr()
                    self.state = 358
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==WdlParser.COMMA:
                        self.state = 352
                        self.match(WdlParser.COMMA)
                        self.state = 353
                        self.match(WdlParser.Identifier)
                        self.state = 354
                        self.match(WdlParser.COLON)
                        self.state = 355
                        self.expr()
                        self.state = 360
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 365
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 366
                self.match(WdlParser.RBRACE)
                pass

            elif la_ == 6:
                localctx = WdlParser.IfthenelseContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 367
                self.match(WdlParser.IF)
                self.state = 368
                self.expr()
                self.state = 369
                self.match(WdlParser.THEN)
                self.state = 370
                self.expr()
                self.state = 371
                self.match(WdlParser.ELSE)
                self.state = 372
                self.expr()
                pass

            elif la_ == 7:
                localctx = WdlParser.Expression_groupContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 374
                self.match(WdlParser.LPAREN)
                self.state = 375
                self.expr()
                self.state = 376
                self.match(WdlParser.RPAREN)
                pass

            elif la_ == 8:
                localctx = WdlParser.NegateContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 378
                self.match(WdlParser.NOT)
                self.state = 379
                self.expr()
                pass

            elif la_ == 9:
                localctx = WdlParser.UnirarysignedContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 380
                _la = self._input.LA(1)
                if not(_la==WdlParser.PLUS or _la==WdlParser.MINUS):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 381
                self.expr()
                pass

            elif la_ == 10:
                localctx = WdlParser.PrimitivesContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 382
                self.primitive_literal()
                pass

            elif la_ == 11:
                localctx = WdlParser.Left_nameContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 383
                self.match(WdlParser.Identifier)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 396
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,27,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 394
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,26,self._ctx)
                    if la_ == 1:
                        localctx = WdlParser.AtContext(self, WdlParser.Expr_coreContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_core)
                        self.state = 386
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 387
                        self.match(WdlParser.LBRACK)
                        self.state = 388
                        self.expr()
                        self.state = 389
                        self.match(WdlParser.RBRACK)
                        pass

                    elif la_ == 2:
                        localctx = WdlParser.Get_nameContext(self, WdlParser.Expr_coreContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr_core)
                        self.state = 391
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 392
                        self.match(WdlParser.DOT)
                        self.state = 393
                        self.match(WdlParser.Identifier)
                        pass

             
                self.state = 398
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,27,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class VersionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VERSION(self):
            return self.getToken(WdlParser.VERSION, 0)

        def RELEASE_VERSION(self):
            return self.getToken(WdlParser.RELEASE_VERSION, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_version

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVersion" ):
                listener.enterVersion(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVersion" ):
                listener.exitVersion(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVersion" ):
                return visitor.visitVersion(self)
            else:
                return visitor.visitChildren(self)




    def version(self):

        localctx = WdlParser.VersionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_version)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 399
            self.match(WdlParser.VERSION)
            self.state = 400
            self.match(WdlParser.RELEASE_VERSION)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_aliasContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ALIAS(self):
            return self.getToken(WdlParser.ALIAS, 0)

        def Identifier(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.Identifier)
            else:
                return self.getToken(WdlParser.Identifier, i)

        def AS(self):
            return self.getToken(WdlParser.AS, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_import_alias

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_alias" ):
                listener.enterImport_alias(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_alias" ):
                listener.exitImport_alias(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImport_alias" ):
                return visitor.visitImport_alias(self)
            else:
                return visitor.visitChildren(self)




    def import_alias(self):

        localctx = WdlParser.Import_aliasContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_import_alias)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 402
            self.match(WdlParser.ALIAS)
            self.state = 403
            self.match(WdlParser.Identifier)
            self.state = 404
            self.match(WdlParser.AS)
            self.state = 405
            self.match(WdlParser.Identifier)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_asContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def AS(self):
            return self.getToken(WdlParser.AS, 0)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_import_as

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_as" ):
                listener.enterImport_as(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_as" ):
                listener.exitImport_as(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImport_as" ):
                return visitor.visitImport_as(self)
            else:
                return visitor.visitChildren(self)




    def import_as(self):

        localctx = WdlParser.Import_asContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_import_as)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 407
            self.match(WdlParser.AS)
            self.state = 408
            self.match(WdlParser.Identifier)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_docContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IMPORT(self):
            return self.getToken(WdlParser.IMPORT, 0)

        def string(self):
            return self.getTypedRuleContext(WdlParser.StringContext,0)


        def import_as(self):
            return self.getTypedRuleContext(WdlParser.Import_asContext,0)


        def import_alias(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Import_aliasContext)
            else:
                return self.getTypedRuleContext(WdlParser.Import_aliasContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_import_doc

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_doc" ):
                listener.enterImport_doc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_doc" ):
                listener.exitImport_doc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImport_doc" ):
                return visitor.visitImport_doc(self)
            else:
                return visitor.visitChildren(self)




    def import_doc(self):

        localctx = WdlParser.Import_docContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_import_doc)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 410
            self.match(WdlParser.IMPORT)
            self.state = 411
            self.string()
            self.state = 413
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlParser.AS:
                self.state = 412
                self.import_as()


            self.state = 418
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlParser.ALIAS:
                self.state = 415
                self.import_alias()
                self.state = 420
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StructContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRUCT(self):
            return self.getToken(WdlParser.STRUCT, 0)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def unbound_decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Unbound_declsContext)
            else:
                return self.getTypedRuleContext(WdlParser.Unbound_declsContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_struct

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStruct" ):
                listener.enterStruct(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStruct" ):
                listener.exitStruct(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStruct" ):
                return visitor.visitStruct(self)
            else:
                return visitor.visitChildren(self)




    def struct(self):

        localctx = WdlParser.StructContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_struct)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 421
            self.match(WdlParser.STRUCT)
            self.state = 422
            self.match(WdlParser.Identifier)
            self.state = 423
            self.match(WdlParser.LBRACE)
            self.state = 427
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 27)) & ~0x3f) == 0 and ((1 << (_la - 27)) & ((1 << (WdlParser.BOOLEAN - 27)) | (1 << (WdlParser.INT - 27)) | (1 << (WdlParser.FLOAT - 27)) | (1 << (WdlParser.STRING - 27)) | (1 << (WdlParser.FILE - 27)) | (1 << (WdlParser.DIRECTORY - 27)) | (1 << (WdlParser.ARRAY - 27)) | (1 << (WdlParser.MAP - 27)) | (1 << (WdlParser.PAIR - 27)) | (1 << (WdlParser.Identifier - 27)))) != 0):
                self.state = 424
                self.unbound_decls()
                self.state = 429
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 430
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meta_kvContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def COLON(self):
            return self.getToken(WdlParser.COLON, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_meta_kv

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta_kv" ):
                listener.enterMeta_kv(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta_kv" ):
                listener.exitMeta_kv(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta_kv" ):
                return visitor.visitMeta_kv(self)
            else:
                return visitor.visitChildren(self)




    def meta_kv(self):

        localctx = WdlParser.Meta_kvContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_meta_kv)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 432
            self.match(WdlParser.Identifier)
            self.state = 433
            self.match(WdlParser.COLON)
            self.state = 434
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Parameter_metaContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PARAMETERMETA(self):
            return self.getToken(WdlParser.PARAMETERMETA, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def meta_kv(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Meta_kvContext)
            else:
                return self.getTypedRuleContext(WdlParser.Meta_kvContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_parameter_meta

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameter_meta" ):
                listener.enterParameter_meta(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameter_meta" ):
                listener.exitParameter_meta(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameter_meta" ):
                return visitor.visitParameter_meta(self)
            else:
                return visitor.visitChildren(self)




    def parameter_meta(self):

        localctx = WdlParser.Parameter_metaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_parameter_meta)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 436
            self.match(WdlParser.PARAMETERMETA)
            self.state = 437
            self.match(WdlParser.LBRACE)
            self.state = 441
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlParser.Identifier:
                self.state = 438
                self.meta_kv()
                self.state = 443
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 444
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MetaContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def META(self):
            return self.getToken(WdlParser.META, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def meta_kv(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Meta_kvContext)
            else:
                return self.getTypedRuleContext(WdlParser.Meta_kvContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_meta

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta" ):
                listener.enterMeta(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta" ):
                listener.exitMeta(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta" ):
                return visitor.visitMeta(self)
            else:
                return visitor.visitChildren(self)




    def meta(self):

        localctx = WdlParser.MetaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_meta)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 446
            self.match(WdlParser.META)
            self.state = 447
            self.match(WdlParser.LBRACE)
            self.state = 451
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlParser.Identifier:
                self.state = 448
                self.meta_kv()
                self.state = 453
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 454
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_runtime_kvContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RUNTIMECPU(self):
            return self.getToken(WdlParser.RUNTIMECPU, 0)

        def COLON(self):
            return self.getToken(WdlParser.COLON, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)


        def RUNTIMECONTAINER(self):
            return self.getToken(WdlParser.RUNTIMECONTAINER, 0)

        def RUNTIMEMEMORY(self):
            return self.getToken(WdlParser.RUNTIMEMEMORY, 0)

        def RUNTIMEGPU(self):
            return self.getToken(WdlParser.RUNTIMEGPU, 0)

        def RUNTIMEDISKS(self):
            return self.getToken(WdlParser.RUNTIMEDISKS, 0)

        def RUNTIMEMAXRETRIES(self):
            return self.getToken(WdlParser.RUNTIMEMAXRETRIES, 0)

        def RUNTIMERETURNCODES(self):
            return self.getToken(WdlParser.RUNTIMERETURNCODES, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_task_runtime_kv

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_runtime_kv" ):
                listener.enterTask_runtime_kv(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_runtime_kv" ):
                listener.exitTask_runtime_kv(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_runtime_kv" ):
                return visitor.visitTask_runtime_kv(self)
            else:
                return visitor.visitChildren(self)




    def task_runtime_kv(self):

        localctx = WdlParser.Task_runtime_kvContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_task_runtime_kv)
        try:
            self.state = 477
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlParser.RUNTIMECPU]:
                self.enterOuterAlt(localctx, 1)
                self.state = 456
                self.match(WdlParser.RUNTIMECPU)
                self.state = 457
                self.match(WdlParser.COLON)
                self.state = 458
                self.expr()
                pass
            elif token in [WdlParser.RUNTIMECONTAINER]:
                self.enterOuterAlt(localctx, 2)
                self.state = 459
                self.match(WdlParser.RUNTIMECONTAINER)
                self.state = 460
                self.match(WdlParser.COLON)
                self.state = 461
                self.expr()
                pass
            elif token in [WdlParser.RUNTIMEMEMORY]:
                self.enterOuterAlt(localctx, 3)
                self.state = 462
                self.match(WdlParser.RUNTIMEMEMORY)
                self.state = 463
                self.match(WdlParser.COLON)
                self.state = 464
                self.expr()
                pass
            elif token in [WdlParser.RUNTIMEGPU]:
                self.enterOuterAlt(localctx, 4)
                self.state = 465
                self.match(WdlParser.RUNTIMEGPU)
                self.state = 466
                self.match(WdlParser.COLON)
                self.state = 467
                self.expr()
                pass
            elif token in [WdlParser.RUNTIMEDISKS]:
                self.enterOuterAlt(localctx, 5)
                self.state = 468
                self.match(WdlParser.RUNTIMEDISKS)
                self.state = 469
                self.match(WdlParser.COLON)
                self.state = 470
                self.expr()
                pass
            elif token in [WdlParser.RUNTIMEMAXRETRIES]:
                self.enterOuterAlt(localctx, 6)
                self.state = 471
                self.match(WdlParser.RUNTIMEMAXRETRIES)
                self.state = 472
                self.match(WdlParser.COLON)
                self.state = 473
                self.expr()
                pass
            elif token in [WdlParser.RUNTIMERETURNCODES]:
                self.enterOuterAlt(localctx, 7)
                self.state = 474
                self.match(WdlParser.RUNTIMERETURNCODES)
                self.state = 475
                self.match(WdlParser.COLON)
                self.state = 476
                self.expr()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_runtimeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RUNTIME(self):
            return self.getToken(WdlParser.RUNTIME, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def task_runtime_kv(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Task_runtime_kvContext)
            else:
                return self.getTypedRuleContext(WdlParser.Task_runtime_kvContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_task_runtime

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_runtime" ):
                listener.enterTask_runtime(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_runtime" ):
                listener.exitTask_runtime(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_runtime" ):
                return visitor.visitTask_runtime(self)
            else:
                return visitor.visitChildren(self)




    def task_runtime(self):

        localctx = WdlParser.Task_runtimeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_task_runtime)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 479
            self.match(WdlParser.RUNTIME)
            self.state = 480
            self.match(WdlParser.LBRACE)
            self.state = 484
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WdlParser.RUNTIMECPU) | (1 << WdlParser.RUNTIMECONTAINER) | (1 << WdlParser.RUNTIMEMEMORY) | (1 << WdlParser.RUNTIMEGPU) | (1 << WdlParser.RUNTIMEDISKS) | (1 << WdlParser.RUNTIMEMAXRETRIES) | (1 << WdlParser.RUNTIMERETURNCODES))) != 0):
                self.state = 481
                self.task_runtime_kv()
                self.state = 486
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 487
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_hints_kvContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def COLON(self):
            return self.getToken(WdlParser.COLON, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_task_hints_kv

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_hints_kv" ):
                listener.enterTask_hints_kv(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_hints_kv" ):
                listener.exitTask_hints_kv(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_hints_kv" ):
                return visitor.visitTask_hints_kv(self)
            else:
                return visitor.visitChildren(self)




    def task_hints_kv(self):

        localctx = WdlParser.Task_hints_kvContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_task_hints_kv)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 489
            self.match(WdlParser.Identifier)
            self.state = 490
            self.match(WdlParser.COLON)
            self.state = 491
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_hintsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def HINTS(self):
            return self.getToken(WdlParser.HINTS, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def task_hints_kv(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Task_hints_kvContext)
            else:
                return self.getTypedRuleContext(WdlParser.Task_hints_kvContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_task_hints

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_hints" ):
                listener.enterTask_hints(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_hints" ):
                listener.exitTask_hints(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_hints" ):
                return visitor.visitTask_hints(self)
            else:
                return visitor.visitChildren(self)




    def task_hints(self):

        localctx = WdlParser.Task_hintsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_task_hints)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 493
            self.match(WdlParser.HINTS)
            self.state = 494
            self.match(WdlParser.LBRACE)
            self.state = 498
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlParser.Identifier:
                self.state = 495
                self.task_hints_kv()
                self.state = 500
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 501
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_inputContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INPUT(self):
            return self.getToken(WdlParser.INPUT, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def any_decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Any_declsContext)
            else:
                return self.getTypedRuleContext(WdlParser.Any_declsContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_task_input

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_input" ):
                listener.enterTask_input(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_input" ):
                listener.exitTask_input(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_input" ):
                return visitor.visitTask_input(self)
            else:
                return visitor.visitChildren(self)




    def task_input(self):

        localctx = WdlParser.Task_inputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 70, self.RULE_task_input)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 503
            self.match(WdlParser.INPUT)
            self.state = 504
            self.match(WdlParser.LBRACE)
            self.state = 508
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 27)) & ~0x3f) == 0 and ((1 << (_la - 27)) & ((1 << (WdlParser.BOOLEAN - 27)) | (1 << (WdlParser.INT - 27)) | (1 << (WdlParser.FLOAT - 27)) | (1 << (WdlParser.STRING - 27)) | (1 << (WdlParser.FILE - 27)) | (1 << (WdlParser.DIRECTORY - 27)) | (1 << (WdlParser.ARRAY - 27)) | (1 << (WdlParser.MAP - 27)) | (1 << (WdlParser.PAIR - 27)) | (1 << (WdlParser.Identifier - 27)))) != 0):
                self.state = 505
                self.any_decls()
                self.state = 510
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 511
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_outputContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OUTPUT(self):
            return self.getToken(WdlParser.OUTPUT, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def bound_decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Bound_declsContext)
            else:
                return self.getTypedRuleContext(WdlParser.Bound_declsContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_task_output

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_output" ):
                listener.enterTask_output(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_output" ):
                listener.exitTask_output(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_output" ):
                return visitor.visitTask_output(self)
            else:
                return visitor.visitChildren(self)




    def task_output(self):

        localctx = WdlParser.Task_outputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 72, self.RULE_task_output)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 513
            self.match(WdlParser.OUTPUT)
            self.state = 514
            self.match(WdlParser.LBRACE)
            self.state = 518
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 27)) & ~0x3f) == 0 and ((1 << (_la - 27)) & ((1 << (WdlParser.BOOLEAN - 27)) | (1 << (WdlParser.INT - 27)) | (1 << (WdlParser.FLOAT - 27)) | (1 << (WdlParser.STRING - 27)) | (1 << (WdlParser.FILE - 27)) | (1 << (WdlParser.DIRECTORY - 27)) | (1 << (WdlParser.ARRAY - 27)) | (1 << (WdlParser.MAP - 27)) | (1 << (WdlParser.PAIR - 27)) | (1 << (WdlParser.Identifier - 27)))) != 0):
                self.state = 515
                self.bound_decls()
                self.state = 520
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 521
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_commandContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def COMMAND(self):
            return self.getToken(WdlParser.COMMAND, 0)

        def task_command_string_part(self):
            return self.getTypedRuleContext(WdlParser.Task_command_string_partContext,0)


        def EndCommand(self):
            return self.getToken(WdlParser.EndCommand, 0)

        def task_command_expr_with_string(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Task_command_expr_with_stringContext)
            else:
                return self.getTypedRuleContext(WdlParser.Task_command_expr_with_stringContext,i)


        def HEREDOC_COMMAND(self):
            return self.getToken(WdlParser.HEREDOC_COMMAND, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_task_command

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_command" ):
                listener.enterTask_command(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_command" ):
                listener.exitTask_command(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_command" ):
                return visitor.visitTask_command(self)
            else:
                return visitor.visitChildren(self)




    def task_command(self):

        localctx = WdlParser.Task_commandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_task_command)
        self._la = 0 # Token type
        try:
            self.state = 543
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlParser.COMMAND]:
                self.enterOuterAlt(localctx, 1)
                self.state = 523
                self.match(WdlParser.COMMAND)
                self.state = 524
                self.task_command_string_part()
                self.state = 528
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlParser.StringCommandStart:
                    self.state = 525
                    self.task_command_expr_with_string()
                    self.state = 530
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 531
                self.match(WdlParser.EndCommand)
                pass
            elif token in [WdlParser.HEREDOC_COMMAND]:
                self.enterOuterAlt(localctx, 2)
                self.state = 533
                self.match(WdlParser.HEREDOC_COMMAND)
                self.state = 534
                self.task_command_string_part()
                self.state = 538
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==WdlParser.StringCommandStart:
                    self.state = 535
                    self.task_command_expr_with_string()
                    self.state = 540
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 541
                self.match(WdlParser.EndCommand)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_command_string_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CommandStringPart(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.CommandStringPart)
            else:
                return self.getToken(WdlParser.CommandStringPart, i)

        def getRuleIndex(self):
            return WdlParser.RULE_task_command_string_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_command_string_part" ):
                listener.enterTask_command_string_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_command_string_part" ):
                listener.exitTask_command_string_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_command_string_part" ):
                return visitor.visitTask_command_string_part(self)
            else:
                return visitor.visitChildren(self)




    def task_command_string_part(self):

        localctx = WdlParser.Task_command_string_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_task_command_string_part)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 548
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlParser.CommandStringPart:
                self.state = 545
                self.match(WdlParser.CommandStringPart)
                self.state = 550
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_command_expr_partContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def StringCommandStart(self):
            return self.getToken(WdlParser.StringCommandStart, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)


        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_task_command_expr_part

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_command_expr_part" ):
                listener.enterTask_command_expr_part(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_command_expr_part" ):
                listener.exitTask_command_expr_part(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_command_expr_part" ):
                return visitor.visitTask_command_expr_part(self)
            else:
                return visitor.visitChildren(self)




    def task_command_expr_part(self):

        localctx = WdlParser.Task_command_expr_partContext(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_task_command_expr_part)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 551
            self.match(WdlParser.StringCommandStart)
            self.state = 552
            self.expr()
            self.state = 553
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_command_expr_with_stringContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def task_command_expr_part(self):
            return self.getTypedRuleContext(WdlParser.Task_command_expr_partContext,0)


        def task_command_string_part(self):
            return self.getTypedRuleContext(WdlParser.Task_command_string_partContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_task_command_expr_with_string

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_command_expr_with_string" ):
                listener.enterTask_command_expr_with_string(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_command_expr_with_string" ):
                listener.exitTask_command_expr_with_string(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_command_expr_with_string" ):
                return visitor.visitTask_command_expr_with_string(self)
            else:
                return visitor.visitChildren(self)




    def task_command_expr_with_string(self):

        localctx = WdlParser.Task_command_expr_with_stringContext(self, self._ctx, self.state)
        self.enterRule(localctx, 80, self.RULE_task_command_expr_with_string)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 555
            self.task_command_expr_part()
            self.state = 556
            self.task_command_string_part()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_elementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def task_input(self):
            return self.getTypedRuleContext(WdlParser.Task_inputContext,0)


        def task_output(self):
            return self.getTypedRuleContext(WdlParser.Task_outputContext,0)


        def task_command(self):
            return self.getTypedRuleContext(WdlParser.Task_commandContext,0)


        def task_runtime(self):
            return self.getTypedRuleContext(WdlParser.Task_runtimeContext,0)


        def task_hints(self):
            return self.getTypedRuleContext(WdlParser.Task_hintsContext,0)


        def bound_decls(self):
            return self.getTypedRuleContext(WdlParser.Bound_declsContext,0)


        def parameter_meta(self):
            return self.getTypedRuleContext(WdlParser.Parameter_metaContext,0)


        def meta(self):
            return self.getTypedRuleContext(WdlParser.MetaContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_task_element

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_element" ):
                listener.enterTask_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_element" ):
                listener.exitTask_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_element" ):
                return visitor.visitTask_element(self)
            else:
                return visitor.visitChildren(self)




    def task_element(self):

        localctx = WdlParser.Task_elementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 82, self.RULE_task_element)
        try:
            self.state = 566
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlParser.INPUT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 558
                self.task_input()
                pass
            elif token in [WdlParser.OUTPUT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 559
                self.task_output()
                pass
            elif token in [WdlParser.HEREDOC_COMMAND, WdlParser.COMMAND]:
                self.enterOuterAlt(localctx, 3)
                self.state = 560
                self.task_command()
                pass
            elif token in [WdlParser.RUNTIME]:
                self.enterOuterAlt(localctx, 4)
                self.state = 561
                self.task_runtime()
                pass
            elif token in [WdlParser.HINTS]:
                self.enterOuterAlt(localctx, 5)
                self.state = 562
                self.task_hints()
                pass
            elif token in [WdlParser.BOOLEAN, WdlParser.INT, WdlParser.FLOAT, WdlParser.STRING, WdlParser.FILE, WdlParser.DIRECTORY, WdlParser.ARRAY, WdlParser.MAP, WdlParser.PAIR, WdlParser.Identifier]:
                self.enterOuterAlt(localctx, 6)
                self.state = 563
                self.bound_decls()
                pass
            elif token in [WdlParser.PARAMETERMETA]:
                self.enterOuterAlt(localctx, 7)
                self.state = 564
                self.parameter_meta()
                pass
            elif token in [WdlParser.META]:
                self.enterOuterAlt(localctx, 8)
                self.state = 565
                self.meta()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TaskContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TASK(self):
            return self.getToken(WdlParser.TASK, 0)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def task_element(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Task_elementContext)
            else:
                return self.getTypedRuleContext(WdlParser.Task_elementContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_task

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask" ):
                listener.enterTask(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask" ):
                listener.exitTask(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask" ):
                return visitor.visitTask(self)
            else:
                return visitor.visitChildren(self)




    def task(self):

        localctx = WdlParser.TaskContext(self, self._ctx, self.state)
        self.enterRule(localctx, 84, self.RULE_task)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 568
            self.match(WdlParser.TASK)
            self.state = 569
            self.match(WdlParser.Identifier)
            self.state = 570
            self.match(WdlParser.LBRACE)
            self.state = 572 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 571
                self.task_element()
                self.state = 574 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (((((_la - 14)) & ~0x3f) == 0 and ((1 << (_la - 14)) & ((1 << (WdlParser.INPUT - 14)) | (1 << (WdlParser.OUTPUT - 14)) | (1 << (WdlParser.PARAMETERMETA - 14)) | (1 << (WdlParser.META - 14)) | (1 << (WdlParser.HINTS - 14)) | (1 << (WdlParser.RUNTIME - 14)) | (1 << (WdlParser.BOOLEAN - 14)) | (1 << (WdlParser.INT - 14)) | (1 << (WdlParser.FLOAT - 14)) | (1 << (WdlParser.STRING - 14)) | (1 << (WdlParser.FILE - 14)) | (1 << (WdlParser.DIRECTORY - 14)) | (1 << (WdlParser.ARRAY - 14)) | (1 << (WdlParser.MAP - 14)) | (1 << (WdlParser.PAIR - 14)) | (1 << (WdlParser.HEREDOC_COMMAND - 14)) | (1 << (WdlParser.COMMAND - 14)) | (1 << (WdlParser.Identifier - 14)))) != 0)):
                    break

            self.state = 576
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Inner_workflow_elementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def bound_decls(self):
            return self.getTypedRuleContext(WdlParser.Bound_declsContext,0)


        def call(self):
            return self.getTypedRuleContext(WdlParser.CallContext,0)


        def scatter(self):
            return self.getTypedRuleContext(WdlParser.ScatterContext,0)


        def conditional(self):
            return self.getTypedRuleContext(WdlParser.ConditionalContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_inner_workflow_element

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInner_workflow_element" ):
                listener.enterInner_workflow_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInner_workflow_element" ):
                listener.exitInner_workflow_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInner_workflow_element" ):
                return visitor.visitInner_workflow_element(self)
            else:
                return visitor.visitChildren(self)




    def inner_workflow_element(self):

        localctx = WdlParser.Inner_workflow_elementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 86, self.RULE_inner_workflow_element)
        try:
            self.state = 582
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlParser.BOOLEAN, WdlParser.INT, WdlParser.FLOAT, WdlParser.STRING, WdlParser.FILE, WdlParser.DIRECTORY, WdlParser.ARRAY, WdlParser.MAP, WdlParser.PAIR, WdlParser.Identifier]:
                self.enterOuterAlt(localctx, 1)
                self.state = 578
                self.bound_decls()
                pass
            elif token in [WdlParser.CALL]:
                self.enterOuterAlt(localctx, 2)
                self.state = 579
                self.call()
                pass
            elif token in [WdlParser.SCATTER]:
                self.enterOuterAlt(localctx, 3)
                self.state = 580
                self.scatter()
                pass
            elif token in [WdlParser.IF]:
                self.enterOuterAlt(localctx, 4)
                self.state = 581
                self.conditional()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_aliasContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def AS(self):
            return self.getToken(WdlParser.AS, 0)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_call_alias

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_alias" ):
                listener.enterCall_alias(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_alias" ):
                listener.exitCall_alias(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_alias" ):
                return visitor.visitCall_alias(self)
            else:
                return visitor.visitChildren(self)




    def call_alias(self):

        localctx = WdlParser.Call_aliasContext(self, self._ctx, self.state)
        self.enterRule(localctx, 88, self.RULE_call_alias)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 584
            self.match(WdlParser.AS)
            self.state = 585
            self.match(WdlParser.Identifier)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_inputContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def EQUAL(self):
            return self.getToken(WdlParser.EQUAL, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_call_input

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_input" ):
                listener.enterCall_input(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_input" ):
                listener.exitCall_input(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_input" ):
                return visitor.visitCall_input(self)
            else:
                return visitor.visitChildren(self)




    def call_input(self):

        localctx = WdlParser.Call_inputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 90, self.RULE_call_input)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 587
            self.match(WdlParser.Identifier)
            self.state = 588
            self.match(WdlParser.EQUAL)
            self.state = 589
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_inputsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INPUT(self):
            return self.getToken(WdlParser.INPUT, 0)

        def COLON(self):
            return self.getToken(WdlParser.COLON, 0)

        def call_input(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Call_inputContext)
            else:
                return self.getTypedRuleContext(WdlParser.Call_inputContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.COMMA)
            else:
                return self.getToken(WdlParser.COMMA, i)

        def getRuleIndex(self):
            return WdlParser.RULE_call_inputs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_inputs" ):
                listener.enterCall_inputs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_inputs" ):
                listener.exitCall_inputs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_inputs" ):
                return visitor.visitCall_inputs(self)
            else:
                return visitor.visitChildren(self)




    def call_inputs(self):

        localctx = WdlParser.Call_inputsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 92, self.RULE_call_inputs)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 591
            self.match(WdlParser.INPUT)
            self.state = 592
            self.match(WdlParser.COLON)

            self.state = 593
            self.call_input()
            self.state = 598
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlParser.COMMA:
                self.state = 594
                self.match(WdlParser.COMMA)
                self.state = 595
                self.call_input()
                self.state = 600
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_bodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def call_inputs(self):
            return self.getTypedRuleContext(WdlParser.Call_inputsContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_call_body

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_body" ):
                listener.enterCall_body(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_body" ):
                listener.exitCall_body(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_body" ):
                return visitor.visitCall_body(self)
            else:
                return visitor.visitChildren(self)




    def call_body(self):

        localctx = WdlParser.Call_bodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 94, self.RULE_call_body)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 601
            self.match(WdlParser.LBRACE)
            self.state = 603
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlParser.INPUT:
                self.state = 602
                self.call_inputs()


            self.state = 605
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_aftersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def AFTER(self):
            return self.getToken(WdlParser.AFTER, 0)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def getRuleIndex(self):
            return WdlParser.RULE_call_afters

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_afters" ):
                listener.enterCall_afters(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_afters" ):
                listener.exitCall_afters(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_afters" ):
                return visitor.visitCall_afters(self)
            else:
                return visitor.visitChildren(self)




    def call_afters(self):

        localctx = WdlParser.Call_aftersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 96, self.RULE_call_afters)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 607
            self.match(WdlParser.AFTER)
            self.state = 608
            self.match(WdlParser.Identifier)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.Identifier)
            else:
                return self.getToken(WdlParser.Identifier, i)

        def DOT(self, i:int=None):
            if i is None:
                return self.getTokens(WdlParser.DOT)
            else:
                return self.getToken(WdlParser.DOT, i)

        def getRuleIndex(self):
            return WdlParser.RULE_call_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_name" ):
                listener.enterCall_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_name" ):
                listener.exitCall_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_name" ):
                return visitor.visitCall_name(self)
            else:
                return visitor.visitChildren(self)




    def call_name(self):

        localctx = WdlParser.Call_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 98, self.RULE_call_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 610
            self.match(WdlParser.Identifier)
            self.state = 615
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlParser.DOT:
                self.state = 611
                self.match(WdlParser.DOT)
                self.state = 612
                self.match(WdlParser.Identifier)
                self.state = 617
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CALL(self):
            return self.getToken(WdlParser.CALL, 0)

        def call_name(self):
            return self.getTypedRuleContext(WdlParser.Call_nameContext,0)


        def call_alias(self):
            return self.getTypedRuleContext(WdlParser.Call_aliasContext,0)


        def call_afters(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Call_aftersContext)
            else:
                return self.getTypedRuleContext(WdlParser.Call_aftersContext,i)


        def call_body(self):
            return self.getTypedRuleContext(WdlParser.Call_bodyContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_call

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall" ):
                listener.enterCall(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall" ):
                listener.exitCall(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall" ):
                return visitor.visitCall(self)
            else:
                return visitor.visitChildren(self)




    def call(self):

        localctx = WdlParser.CallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 100, self.RULE_call)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 618
            self.match(WdlParser.CALL)
            self.state = 619
            self.call_name()
            self.state = 621
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlParser.AS:
                self.state = 620
                self.call_alias()


            self.state = 626
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WdlParser.AFTER:
                self.state = 623
                self.call_afters()
                self.state = 628
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 630
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlParser.LBRACE:
                self.state = 629
                self.call_body()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ScatterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SCATTER(self):
            return self.getToken(WdlParser.SCATTER, 0)

        def LPAREN(self):
            return self.getToken(WdlParser.LPAREN, 0)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def In(self):
            return self.getToken(WdlParser.In, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)


        def RPAREN(self):
            return self.getToken(WdlParser.RPAREN, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def inner_workflow_element(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Inner_workflow_elementContext)
            else:
                return self.getTypedRuleContext(WdlParser.Inner_workflow_elementContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_scatter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterScatter" ):
                listener.enterScatter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitScatter" ):
                listener.exitScatter(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitScatter" ):
                return visitor.visitScatter(self)
            else:
                return visitor.visitChildren(self)




    def scatter(self):

        localctx = WdlParser.ScatterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 102, self.RULE_scatter)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 632
            self.match(WdlParser.SCATTER)
            self.state = 633
            self.match(WdlParser.LPAREN)
            self.state = 634
            self.match(WdlParser.Identifier)
            self.state = 635
            self.match(WdlParser.In)
            self.state = 636
            self.expr()
            self.state = 637
            self.match(WdlParser.RPAREN)
            self.state = 638
            self.match(WdlParser.LBRACE)
            self.state = 642
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WdlParser.SCATTER) | (1 << WdlParser.CALL) | (1 << WdlParser.IF) | (1 << WdlParser.BOOLEAN) | (1 << WdlParser.INT) | (1 << WdlParser.FLOAT) | (1 << WdlParser.STRING) | (1 << WdlParser.FILE) | (1 << WdlParser.DIRECTORY) | (1 << WdlParser.ARRAY) | (1 << WdlParser.MAP) | (1 << WdlParser.PAIR))) != 0) or _la==WdlParser.Identifier:
                self.state = 639
                self.inner_workflow_element()
                self.state = 644
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 645
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConditionalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(WdlParser.IF, 0)

        def LPAREN(self):
            return self.getToken(WdlParser.LPAREN, 0)

        def expr(self):
            return self.getTypedRuleContext(WdlParser.ExprContext,0)


        def RPAREN(self):
            return self.getToken(WdlParser.RPAREN, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def inner_workflow_element(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Inner_workflow_elementContext)
            else:
                return self.getTypedRuleContext(WdlParser.Inner_workflow_elementContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_conditional

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConditional" ):
                listener.enterConditional(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConditional" ):
                listener.exitConditional(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConditional" ):
                return visitor.visitConditional(self)
            else:
                return visitor.visitChildren(self)




    def conditional(self):

        localctx = WdlParser.ConditionalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 104, self.RULE_conditional)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 647
            self.match(WdlParser.IF)
            self.state = 648
            self.match(WdlParser.LPAREN)
            self.state = 649
            self.expr()
            self.state = 650
            self.match(WdlParser.RPAREN)
            self.state = 651
            self.match(WdlParser.LBRACE)
            self.state = 655
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WdlParser.SCATTER) | (1 << WdlParser.CALL) | (1 << WdlParser.IF) | (1 << WdlParser.BOOLEAN) | (1 << WdlParser.INT) | (1 << WdlParser.FLOAT) | (1 << WdlParser.STRING) | (1 << WdlParser.FILE) | (1 << WdlParser.DIRECTORY) | (1 << WdlParser.ARRAY) | (1 << WdlParser.MAP) | (1 << WdlParser.PAIR))) != 0) or _la==WdlParser.Identifier:
                self.state = 652
                self.inner_workflow_element()
                self.state = 657
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 658
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Workflow_inputContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INPUT(self):
            return self.getToken(WdlParser.INPUT, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def any_decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Any_declsContext)
            else:
                return self.getTypedRuleContext(WdlParser.Any_declsContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_workflow_input

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWorkflow_input" ):
                listener.enterWorkflow_input(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWorkflow_input" ):
                listener.exitWorkflow_input(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWorkflow_input" ):
                return visitor.visitWorkflow_input(self)
            else:
                return visitor.visitChildren(self)




    def workflow_input(self):

        localctx = WdlParser.Workflow_inputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 106, self.RULE_workflow_input)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 660
            self.match(WdlParser.INPUT)
            self.state = 661
            self.match(WdlParser.LBRACE)
            self.state = 665
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 27)) & ~0x3f) == 0 and ((1 << (_la - 27)) & ((1 << (WdlParser.BOOLEAN - 27)) | (1 << (WdlParser.INT - 27)) | (1 << (WdlParser.FLOAT - 27)) | (1 << (WdlParser.STRING - 27)) | (1 << (WdlParser.FILE - 27)) | (1 << (WdlParser.DIRECTORY - 27)) | (1 << (WdlParser.ARRAY - 27)) | (1 << (WdlParser.MAP - 27)) | (1 << (WdlParser.PAIR - 27)) | (1 << (WdlParser.Identifier - 27)))) != 0):
                self.state = 662
                self.any_decls()
                self.state = 667
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 668
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Workflow_outputContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OUTPUT(self):
            return self.getToken(WdlParser.OUTPUT, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def bound_decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Bound_declsContext)
            else:
                return self.getTypedRuleContext(WdlParser.Bound_declsContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_workflow_output

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWorkflow_output" ):
                listener.enterWorkflow_output(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWorkflow_output" ):
                listener.exitWorkflow_output(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWorkflow_output" ):
                return visitor.visitWorkflow_output(self)
            else:
                return visitor.visitChildren(self)




    def workflow_output(self):

        localctx = WdlParser.Workflow_outputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 108, self.RULE_workflow_output)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 670
            self.match(WdlParser.OUTPUT)
            self.state = 671
            self.match(WdlParser.LBRACE)
            self.state = 675
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 27)) & ~0x3f) == 0 and ((1 << (_la - 27)) & ((1 << (WdlParser.BOOLEAN - 27)) | (1 << (WdlParser.INT - 27)) | (1 << (WdlParser.FLOAT - 27)) | (1 << (WdlParser.STRING - 27)) | (1 << (WdlParser.FILE - 27)) | (1 << (WdlParser.DIRECTORY - 27)) | (1 << (WdlParser.ARRAY - 27)) | (1 << (WdlParser.MAP - 27)) | (1 << (WdlParser.PAIR - 27)) | (1 << (WdlParser.Identifier - 27)))) != 0):
                self.state = 672
                self.bound_decls()
                self.state = 677
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 678
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Workflow_elementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WdlParser.RULE_workflow_element

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class OutputContext(Workflow_elementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Workflow_elementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def workflow_output(self):
            return self.getTypedRuleContext(WdlParser.Workflow_outputContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOutput" ):
                listener.enterOutput(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOutput" ):
                listener.exitOutput(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOutput" ):
                return visitor.visitOutput(self)
            else:
                return visitor.visitChildren(self)


    class InputContext(Workflow_elementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Workflow_elementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def workflow_input(self):
            return self.getTypedRuleContext(WdlParser.Workflow_inputContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInput" ):
                listener.enterInput(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInput" ):
                listener.exitInput(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInput" ):
                return visitor.visitInput(self)
            else:
                return visitor.visitChildren(self)


    class Parameter_meta_elementContext(Workflow_elementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Workflow_elementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def parameter_meta(self):
            return self.getTypedRuleContext(WdlParser.Parameter_metaContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameter_meta_element" ):
                listener.enterParameter_meta_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameter_meta_element" ):
                listener.exitParameter_meta_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameter_meta_element" ):
                return visitor.visitParameter_meta_element(self)
            else:
                return visitor.visitChildren(self)


    class Meta_elementContext(Workflow_elementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Workflow_elementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def meta(self):
            return self.getTypedRuleContext(WdlParser.MetaContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeta_element" ):
                listener.enterMeta_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeta_element" ):
                listener.exitMeta_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeta_element" ):
                return visitor.visitMeta_element(self)
            else:
                return visitor.visitChildren(self)


    class Inner_elementContext(Workflow_elementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WdlParser.Workflow_elementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def inner_workflow_element(self):
            return self.getTypedRuleContext(WdlParser.Inner_workflow_elementContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInner_element" ):
                listener.enterInner_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInner_element" ):
                listener.exitInner_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInner_element" ):
                return visitor.visitInner_element(self)
            else:
                return visitor.visitChildren(self)



    def workflow_element(self):

        localctx = WdlParser.Workflow_elementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 110, self.RULE_workflow_element)
        try:
            self.state = 685
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlParser.INPUT]:
                localctx = WdlParser.InputContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 680
                self.workflow_input()
                pass
            elif token in [WdlParser.OUTPUT]:
                localctx = WdlParser.OutputContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 681
                self.workflow_output()
                pass
            elif token in [WdlParser.SCATTER, WdlParser.CALL, WdlParser.IF, WdlParser.BOOLEAN, WdlParser.INT, WdlParser.FLOAT, WdlParser.STRING, WdlParser.FILE, WdlParser.DIRECTORY, WdlParser.ARRAY, WdlParser.MAP, WdlParser.PAIR, WdlParser.Identifier]:
                localctx = WdlParser.Inner_elementContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 682
                self.inner_workflow_element()
                pass
            elif token in [WdlParser.PARAMETERMETA]:
                localctx = WdlParser.Parameter_meta_elementContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 683
                self.parameter_meta()
                pass
            elif token in [WdlParser.META]:
                localctx = WdlParser.Meta_elementContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 684
                self.meta()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WorkflowContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WORKFLOW(self):
            return self.getToken(WdlParser.WORKFLOW, 0)

        def Identifier(self):
            return self.getToken(WdlParser.Identifier, 0)

        def LBRACE(self):
            return self.getToken(WdlParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(WdlParser.RBRACE, 0)

        def workflow_element(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Workflow_elementContext)
            else:
                return self.getTypedRuleContext(WdlParser.Workflow_elementContext,i)


        def getRuleIndex(self):
            return WdlParser.RULE_workflow

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWorkflow" ):
                listener.enterWorkflow(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWorkflow" ):
                listener.exitWorkflow(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWorkflow" ):
                return visitor.visitWorkflow(self)
            else:
                return visitor.visitChildren(self)




    def workflow(self):

        localctx = WdlParser.WorkflowContext(self, self._ctx, self.state)
        self.enterRule(localctx, 112, self.RULE_workflow)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 687
            self.match(WdlParser.WORKFLOW)
            self.state = 688
            self.match(WdlParser.Identifier)
            self.state = 689
            self.match(WdlParser.LBRACE)
            self.state = 693
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WdlParser.SCATTER) | (1 << WdlParser.CALL) | (1 << WdlParser.IF) | (1 << WdlParser.INPUT) | (1 << WdlParser.OUTPUT) | (1 << WdlParser.PARAMETERMETA) | (1 << WdlParser.META) | (1 << WdlParser.BOOLEAN) | (1 << WdlParser.INT) | (1 << WdlParser.FLOAT) | (1 << WdlParser.STRING) | (1 << WdlParser.FILE) | (1 << WdlParser.DIRECTORY) | (1 << WdlParser.ARRAY) | (1 << WdlParser.MAP) | (1 << WdlParser.PAIR))) != 0) or _la==WdlParser.Identifier:
                self.state = 690
                self.workflow_element()
                self.state = 695
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 696
            self.match(WdlParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Document_elementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def import_doc(self):
            return self.getTypedRuleContext(WdlParser.Import_docContext,0)


        def struct(self):
            return self.getTypedRuleContext(WdlParser.StructContext,0)


        def task(self):
            return self.getTypedRuleContext(WdlParser.TaskContext,0)


        def workflow(self):
            return self.getTypedRuleContext(WdlParser.WorkflowContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_document_element

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDocument_element" ):
                listener.enterDocument_element(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDocument_element" ):
                listener.exitDocument_element(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDocument_element" ):
                return visitor.visitDocument_element(self)
            else:
                return visitor.visitChildren(self)




    def document_element(self):

        localctx = WdlParser.Document_elementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 114, self.RULE_document_element)
        try:
            self.state = 702
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WdlParser.IMPORT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 698
                self.import_doc()
                pass
            elif token in [WdlParser.STRUCT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 699
                self.struct()
                pass
            elif token in [WdlParser.TASK]:
                self.enterOuterAlt(localctx, 3)
                self.state = 700
                self.task()
                pass
            elif token in [WdlParser.WORKFLOW]:
                self.enterOuterAlt(localctx, 4)
                self.state = 701
                self.workflow()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DocumentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def version(self):
            return self.getTypedRuleContext(WdlParser.VersionContext,0)


        def EOF(self):
            return self.getToken(WdlParser.EOF, 0)

        def document_element(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WdlParser.Document_elementContext)
            else:
                return self.getTypedRuleContext(WdlParser.Document_elementContext,i)


        def workflow(self):
            return self.getTypedRuleContext(WdlParser.WorkflowContext,0)


        def getRuleIndex(self):
            return WdlParser.RULE_document

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDocument" ):
                listener.enterDocument(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDocument" ):
                listener.exitDocument(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDocument" ):
                return visitor.visitDocument(self)
            else:
                return visitor.visitChildren(self)




    def document(self):

        localctx = WdlParser.DocumentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 116, self.RULE_document)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 704
            self.version()
            self.state = 708
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,58,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 705
                    self.document_element() 
                self.state = 710
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,58,self._ctx)

            self.state = 718
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WdlParser.WORKFLOW:
                self.state = 711
                self.workflow()
                self.state = 715
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WdlParser.IMPORT) | (1 << WdlParser.WORKFLOW) | (1 << WdlParser.TASK) | (1 << WdlParser.STRUCT))) != 0):
                    self.state = 712
                    self.document_element()
                    self.state = 717
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 720
            self.match(WdlParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[16] = self.expr_infix0_sempred
        self._predicates[17] = self.expr_infix1_sempred
        self._predicates[18] = self.expr_infix2_sempred
        self._predicates[19] = self.expr_infix3_sempred
        self._predicates[20] = self.expr_infix4_sempred
        self._predicates[22] = self.expr_core_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_infix0_sempred(self, localctx:Expr_infix0Context, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

    def expr_infix1_sempred(self, localctx:Expr_infix1Context, predIndex:int):
            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         

    def expr_infix2_sempred(self, localctx:Expr_infix2Context, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 7:
                return self.precpred(self._ctx, 2)
         

    def expr_infix3_sempred(self, localctx:Expr_infix3Context, predIndex:int):
            if predIndex == 8:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 9:
                return self.precpred(self._ctx, 2)
         

    def expr_infix4_sempred(self, localctx:Expr_infix4Context, predIndex:int):
            if predIndex == 10:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 11:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 12:
                return self.precpred(self._ctx, 2)
         

    def expr_core_sempred(self, localctx:Expr_coreContext, predIndex:int):
            if predIndex == 13:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 14:
                return self.precpred(self._ctx, 5)
         




