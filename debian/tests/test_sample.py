from wdlparse.draft2 import wdl_parser

wdl_code = """
task my_task {
  File file
  command {
    ./my_binary --input=${file} > results
  }
  output {
    File results = "results"
  }
}

workflow my_wf {
  call my_task
}
"""

def test_basic_Ast():
	ast = wdl_parser.parse(wdl_code).ast()
	cmpnd=wdl_parser.parse(wdl_code).is_compound_nud()
	check=isinstance(ast,wdl_parser.Ast)

	assert ast.name == "Namespace"
	assert cmpnd == False
	assert check == True
