# wdl-parsers

A Python package that provides the generated `Hermes` and `Antlr4` WDL parsers for Python.

| Version | Parser(s) | Module |
|---------|-----------|--------|
| draft-2 | Hermes | draft2 |
| v1.0 | Antlr4 | v1 |
| development | Antlr4 |dev |


## Installation

#### Install via pip

`pip install wdlparse`

## Usage

```python
# to be written.
```

## License

The grammar files come from [openwdl/wdl](https://github.com/openwdl/wdl/) under the [BSD 3-Clause "New" or "Revised" License](https://github.com/openwdl/wdl/blob/main/LICENSE).
